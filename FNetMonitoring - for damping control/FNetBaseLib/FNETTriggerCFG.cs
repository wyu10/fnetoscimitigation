﻿/*******************************************************************************************
 * Class to retrieve trigger configuration, load specified assembly dynamically 
 * and create trigger object of specified class. 
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.9
 * Modified by Ferriad at 11/2/2017
 *  1. Restruct the exception handling in trigger object creation part by throwing all exception of the most shallow layer.
 *  2. Format the error message. 
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace FNetBaseLib
{
    public class FNETTriggerCFGManager
    {
        private static ExeConfigurationFileMap _cfgMap;
        private static Dictionary<string, Assembly> _loadedAssembly = new Dictionary<string, Assembly>();
        static FNETTriggerCFGManager()
        {
            _cfgMap = new ExeConfigurationFileMap();
            //_cfgMap.ExeConfigFilename = _cfgFilePath + _cfgFileName;
        }
        public static FNetTriggerBase CreateTriggerObj(string TriggerCFGName, string TriggerCFGFile= "FNETTriggerConfiguration.config")
        {
            FNetTriggerBase TriggerObj = null;
            try
            {
                lock (_cfgMap)
                {
                    FNetTriggerBase.WriteGeneralLog("----------------------------");
                    FNetTriggerBase.WriteGeneralLog(string.Format("Start to create trigger instance according to <{0}> in {1}", TriggerCFGName, TriggerCFGFile));
                    if (!File.Exists(TriggerCFGFile))
                    {
                        FNetTriggerBase.WriteGeneralLog(string.Format("Error Opening Configuration file. Message: {0} doesn't exist: ", TriggerCFGFile));
                        throw new Exception(string.Format("Error Opening Configuration file. Message: {0} doesn't exist.", TriggerCFGFile));
                    }
                    _cfgMap.ExeConfigFilename = TriggerCFGFile;
                    FNetTriggerBase.WriteGeneralLog(string.Format("\t->CFG file {0} is found.", TriggerCFGFile));
                    var config = ConfigurationManager.OpenMappedExeConfiguration(_cfgMap, ConfigurationUserLevel.None);
                    
                    //-----------Get db configuration-------------
                    try
                    {
                        //try { FNetTriggerBase.MonitorHost = config.AppSettings.Settings["MonitorHost"].Value; } catch { }
                        //try { FNetTriggerBase.MonitorHostPort = Convert.ToInt32(config.AppSettings.Settings["MonitorHostPort"].Value); } catch { }
                        try { FNetTriggerBase.HeartBeatPort = Convert.ToInt32(config.AppSettings.Settings["HeartBeatPort"].Value); } catch { }
                        try
                        {
                            FNetTriggerBase.HeartBeatDestination = config.AppSettings.Settings["HeartBeatDestination"].Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var v in FNetTriggerBase.HeartBeatDestination)
                            {
                                var IPList = System.Net.Dns.GetHostEntry(v).AddressList;
                                if (IPList.Length > 0 && !FNetTriggerBase.IPEndPointList.ContainsKey(v))
                                    FNetTriggerBase.IPEndPointList.Add(v, new System.Net.IPEndPoint(IPList[0], FNetTriggerBase.HeartBeatPort));
                            }
                        }
                        catch { }
                        try { FNetTriggerBase.ThisNodeName = config.AppSettings.Settings["ThisNodeName"].Value; } catch { }
                    }
                    catch (Exception e)
                    {
                        //FNetTriggerBase.WriteGeneralLog(String.Format("Error Finding Monitor system configuration. Message: {0}", e.Message));
                    }
                    //--------------------------------------------
                    var section = config.GetSection("TriggerConfigurationSection");

                    if (section == null)
                    {
                        FNetTriggerBase.WriteGeneralLog(string.Format("Error finding section <TriggerConfigurationSection> ... </TriggerConfigurationSection> in file {0}.", TriggerCFGFile));
                        throw new Exception(string.Format("Error finding section <TriggerConfigurationSection> ... </TriggerConfigurationSection> in file {0}.",
                            TriggerCFGFile));
                    }
                    FNetTriggerBase.WriteGeneralLog("\t->Section <TriggerConfigurationSection> is found.");

                    var TriggerCFG = (section as TriggerConfigurationSection).Triggers[TriggerCFGName];
                    if (TriggerCFG == null)
                    {
                        FNetTriggerBase.WriteGeneralLog(string.Format("Error finding configuration \"{0}\" in file {1}", TriggerCFGName, TriggerCFGFile));
                        throw new Exception(string.Format("Error finding configuration \"{0}\" in file {1}", TriggerCFGName, TriggerCFGFile));
                    }
                    FNetTriggerBase.WriteGeneralLog(string.Format("\t->Section {0} is found.", TriggerCFGName));

                    if (TriggerCFG.AssemblyFile == null || TriggerCFG.AssemblyFile.Length < 1 || !File.Exists(TriggerCFG.AssemblyFile))
                    {
                        FNetTriggerBase.WriteGeneralLog(string.Format("Error finding assembly {0} for {1}", TriggerCFG.AssemblyFile, TriggerCFGName));
                        throw new Exception(string.Format("Error finding assembly {0} for {1}", TriggerCFG.AssemblyFile, TriggerCFGName));
                    }
                    FNetTriggerBase.WriteGeneralLog(string.Format("\t->Assembly file is found: {0}.", TriggerCFG.AssemblyFile));

                    if (!_loadedAssembly.ContainsKey(TriggerCFG.AssemblyFile.ToLower()))
                    {
                        //Assembly OneAssembly = Assembly.LoadFile(TriggerCFG.AssemblyFile);
                        FileInfo fi = new FileInfo(TriggerCFG.AssemblyFile);
                        Assembly OneAssembly = Assembly.LoadFile(fi.FullName);
                        if (OneAssembly==null)
                        {
                            FNetTriggerBase.WriteGeneralLog(string.Format("Error loading assembly {0} for {1}", TriggerCFG.AssemblyFile, TriggerCFGName));
                            throw new Exception(string.Format("Error loading assembly {0} for {1}", TriggerCFG.AssemblyFile, TriggerCFGName));
                        }
                        FNetTriggerBase.WriteGeneralLog(string.Format("\t->Assembly file is loaded: {0}.", TriggerCFG.AssemblyFile));
                        _loadedAssembly.Add(TriggerCFG.AssemblyFile.ToLower(), OneAssembly);
                    }
                    else
                        FNetTriggerBase.WriteGeneralLog(string.Format("\t->Assembly file is already loaded: {0}.", TriggerCFG.AssemblyFile));

                    if (TriggerCFG.ClassName == null || TriggerCFG.ClassName.Length < 1)
                    {
                        FNetTriggerBase.WriteGeneralLog(String.Format("Error getting ClassName. Message: Need to specify class name for {0}", TriggerCFGName));
                        throw new Exception(String.Format("Error getting ClassName. Message: Need to specify class name for {0}", TriggerCFGName));
                    }
                    FNetTriggerBase.WriteGeneralLog(string.Format("\t->Trying to get class Type: {0}.", TriggerCFG.ClassName));

                    Type TriggerType = _loadedAssembly[TriggerCFG.AssemblyFile.ToLower()].GetType(TriggerCFG.ClassName);
                    if(TriggerType==null)
                    {
                        FNetTriggerBase.WriteGeneralLog(string.Format("Error getting class Type {0}", TriggerCFG.ClassName));
                        throw new Exception(string.Format("Error getting class Type {0}", TriggerCFG.ClassName));
                    }
                    FNetTriggerBase.WriteGeneralLog(string.Format("\t->Class {0} is found.", TriggerCFG.ClassName));
                    if (!TriggerType.IsSubclassOf(typeof(FNetTriggerBase)))
                    {
                        FNetTriggerBase.WriteGeneralLog(string.Format("Error creating Subclass of FNetTriggerBase. Message: Specified class {0} must extend FNetTriggerBase.", TriggerCFG.ClassName));
                        throw new Exception(string.Format("Error creating Subclass of FNetTriggerBase. Message: Specified class {0} must extend FNetTriggerBase.", TriggerCFG.ClassName));
                    }
                    FNetTriggerBase.WriteGeneralLog(string.Format("\t->Class {0} is a valid subclass of FNetTriggerBase.", TriggerCFG.ClassName));

                    TriggerObj = (FNetTriggerBase)Activator.CreateInstance(TriggerType);
                    if (TriggerObj == null)
                    {
                        FNetTriggerBase.WriteGeneralLog(String.Format("Error creating TriggerObj Instance. Message: Failed to create instance of {0}", TriggerCFG.ClassName));
                        throw new Exception(String.Format("Error creating TriggerObj Instance. Message: Failed to create instance of {0}", TriggerCFG.ClassName));
                    }
                    FNetTriggerBase.WriteGeneralLog(string.Format("\t->Instance of class {0} is created.", TriggerCFG.ClassName));
                    FNetTriggerBase.WriteGeneralLog("\t->Setting parameters:");
                    if (TriggerCFG.ParameterString!=null && TriggerCFG.ParameterString.Trim().Length>2)
                    {
                        TriggerObj.MonitoredGrid = TriggerCFG.MonitoredGrid;
                        string[] ParaAndValues = TriggerCFG.ParameterString.Replace(System.Environment.NewLine, string.Empty).Replace("\t", "").Split(';');
                        foreach(string PaV in ParaAndValues)
                        {
                            if(PaV!=null &&PaV.Trim().Length>2)
                            {
                                string[] arr = PaV.Split('=');
                                if (arr.Length == 2)
                                {
                                    TriggerObj.SetParameter(arr[0].Trim(), arr[1].Trim());
                                    FNetTriggerBase.WriteGeneralLog(string.Format("\t\t\t->Set {0} = {1}", arr[0].Trim(), arr[1].Trim()));
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                FNetTriggerBase.WriteGeneralLog(string.Format("Error creating the trigger instance for \"{0}\". Message: {1}", TriggerCFGName, e.Message));
                throw new Exception(string.Format("Error creating the trigger instance for \"{0}\". Message: {1}", TriggerCFGName, e.Message));
            }
            return TriggerObj;
        }
    }
    public class TriggerCFGElement : ConfigurationElement
    {
        [ConfigurationProperty("Name", IsRequired = true, IsKey = true)]
        public string Name { get { return (string)base["Name"]; } }

        [ConfigurationProperty("AssemblyFile", IsRequired = true)]
        public string AssemblyFile { get { return (string)base["AssemblyFile"]; } }

        [ConfigurationProperty("ClassName", IsRequired = true)]
        public string ClassName { get { return (string)base["ClassName"]; } }

        [ConfigurationProperty("MonitoredGrid", IsRequired = true)]
        public string MonitoredGrid { get { return (string)base["MonitoredGrid"]; } }

        [ConfigurationProperty("ParameterString", IsRequired = true)]
        public string ParameterString { get { return (string)base["ParameterString"]; } }
        
    }

    [ConfigurationCollection(typeof(TriggerCFGElement))]
    public class TriggerCFGElementCollection : ConfigurationElementCollection
    {
        internal const string PropertyName = "Trigger";

        public override ConfigurationElementCollectionType CollectionType { get { return ConfigurationElementCollectionType.BasicMapAlternate; } }
        protected override string ElementName { get { return PropertyName; } }
        protected override bool IsElementName(string elementName) { return elementName.Equals(PropertyName, StringComparison.InvariantCultureIgnoreCase); }
        public override bool IsReadOnly() { return false; }
        protected override ConfigurationElement CreateNewElement() { return new TriggerCFGElement(); }
        protected override object GetElementKey(ConfigurationElement element) { return ((TriggerCFGElement)(element)).Name; }
        public new TriggerCFGElement this[string CFGName] { get { return (TriggerCFGElement)BaseGet(CFGName); } }
    }

    public class TriggerConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("Triggers")]
        public TriggerCFGElementCollection Triggers
        {
            get { return ((TriggerCFGElementCollection)(base["Triggers"])); }
            set { base["Triggers"] = value; }
        }
    }












    public class TriggerConfigurationGroup : ConfigurationSectionGroup
    {
        [ConfigurationProperty("TriggerConfiguration")]
        public TriggerConfiguration TriggerConfiguration
        {
            get { return (TriggerConfiguration)base.Sections["TriggerConfiguration"];}
        }
    }
    
    public class TriggerConfiguration:ConfigurationSection
    {
        [ConfigurationProperty("Name")]
        public ValueElement To
        {
            get { return (ValueElement)base["Name"]; }
        }
    }
    public class ValueElement : ConfigurationElement
    {
        [ConfigurationProperty("value")]
        public string Value
        {
            get { return (string)base["value"]; }
            set { base["value"] = value; }
        }
    }



    public class PageGroup : ConfigurationSectionGroup
    {
        //[ConfigurationProperty("TriggerConfiguration")]
        //public TriggerConfiguration TriggerConfiguration
        //{
        //    get { return (TriggerConfiguration)base.Sections["TriggerConfiguration"]; }
        //}
    }
    public class PageAppearanceSection : ConfigurationSection
    {
        // Create a "remoteOnly" attribute.
        [ConfigurationProperty("remoteOnly", DefaultValue = "false", IsRequired = false)]
        public Boolean RemoteOnly
        {
            get
            {
                return (Boolean)this["remoteOnly"];
            }
            set
            {
                this["remoteOnly"] = value;
            }
        }

        // Create a "font" element.
        [ConfigurationProperty("font")]
        public FontElement Font
        {
            get
            {
                return (FontElement)this["font"];
            }
            set
            { this["font"] = value; }
        }

        // Create a "color element."
        [ConfigurationProperty("color")]
        public ColorElement Color
        {
            get
            {
                return (ColorElement)this["color"];
            }
            set
            { this["color"] = value; }
        }
    }

    // Define the "font" element
    // with "name" and "size" attributes.
    public class FontElement : ConfigurationElement
    {
        [ConfigurationProperty("name", DefaultValue = "Arial", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\", MinLength = 1, MaxLength = 60)]
        public String Name
        {
            get
            {
                return (String)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("size", DefaultValue = "12", IsRequired = false)]
        [IntegerValidator(ExcludeRange = false, MaxValue = 24, MinValue = 6)]
        public int Size
        {
            get
            { return (int)this["size"]; }
            set
            { this["size"] = value; }
        }
    }

    // Define the "color" element 
    // with "background" and "foreground" attributes.
    public class ColorElement : ConfigurationElement
    {
        [ConfigurationProperty("background", DefaultValue = "FFFFFF", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\GHIJKLMNOPQRSTUVWXYZ", MinLength = 6, MaxLength = 6)]
        public String Background
        {
            get
            {
                return (String)this["background"];
            }
            set
            {
                this["background"] = value;
            }
        }

        [ConfigurationProperty("foreground", DefaultValue = "000000", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\GHIJKLMNOPQRSTUVWXYZ", MinLength = 6, MaxLength = 6)]
        public String Foreground
        {
            get
            {
                return (String)this["foreground"];
            }
            set
            {
                this["foreground"] = value;
            }
        }

    }



    


}
