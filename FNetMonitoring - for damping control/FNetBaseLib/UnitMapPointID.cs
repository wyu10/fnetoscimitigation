﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib
{
    /// <summary>
    /// This class is used to map a FDR ID & mea type to a point ID in historian archive, or reverse the procedure.
    /// **** Be cautious to modify this class.                                                              ****
    /// **** Modification to the existing mea type code may make the historical data unreadable.            ****
    /// **** The lower 4 digit of the decimal PointID is measurement type, the other digit is unit ID.      ****
    /// **** It allows 10000 measurement types for each unit, which should be sufficient.                   ****
    /// **** So please add new measurement types if needed rather than modifying the exsiting types.        ****
    /// **** Please add new measurement types wisely.                                                       ****
    /// Wenpeng 01/2022.
    /// </summary>
    public class UnitMapPointID
    {

        //map unit ID (FDR ID or UGA ID) and measurement type to measurement Point ID in the historian.
        //The lower 4 digit of the decimal PointID is measurement type, the other digit is unit ID.
        public static ulong GetMeaPointID(int UnitID, MeaType measurementType)
        {
            ulong PointID = (ulong)UnitID;
            PointID = PointID * 10000 + (ulong)measurementType;     //to make the PointID human readable, we don't use bit operation.
            return PointID;
        }
        public static ulong GetMeaPointID(int UnitID, string Acronym)
        {
            ulong PointID = (ulong)UnitID;
            PointID = PointID * 10000 + (ulong)GetMeaType(Acronym);     //to make the PointID human readable, we don't use bit operation.
            return PointID;
        }
        public static MeaType GetMeaType(string Acronym)
        {
            switch (Acronym.ToUpper())
            {
                case "FREQ": return MeaType.Frequency;
                case "VPHM": return MeaType.VoltageMag;
                case "VPHA": return MeaType.PhaseAngle;
                default: return MeaType.Frequency;
            }
        }
        public static string GetMeaTypeAcronym(MeaType meaType)
        {
            switch (meaType)
            {
                case MeaType.Frequency: return "FREQ";
                case MeaType.VoltageMag: return "VPHM";
                case MeaType.PhaseAngle: return "VPHA";
                default: throw new Exception("not implemented");
            }
        }
        //map measurement Point ID in the historian to unit ID (FDR ID or UGA ID) and measurement type.
        public static bool GetUnitIDAndType(ulong PointID, out int UnitID, out MeaType MeasurementType)
        {
            UnitID = 0;
            MeasurementType = MeaType.Unknown;
            if (PointID < 10000)
                return false;
            UnitID = (int)(PointID / 10000);
            try
            {
                MeasurementType = (MeaType)(PointID % 10000);
            }
            catch (Exception e)
            {
                MeasurementType = MeaType.Unknown;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Add new types, do NOT modify the existing code of MeaType, or else the historical data may not be readable...
        /// The code range could be 1 - 9999
        /// </summary>
        public enum MeaType : ulong
        {
            Unknown = 0,
            //--------------
            //for single phase
            Frequency = 1,
            PhaseAngle = 2,
            VoltageMag = 3,
            //-------------
            //for single phase
            CurrentAngle = 12,
            CurrentMag = 13,
            //ActivePower,
            //ReactivePower,
            //-------------
            //...Phase A...
            //...Phase B...
            //...Phase C...
            //-------------
            SatelliteNum = 91,
            Latitude = 92,
            Longitude = 93,
            ReceivedTime = 94,
            //-------------
            THD = 100,
            SNR = 101,
            SAG = 102,
            SWELL = 103,
            //-------------
            Har2 = 122,
            Har3 = 123,
            Har4 = 124,
            Har5 = 125,
            Har6 = 126,
            Har7 = 127,
            Har8 = 128,
            Har9 = 129,
            Har10 = 130,
            Har11 = 131,
            Har12 = 132,
            Har13 = 133,
            Har14 = 134,
            Har15 = 135,
            //------------
            POW_V = 200,    //voltage POW
            POW_I = 201     //Current POW
            //------------
            //...
            //------------

        }
    }
}
