﻿using System.Collections.Generic;
namespace FNetBaseLib.YWPLib
{
    public class WindowForMax<T>
    {
        public delegate int Compare<Ty>(Ty A, Ty B);
        private Compare<T> _isAPriorToB;
        private LinkedList<ObjWithIndex> _window = new LinkedList<ObjWithIndex>();
        private long _curIndex = 0;
        private long _windowLenght = 0;
        public WindowForMax(long windowLength, Compare<T> CompareHandler)
        {
            _windowLenght = windowLength;
            _isAPriorToB = CompareHandler;
        }
        public void Push(T OneItem)
        {
            ObjWithIndex O = new ObjWithIndex(++_curIndex, OneItem);
            while (_window.Count > 0 && _isAPriorToB(OneItem, _window.First.Value.Obj)>0)
                _window.RemoveFirst();
            _window.AddFirst(O);
            while (_window.Count > 0 && _window.Last.Value.Index <= _curIndex - _windowLenght)
                _window.RemoveLast();
        }
        public T Max
        {
            get { return _window.Count > 0 ? _window.Last.Value.Obj : default(T); }
        }
        private struct ObjWithIndex
        {
            public ObjWithIndex(long index, T obj)
            {
                Obj = obj;
                Index = index;
            }
            public long Index;
            public T Obj;            
        }
    }
    public class WindowForMaxAndMin<T>
    {
        private WindowForMax<T> _windowForMax;
        private WindowForMax<T> _windowForMin;
        public WindowForMaxAndMin(long windowLength, WindowForMax<T>.Compare<T> CompareHandler)
        {
            _windowForMax = new WindowForMax<T>(windowLength, CompareHandler);
            _windowForMin = new WindowForMax<T>(windowLength, new WindowForMax<T>.Compare<T>((A, B) => CompareHandler(B, A)));
        }
        public void Push(T OneItem)
        {
            _windowForMax.Push(OneItem);
            _windowForMin.Push(OneItem);
        }
        public T Max { get { return _windowForMax.Max; } }
        public T Min { get { return _windowForMin.Max; } }        
    }
}
