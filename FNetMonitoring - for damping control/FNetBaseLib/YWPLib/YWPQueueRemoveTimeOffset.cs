﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib.YWPLib
{
    /// <summary>
    /// A class that can add a time offset to the raw timestamp.
    /// </summary>
    public class YWPQueueRemoveTimeOffset
    {
        public delegate void Log(string msg);
        //public event Log OutputLog;   //for testing.
        private int UnitID;
        /// <summary>
        /// List of data, ordered by timestamp. Early [0, ... , ListLength] Late.
        /// </summary>
        private List<FNetMeasurement> _DataList = new List<FNetMeasurement>();

        /// <summary>
        /// The time offset which will be ADDED to the timestamp to get correct timestamp.
        /// </summary>
        private readonly double _TimeOffsetToBeAddedInSec = 0;
        private readonly int _FramePerSecond = 10;
        private double _FrameIntervalInSec = 1;
        /// <summary>
        /// It is for check if the frames are continuous. It is the upper limite of frame interval.
        /// Usually equal to frame interval * 1.05
        /// </summary>
        private double _FrameIntervalInSec_Max = 1;

        private DateTime _LastReportedTime = DateTime.MinValue;

        /// <summary>
        /// If an overlarge time is pushed, the count++ and the data will be abandoned until the count larger than a threshold
        /// </summary>
        private int _CountOverlargeTime = 0;

        /// <summary>
        /// If an overEarly time is pushed, the count++ and the data will be abandoned until the count larger than a threshold.
        /// </summary>
        private int _CountOverEarly = 0;
        private int _ValidTimeRangeInSec = 3600;
        /// <summary>
        /// Max waiting time to determine whether a frame is lost
        /// </summary>
        private int _WaitTimeInSec = 2;

        /// <summary>
        /// max timespan of the interpolation.
        /// </summary>
        private double _MaxInterpolationTimeRangeInSec = 1.5;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="framePerSecond"></param>
        /// <param name="timeOffsetToBeAdded_inSec">second(s) that will be 'added' to timestamp to get the accurate one. Raw time + offset = Accurate time</param>
        /// <param name="validTimeRange_inSec">If an overlarge time is pushed, the count++ and the data will be abandoned until the count larger than 20. validTimeRange_inSec will indicate if a timestamp is overlarge comparing to latest timestamp in the list.</param>
        public YWPQueueRemoveTimeOffset(int framePerSecond, double timeOffsetToBeAdded_inSec, int validTimeRange_inSec, int unitID)
        {
            _TimeOffsetToBeAddedInSec = timeOffsetToBeAdded_inSec;
            _FramePerSecond = framePerSecond;
            _FrameIntervalInSec = 1.0 / framePerSecond;
            _FrameIntervalInSec_Max = _FrameIntervalInSec * 1.05;
            _ValidTimeRangeInSec = validTimeRange_inSec;
            UnitID = unitID;
        }
        private bool PushRawData(FNetMeasurement rawdata)
        {
            //OutputLog("push " + rawdata.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            if (rawdata == null)
                return false;
            if (_DataList.Count == 0)
            {
                _DataList.Add(rawdata);
                return true;
            }
            //rawdata Time is too early.
            if (rawdata.TimeStamp.AddSeconds(_TimeOffsetToBeAddedInSec).CompareTo(_LastReportedTime) <= 0)
            {
                if (_CountOverEarly++ < 150)
                {
                    //OutputLog(string.Format("too early, discard. count={0}, LastRptTime={1}", _CountOverEarly, _LastReportedTime.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                    return false;
                }
                else
                {
                    _LastReportedTime = DateTime.MinValue;                    
                    _DataList.Clear();
                    _DataList.Add(rawdata);
                    //OutputLog("Continous over early time is pushed. Keep the data.");
                    return true;
                }
            }
            _CountOverEarly = 0;
            //rawdata time is the latest time. This is the most common case.
            if (rawdata.TimeStamp.CompareTo(_DataList.Last().TimeStamp) > 0)
            {
                //how to know if a timestamp is right or wrong? For example what if a time is 2099-01-01?
                //what if the data stream stops for several days?
                if (rawdata.TimeStamp.Subtract(_DataList.Last().TimeStamp).TotalSeconds > _ValidTimeRangeInSec )
                {
                    if (_CountOverlargeTime++ < 150)
                    {
                        //OutputLog("time is too lead... Discard. Count="+ _CountOverlargeTime.ToString());
                        return false;
                    }
                    else
                    {
                        //OutputLog("Continous over large time is pushed. Keep the data.");
                    }

                }                
                _CountOverlargeTime = 0;
                _DataList.Add(rawdata);
                //OutputLog("data is pushed successfully.");
                return true;
            }
            _CountOverlargeTime = 0;
            //------------------------------------------------------------------
            // _LastReportedTime < rawdata time < _DataList.Last().TimeStamp
            if (rawdata.TimeStamp.CompareTo(_DataList[0].TimeStamp) < 0)
                _DataList.Insert(0, rawdata);
            else if (rawdata.TimeStamp.CompareTo(_DataList[0].TimeStamp) == 0)
                _DataList[0] = rawdata;
            else if (rawdata.TimeStamp.CompareTo(_DataList.Last().TimeStamp) == 0)
                _DataList[_DataList.Count - 1] = rawdata;
            else
            {
                //OutputLog("Try to find a spot. First:"+ _DataList[0].TimeStamp.ToString("HH:mm:ss.fff")+". Last:"+ _DataList.Last().TimeStamp.ToString("HH:mm:ss.fff"));
                int left = 0, right = _DataList.Count - 1;
                int middle;
                while (right - left > 1)
                {
                    middle = (right + left) / 2;
                    int c = rawdata.TimeStamp.CompareTo(_DataList[middle].TimeStamp);
                    if (c == 0)
                    {
                        left = right = middle;
                        _DataList[middle] = rawdata;
                        break;
                    }
                    else if (c < 0) right = middle;
                    else left = middle;
                }
                if (rawdata.TimeStamp.CompareTo(_DataList[left].TimeStamp) == 0)
                    _DataList[left] = rawdata;
                else if (rawdata.TimeStamp.CompareTo(_DataList[right].TimeStamp) == 0)
                    _DataList[right] = rawdata;
                else
                    _DataList.Insert(right, rawdata);
                //OutputLog("Found a spot.");
            }
            return true;
        }

        public List<FNetMeasurement> PushRawDataAndGetAvailable(FNetMeasurement rawdata)
        {
            //OutputLog("start PushRawData(rawdata)");
            PushRawData(rawdata);
            //OutputLog("start PopAvailableData");
            return PopAvailableData();

        }
        public List<FNetMeasurement> GetRestOfData()
        {
            return PopAvailableData(true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private List<FNetMeasurement> PopAvailableData(bool NoNeedToWait = false)
        {
            List<FNetMeasurement> result = new List<FNetMeasurement>();
            if (_DataList.Count < 2)
                return result;
            if (_LastReportedTime == DateTime.MinValue)
                UpdateLastReportedTime();
            while (true)
            {
                if (_DataList.Count < 2) break;
                //Continuous frames
                if (_DataList[1].TimeStamp.Subtract(_DataList[0].TimeStamp).TotalSeconds < _FrameIntervalInSec_Max)
                {
                    var frames = GetDataBetweenFrames(_DataList[0], _DataList[1]);
                    if (frames != null && frames.Count > 0)
                        result.AddRange(frames);
                    _DataList.RemoveAt(0);
                }
                //Time is not continuous, and next frame(s) is(are) probabily lost so no need to wait.
                else if (_DataList.Last().TimeStamp.Subtract(_DataList[0].TimeStamp).TotalSeconds >= _WaitTimeInSec || NoNeedToWait)
                {   //if not too much data is lost, interpolate the frames.
                    //OutputLog("Don't wait anymore.");
                    if (_DataList[1].TimeStamp.Subtract(_DataList[0].TimeStamp).TotalSeconds <= _MaxInterpolationTimeRangeInSec)
                    {
                        var frames = GetDataBetweenFrames(_DataList[0], _DataList[1]);
                        if (frames != null && frames.Count > 0)
                            result.AddRange(frames);

                        _DataList.RemoveAt(0);
                    }
                    //if too much data is lost, do not interpolate but discard the last frame.
                    else
                    {
                        //OutputLog("Discard first data " + _DataList[0].TimeStamp.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        _DataList.RemoveAt(0);
                    }
                }
                //Time is not continuous, and the next frame maybe available later, need to wait.
                else
                    break;
            }
            if (result.Count > 0)
                _LastReportedTime = result.Last().TimeStamp;
            return result;
        }
        private List<FNetMeasurement> GetDataBetweenFrames(FNetMeasurement start, FNetMeasurement end)
        {
            if (start == null || end == null)
                return null;
            DateTime startTime = start.TimeStamp.AddSeconds(_TimeOffsetToBeAddedInSec);
            DateTime endTime = end.TimeStamp.AddSeconds(_TimeOffsetToBeAddedInSec);
            if (endTime.CompareTo(startTime) <= 0)
                return null;
            DateTime nextFrameTime = startTime;
            double distance_A, distance_B;
            List<FNetMeasurement> result = new List<FNetMeasurement>();
            while (true)
            {
                nextFrameTime = GetNextFrameTimeFromAnyTime(nextFrameTime);
                if (nextFrameTime.CompareTo(endTime) > 0)
                    break;
                FNetMeasurement mea = new FNetMeasurement(UnitID, nextFrameTime);
                distance_A = nextFrameTime.Subtract(startTime).TotalSeconds;
                distance_B = endTime.Subtract(nextFrameTime).TotalSeconds;
                if (start.HasFrequency && end.HasFrequency)
                    mea.Frequency = (float)((end.Frequency - start.Frequency) / (distance_B + distance_A) * distance_A + start.Frequency);
                if(start.HasAngle && end.HasAngle)
                {
                    double endAng = end.Angle - Math.Round((end.Angle - start.Angle) / 2 / Math.PI, 0) * Math.PI * 2;
                    double ang = (float)((endAng - start.Angle) / (distance_B + distance_A) * distance_A + start.Angle);
                    ang -= Math.Floor(ang / Math.PI / 2) * 2 * Math.PI; //Limit angle in [0, 2PI)
                    mea.Angle = (float)ang;
                }
                if(start.HasVoltage && end.HasVoltage)
                    mea.Voltage = (float)((end.Voltage - start.Voltage) / (distance_B + distance_A) * distance_A + start.Voltage);
                result.Add(mea);
            }
            return result;
        }
        private void UpdateLastReportedTime()
        {
            if (_DataList.Count < 1)
                return;
            _LastReportedTime = _DataList[0].TimeStamp;
            DateTime lastDataTime = _DataList[0].TimeStamp.AddSeconds(_TimeOffsetToBeAddedInSec);
            while (_LastReportedTime.CompareTo(lastDataTime) < 0)
                _LastReportedTime = GetNextFrameTime(_LastReportedTime);
            while (_LastReportedTime.CompareTo(lastDataTime) >= 0)
                _LastReportedTime = GetPreviousFrameTime(_LastReportedTime);


        }
        private DateTime GetPreviousFrameTime(DateTime frametime)
        {
            return frametime.AddMilliseconds(-1000 / _FramePerSecond);
        }
        private DateTime GetNextFrameTime(DateTime frametime)
        {
            return frametime.AddMilliseconds(1000 / _FramePerSecond);
        }
        private DateTime GetNextFrameTimeFromAnyTime(DateTime anytime)
        {
            return anytime.RemoveFractionalSec().AddSeconds(Math.Ceiling(Math.Round(anytime.FractionalSecond() / _FrameIntervalInSec + 0.01, 2)) * _FrameIntervalInSec);
        }

        /// <summary>
        /// Specify a (accurate) timestamp to get the corresponding data. 
        /// If the data is returned successfully, return true;
        /// If it is certain that the specified time&data will not be available, return true, and data.Timestamp will be Time.Min
        /// If the data is not available currently, but may be available later, return false and data.TimeStamp will be Time.Min
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        //public bool PopData(DateTime timestamp, out FNetMeasurement data )
        //{
        //    data = null;
        //    if (_DataList.Count < 2)
        //        return false;


        //    return true;
        //}
    }
    
}