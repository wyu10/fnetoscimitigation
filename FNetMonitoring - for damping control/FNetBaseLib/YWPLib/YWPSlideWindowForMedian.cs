﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib.YWPLib
{
    /// <summary>
    /// o(n)
    /// </summary>
    public class YWPSlideWindowForMedian
    {
        Queue<double> queue = new Queue<double>();
        List<double> sortedList = new List<double>();
        private int WindowLength = 1;
        public YWPSlideWindowForMedian(int windowLength)
        {
            WindowLength = windowLength;
        }
        public void Push(double OneValue)
        {
            queue.Enqueue(OneValue);
            while(queue.Count>WindowLength)
            {
                RemoveFromSortedList(queue.Dequeue());
            }
            AddToSortedList(OneValue);
        }
        public double GetMedian()
        {
            int count = sortedList.Count;
            if (count == 0) return 0;
            double median = sortedList[count / 2];
            if (count % 2 == 0)
                median = (median + sortedList[count / 2 - 1]) / 2;
            return median;
        }
        private void RemoveFromSortedList(double OneValue)
        {
            int i = FindIndex(OneValue);
            if (i < sortedList.Count && sortedList[i] == OneValue)
                sortedList.RemoveAt(i); //o(n)
        }
        private void AddToSortedList(double OneValue)
        {
            int i = FindIndex(OneValue);
            if (i >= sortedList.Count)
                sortedList.Add(OneValue);
            else
                sortedList.Insert(i, OneValue); //o(n)
        }
        private int FindIndex(double OneValue)
        {
            SortedSet<double> A;
            if (sortedList.Count == 0) return 0;
            int start = 0;
            int end = sortedList.Count - 1;
            int Med = (start + end) / 2;
            if (OneValue <= sortedList[start])
                return 0;
            if (OneValue > sortedList[end])
                return end + 1;
            if (OneValue == sortedList[end])
                return end;
            while(end-start>1)
            {
                Med = (start + end) / 2;
                if (OneValue == sortedList[Med]) return Med;
                if (OneValue < sortedList[Med])
                    end = Med;
                else
                    start = Med;
            }
            return end;
        }
    }
}
