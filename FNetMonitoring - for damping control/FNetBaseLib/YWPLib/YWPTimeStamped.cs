﻿/*******************************************************************************************
 * Base class all time series data structure.
 * Derived classes can to saved to YWPTimeSeriesBuffers and plot to .png picture with 'YWPTimeSeriesPlotter'
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib.YWPLib
{
    
    public class TimeStampedValue: TimeStamped
    {
        public double Value = 0;
        public static GetValue<TimeStampedValue> GetValueHandler = (A) => A.Value;
        public static GetValue<TimeStamped> GetValueHandler_Gen = (A) => ((TimeStampedValue)A).Value;
        public TimeStampedValue(DateTime timeStamp, double value)
        {
            TimeStamp = timeStamp;Value = value;
        }
    }
    public class TimeStampedInt:TimeStamped
    {
        public int Value = 0;
        public TimeStampedInt(DateTime timeStamp, int value)
        {
            TimeStamp = timeStamp; Value = value;
        }
    }
}
