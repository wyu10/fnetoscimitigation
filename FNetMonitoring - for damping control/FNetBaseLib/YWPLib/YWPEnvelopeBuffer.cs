﻿/*******************************************************************************************
 * This class is used to calculate envelope band height.
 * 1) Send data stream to this class by invoking Push(T)
 * 2) This class append the data to its upper and lower envelope line.
 * 3) Call GetEnvelopeHeigh(DateTime) to get the envelope band height.
 * Old data, according to 'BufferLenInSecond', will expires and be removed automatically
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib.YWPLib
{
    public class YWPEnvelopeBuffer<T> where T : TimeStamped
    {
        public YWPEnvelopeLineBuffer<T> UpperEnvelopeLine { get; set; }
        public YWPEnvelopeLineBuffer<T> LowerEnvelopeLine { get; set; }
        private IsAPriorToB<double> LargerIsPrior = (A, B) => A > B;
        private IsAPriorToB<double> SmallerIsPrior = (A, B) => A < B;
        private GetValue<T> _getValueHandler;
        /// <summary>
        /// Initial the object.
        /// </summary>
        /// <param name="ValuePerSec"></param>
        /// <param name="BufferLenInSecond"></param>
        /// <param name="ResolutionInSecond">Maximum length between two peaks. Major peaks are connected directly to get the envelope line,
        /// the minor peaks between major peaks are ignored. This parameter specifies the maximum distance between two major peaks.</param>
        /// <param name="GetValueHandler">How to get the value from the data structure.</param>
        public YWPEnvelopeBuffer(double ValuePerSec, double BufferLenInSecond, double ResolutionInSecond, GetValue<T> GetValueHandler)
        {
            _getValueHandler = GetValueHandler;
            UpperEnvelopeLine = new YWPEnvelopeLineBuffer<T>(ValuePerSec, BufferLenInSecond, ResolutionInSecond, LargerIsPrior, GetValueHandler);
            LowerEnvelopeLine = new YWPEnvelopeLineBuffer<T>(ValuePerSec, BufferLenInSecond, ResolutionInSecond, SmallerIsPrior, GetValueHandler);
        }
        public void Clear()
        {
            UpperEnvelopeLine.Clear();
            LowerEnvelopeLine.Clear();
        }
        public void Push(T OneData)
        {
            UpperEnvelopeLine.Push(OneData);
            LowerEnvelopeLine.Push(OneData);
        }
        public void RemoveOldData(DateTime OldTimeThreshold)
        {
            UpperEnvelopeLine.RemoveOldData(OldTimeThreshold);
            LowerEnvelopeLine.RemoveOldData(OldTimeThreshold);
        }
        public double GetEnvelopeHeigh(DateTime Time)
        {
            double up = 0, low = 0;
            if ((UpperEnvelopeLine.GetValueByTime(Time, _getValueHandler, out up) & (FindValueResult.Original | FindValueResult.Interpolation)) > 0
                && (LowerEnvelopeLine.GetValueByTime(Time, _getValueHandler, out low) & (FindValueResult.Original | FindValueResult.Interpolation)) > 0)
                return Math.Abs(up - low);
            return 0;
        }
        public double GetEnvelopeHeigh(DateTime Time, out double Center)
        {
            double up = 0, low = 0;
            if ((UpperEnvelopeLine.GetValueByTime(Time, _getValueHandler, out up) & (FindValueResult.Original | FindValueResult.Interpolation)) > 0
                && (LowerEnvelopeLine.GetValueByTime(Time, _getValueHandler, out low) & (FindValueResult.Original | FindValueResult.Interpolation)) > 0)
            {
                Center = (up + low) / 2;
                return Math.Abs(up - low);
            }
            Center = 0;
            return 0;
        }
    }
}
