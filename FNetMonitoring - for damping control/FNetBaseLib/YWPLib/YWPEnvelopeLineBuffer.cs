﻿/*******************************************************************************************
 * This class is used to calculate envelope line.
 * 1) Send data stream to this class by invoking Push(T)
 * 2) Only peak values of envelope line will be added to the buffer.
 * 
 * This class could get upper or lower envelope line with different 'CompareHandler' 
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib.YWPLib
{
    public class YWPEnvelopeLineBuffer<T>:YWPTimeSeriesBuffer<T> where T:TimeStamped
    {
        private IsAPriorToB<double> _isAPriorToB;
        private GetValue<T> _getValue;
        private double _resolutionInSecond;
        private YWPSlideWindowForMax<T> _slideWindowForMax;
        private double _windowRatioForMax = 0.5;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ValuePerSec"></param>
        /// <param name="BufferLenInSecond"></param>
        /// <param name="ResolutionInSecond">refer to class YWPEnvelopeBuffer</param>
        /// <param name="CompareHandler"></param>
        /// <param name="GetValueHandler"></param>
        public YWPEnvelopeLineBuffer(double ValuePerSec, double BufferLenInSecond, double ResolutionInSecond, IsAPriorToB<double> CompareHandler, GetValue<T> GetValueHandler) :base(ValuePerSec,BufferLenInSecond)
        {
            _isAPriorToB = CompareHandler;
            _getValue = GetValueHandler;
            _resolutionInSecond = ResolutionInSecond;
            _slideWindowForMax = new YWPSlideWindowForMax<T>(ValuePerSec, _windowRatioForMax * ResolutionInSecond, CompareHandler, GetValueHandler);
        }
        /// <summary>
        /// Put data stream in, and only the envelope line will be saved.
        /// </summary>
        public new void Push(T OneData)
        {
            _slideWindowForMax.Push(OneData);
            T Max = _slideWindowForMax.Max;
            int inside = _slideWindowForMax.IsInside(Max.TimeStamp);
            if (inside == 0) return;    //if Max is on the boundary of the buffer, may not be a extremum
            if (Count > 0 && Max.TimeStamp.CompareTo(Head.TimeStamp) <= 0) return;  //if Max alwary exists in the buffer
            while (Count >= 2)
            {
                T HeadPre = this[Count - 2];
                if (Max.TimeStamp.Subtract(HeadPre.TimeStamp).TotalSeconds > _resolutionInSecond) break;
                double H = base.GetInterpolationByTime(HeadPre, Max, Head.TimeStamp, _getValue);
                if (_isAPriorToB(H, _getValue(Head))) PopHead();
                else break;                
            }
            base.Push(Max);
        }
        public void Clear()
        {
            base.Clear();
            _slideWindowForMax.Clear();
        }
    }
}
