﻿/*******************************************************************************************
 * This class is used to get the maximum value of a sliding window on a data stream.
 * The buffer is implemented based on YWPTimeSeriesBuffer with the following requirements:
 * 
 * 1) Time complexity O(1) to get the maximum value.
 * 2) Average time complex O(1) to add/remove a value.
 * 
 * It could get maximum or minimum value with different 'CompareHandler'
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;

namespace FNetBaseLib.YWPLib
{
    
    public class YWPSlideWindowForMax<T>:YWPTimeSeriesBuffer<T> where T:TimeStamped
    {        
        private IsAPriorToB<double> _isAPriorToB;
        private GetValue<T> _getValue;
        public YWPSlideWindowForMax(double ValuePerSec, double WindowLenInSecond, IsAPriorToB<double> CompareHandler, GetValue<T> GetValueHandler) :base(ValuePerSec, WindowLenInSecond)
        {
            _isAPriorToB = CompareHandler;
            _getValue = GetValueHandler;
        }
        public new void Push(T OneData)
        {
            while (Count > 0 && _isAPriorToB(_getValue(OneData), _getValue(Head)))
                PopHead();
            base.Push(OneData);
        }
        /// <summary>
        /// Return a integer to indicate if the T is inside buffer's time range. 0: on the boundary; >0 :inside <0: outside
        /// </summary>
        public new int IsInside(DateTime Time)
        {
            if (Count <= 0) return -1;
            double thres = 0.5 / ValuePerSecond;
            double distance = Time.Subtract(Head.TimeStamp).TotalSeconds;
            if (distance <= 0 && distance > -1 * thres) return 0;
            distance = Time.Subtract(Head.TimeStamp.AddSeconds(-1* BuffLenInSecond)).TotalSeconds;
            if (distance >= 0 && distance < thres) return 0;
            return -1 * Time.CompareTo(Head.TimeStamp) * Time.CompareTo(Head.TimeStamp.AddSeconds(-1 * BuffLenInSecond));
        }
        public T Max { get { return Tail; } }
        public double MaxValue
        {
            get
            {
                if (Max != null)
                    return _getValue(Max);
                else
                    return 0;
            }
        }
    }
}
