﻿/*******************************************************************************************
 * This class is used to plot time series and save it to .png file. 
 * The current version is dedicated for Foreced Oscillation Trigger. In the future I may add
 * some general functions for other programs to plot any time series.
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;

namespace FNetBaseLib.YWPLib
{
    public class YWPTimeSeriesPlotter
    {
        /*public static void CreateGeneralPlotImg(string imgFileName, string TitleName, List<Tuple<IEnumerable<TimeStamped>, GetValue<TimeStamped>, string>> TimeSeriesData, int ImgWidth, int ImgHeight)
        {
            Chart OneChart = new Chart();
            OneChart.Width = ImgWidth;
            OneChart.Height = ImgHeight;
            OneChart.ChartAreas.Add(new ChartArea("1"));
            OneChart.Legends.Add(new Legend("1"));
            OneChart.Legends[0].DockedToChartArea = "1";
            OneChart.Legends[0].IsDockedInsideChartArea = false;
            OneChart.Legends[0].BackColor = Color.FromArgb(250, 250, 250);// Color.LightGray;
            OneChart.Titles.Clear();
            OneChart.Titles.Add(TitleName).Text = TitleName;
            OneChart.Titles[0].Font = new Font("Arial", 16);
            double Max = double.MinValue;
            double Min = double.MaxValue;
            //Distribution MyDis = new Distribution(59.5, 60.5, 200, "");
            foreach (var v in TimeSeriesData)
            {
                string seriesName = v.Item3;
                var s = OneChart.Series.Add(seriesName);
                s.Name = seriesName;
                s.Label = seriesName;
                s.ChartArea = "1";
                s.BorderWidth = 1;
                s.XValueType = ChartValueType.DateTime;
                s.ChartType = SeriesChartType.FastLine;
                foreach (var T in v.Item1)
                {
                    s.Points.AddXY(T.TimeStamp, v.Item2(T));
                    if (v.Item2(T) > Max) Max = v.Item2(T);
                    if (v.Item2(T) < Min) Min = v.Item2(T);
                }
            }
            //-----Configure the frequency chart------
            OneChart.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm:ss";
            OneChart.ChartAreas[0].AxisY.LabelStyle.Format = "F3";
            OneChart.ChartAreas[0].AxisY.Minimum = Math.Round(Min - 0.0005, 3);
            OneChart.ChartAreas[0].AxisY.Maximum = Math.Round(Max + 0.0005, 3);
            OneChart.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.LightGray;
            OneChart.ChartAreas[0].AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            OneChart.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;
            OneChart.ChartAreas[0].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            OneChart.ChartAreas[0].AxisX.MinorGrid.Interval = 1;
            OneChart.ChartAreas[0].AxisX.MinorGrid.LineColor = Color.FromArgb(239, 239, 239);
            OneChart.ChartAreas[0].AxisX.MinorGrid.LineDashStyle = ChartDashStyle.Dash;
            OneChart.ChartAreas[0].AxisX.MinorGrid.Enabled = true;
            OneChart.ChartAreas[0].AxisX.MinorGrid.LineWidth = 1;
            OneChart.ChartAreas[0].AxisX.MinorGrid.IntervalType = DateTimeIntervalType.Seconds;
            OneChart.ChartAreas[0].AxisY.LineWidth = 1;
            OneChart.ChartAreas[0].AxisX.LineWidth = 1;
            //OneChart.ChartAreas[i].AxisY.Title = "Frequency (Hz)";
            //OneChart.ChartAreas[0].AxisY.TitleFont = new Font("Arial", 12);
            //OneChart.ChartAreas[i].AxisY.TitleForeColor = Color.Blue;
            OneChart.ChartAreas[0].AxisX.Title = "Time";
            OneChart.ChartAreas[0].AxisX.TitleFont = new Font("Arial", 12);
            OneChart.ChartAreas[0].AxisX.TitleForeColor = Color.Blue;
            //OneChart.ChartAreas[i].AxisY.Interval = (Max - Min > 0.1 ? 0.01 : 0.005);
            OneChart.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Seconds;
            OneChart.ChartAreas[0].AxisX.Interval = 5;
            //------Save Image -----
            OneChart.SaveImage(imgFileName, ChartImageFormat.Png);
        }*/

        private static YWPLogTypeCFG PlotLogCFG = null;// new YWPLogTypeCFG("PlotLog", "", true, true, YWPLogTypeCFG.ExpireType.Daily);
        public static string CreateMultiPlotImg(
            string imgFileName,
            string TitleName,
            List<Tuple<string, List<Tuple<IEnumerable<TimeStamped>, GetValue<TimeStamped>, string>>>> TimeSeriesGroup,
            int ImgWidth,
            int OnePlotHeight,
            DateTime VerticalLineTime,
            DateTime StartTime,
            List<Tuple<string, List<Tuple<IEnumerable<Tuple<double, double>>, string>>>> XYDataGroup
            )
        {
            return CreateMultiPlotImg(imgFileName, TitleName, TimeSeriesGroup, ImgWidth, OnePlotHeight, VerticalLineTime, StartTime, XYDataGroup, false, 0, "");
        }
        
        public static string CreateMultiPlotImg(
            string imgFileName,
            string TitleName,
            List<YWPPlotCFG> TimeSeriesGroup,
            int ImgWidth,
            int OnePlotHeight,
            DateTime VerticalLineTime,
            DateTime StartTime,
            List<Tuple<string, List<Tuple<IEnumerable<Tuple<double, double>>, string>>>> XYDataGroup,
            bool IgnoreZero = false,
            int TimeLengthInSec = 0,
            string BGImg = ""
            )
        {
            StringBuilder SB = new StringBuilder();
            DateTime EndTime = DateTime.MaxValue;
            if (TimeLengthInSec > 0)
                EndTime = StartTime.AddSeconds(TimeLengthInSec);
            try
            {
                int YBinCount = OnePlotHeight / 35;
                int XBinCount = ImgWidth / 60;
                Chart OneChart = new Chart();
                OneChart.Width = ImgWidth;
                double TitleHeight = 0.08;
                int ChartCount = 0;
                if (TimeSeriesGroup != null)
                    ChartCount = TimeSeriesGroup.Count;
                if (XYDataGroup != null) ChartCount += XYDataGroup.Count;
                if (ChartCount < 1)
                    return "No Data";
                OneChart.Height = (int)((ChartCount + TitleHeight) * OnePlotHeight);
                if (ChartCount >= 6)
                    OneChart.Height = (int)(((ChartCount + 1) / 2 + TitleHeight) * OnePlotHeight);
                OneChart.Titles.Clear();
                OneChart.Titles.Add(TitleName).Text = TitleName;
                OneChart.Titles[0].Font = new Font("Arial", 10);
                OneChart.Titles[0].Position.X = 50;
                OneChart.Titles[0].Position.Y = 0;
                OneChart.Titles[0].Alignment = ContentAlignment.TopCenter;
                int i;
                int TimeSeriesCount = 0;
                if (TimeSeriesGroup != null)
                    TimeSeriesCount = TimeSeriesGroup.Count;
                for (i = 0; i < TimeSeriesCount; ++i)
                {
                    try
                    {
                        var TimeSeriesData = TimeSeriesGroup[i].MultipleDataSeries;
                        if (TimeSeriesData == null || TimeSeriesData.Count == 0) continue;
                        //YWPDistribution MyDis = new YWPDistribution(59.5, 60.5, 200, "");
                        string ChartAreaName = i.ToString();
                        ChartArea ThisChartArea = new ChartArea(ChartAreaName);
                        Legend ThisLegend = new Legend(ChartAreaName);
                        OneChart.ChartAreas.Add(ThisChartArea);
                        OneChart.Legends.Add(ThisLegend);
                        ThisLegend.DockedToChartArea = ChartAreaName;
                        ThisLegend.IsDockedInsideChartArea = false;
                        ThisLegend.Docking = Docking.Right;
                        ThisLegend.BackColor = Color.FromArgb(250, 250, 250);// Color.LightGray;  
                        ThisLegend.LegendStyle = LegendStyle.Column;                       
                        string YTitle = TimeSeriesGroup[i].PlotName;
                        double Max = double.MinValue;
                        double Min = double.MaxValue;
                        SB.AppendLine("Series count: " + TimeSeriesData.Count.ToString());
                        if (ChartCount < 6)
                        {
                            ThisChartArea.Position.X = 0;
                            ThisChartArea.Position.Y = (float)(100.0 / (ChartCount+TitleHeight) * (i + TitleHeight));
                            ThisChartArea.Position.Width = 88;
                            ThisChartArea.Position.Height = (float)(100.0 / (ChartCount + TitleHeight));
                        }
                        else
                        {
                            ThisChartArea.Position.X = i % 2 * 50;
                            ThisChartArea.Position.Y = (float)(100.0 / ((ChartCount + 1) / 2 + TitleHeight) * (i / 2 + TitleHeight));
                            ThisChartArea.Position.Width = 43;
                            ThisChartArea.Position.Height = (float)(100.0 / ((ChartCount + 1) / 2 + TitleHeight));
                        }
                        if (BGImg != "")
                        {
                            ThisChartArea.BackImage = BGImg;
                            ThisChartArea.BackImageWrapMode = ChartImageWrapMode.Unscaled;
                            ThisChartArea.BackImageAlignment = ChartImageAlignmentStyle.BottomRight;
                        }
                        foreach (var v in TimeSeriesData)
                        {
                            string seriesName = v.DataSeriesName;
                            Series s = null;
                            try { s = OneChart.Series.Add(seriesName); }
                            catch (Exception eee) { continue; }
                            s.Name = seriesName;
                            s.Label = seriesName;
                            s.ChartArea = ChartAreaName;
                            s.Legend = ChartAreaName;
                            s.BorderWidth = 1;
                            s.XValueType = ChartValueType.DateTime;
                            s.ChartType = SeriesChartType.FastLine;
                            foreach (var T in v.DataSeries)
                            {
                                if (T == null || T.TimeStamp.CompareTo(StartTime) < 0 || T.TimeStamp.CompareTo(EndTime) > 0)
                                    continue;
                                var value = v.MethodToGetValue(T);
                                if (IgnoreZero && value == 0) continue;
                                int index = s.Points.AddXY(T.TimeStamp, value);
                                if (value > Max) Max = value;
                                if (value < Min) Min = value;
                            }
                        }
                        //-----Configure the frequency chart------ 
                        ThisChartArea.AxisX.LabelStyle.Format = "yyyy-MM-dd HH:mm:ss";
                        ThisChartArea.AxisY.LabelStyle.Format = "F4";
                        double NewMax, NewMin, Interval;
                        if (Max == Min) { Max += 1; Min -= 1; }
                        //GetRangeAndInterval(Max, Min, YBinCount, out NewMax, out NewMin, out Interval);
                        GetRangeAndInterval(TimeSeriesGroup[i], 0.0005, Max, Min, YBinCount, out NewMax, out NewMin, out Interval);
                        ThisChartArea.AxisY.Minimum = NewMin;
                        ThisChartArea.AxisY.Maximum = NewMax;
                        ThisChartArea.AxisY.Interval = Interval;
                        ThisChartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
                        ThisChartArea.AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                        ThisChartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
                        ThisChartArea.AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                        //ThisChartArea.AxisX.MinorGrid.Interval = 1;
                        ThisChartArea.AxisX.MinorGrid.Enabled = true;
                        ThisChartArea.AxisX.MinorGrid.LineColor = Color.FromArgb(235, 235, 235);
                        ThisChartArea.AxisX.MinorGrid.LineDashStyle = ChartDashStyle.Dash;
                        ThisChartArea.AxisX.MinorGrid.LineWidth = 1;
                        ThisChartArea.AxisX.MinorGrid.IntervalType = DateTimeIntervalType.Seconds;
                        ThisChartArea.AxisY.LineWidth = 1;
                        ThisChartArea.AxisX.LineWidth = 1;
                        ThisChartArea.AxisY.Title = YTitle;
                        ThisChartArea.AxisY.TitleFont = new Font("Arial", 12);
                        ThisChartArea.AxisY.TitleForeColor = Color.Blue;
                        ThisChartArea.AxisX.ScaleView.Position = 0.5;

                        ThisChartArea.AxisX.IntervalType = DateTimeIntervalType.Seconds;
                        if (VerticalLineTime != DateTime.MinValue)
                        {
                            StripLine OneStrip = new StripLine();
                            OneStrip.IntervalType = DateTimeIntervalType.Days;
                            OneStrip.Interval = 1;
                            OneStrip.IntervalOffsetType = DateTimeIntervalType.Seconds;
                            OneStrip.IntervalOffset = VerticalLineTime.TimeOfDay.TotalSeconds;
                            OneStrip.StripWidthType = DateTimeIntervalType.Seconds;
                            OneStrip.StripWidth = 1;
                            OneStrip.BackColor = Color.LightGray;
                            ThisChartArea.AxisX.StripLines.Add(OneStrip);
                        }
                    }
                    catch (Exception ee)
                    {
                        SB.AppendLine("error occurs when plotting. " + ee.Message + ". Stack:" + ee.StackTrace);
                    }
                }
                if (XYDataGroup != null)
                {
                    for (int j = i; j < XYDataGroup.Count + i; ++j)
                    {
                        try
                        {
                            //break;
                            var TimeSeriesData = XYDataGroup[j - i].Item2;
                            if (TimeSeriesData == null || TimeSeriesData.Count == 0) continue;
                            string ChartAreaName = j.ToString();
                            ChartArea ThisChartArea = new ChartArea(ChartAreaName);
                            Legend ThisLegend = new Legend(ChartAreaName);
                            OneChart.ChartAreas.Add(ThisChartArea);
                            OneChart.Legends.Add(ThisLegend);
                            ThisLegend.DockedToChartArea = ChartAreaName;
                            ThisLegend.IsDockedInsideChartArea = false;
                            ThisLegend.Docking = Docking.Right;
                            ThisLegend.BackColor = Color.FromArgb(250, 250, 250);// Color.LightGray;  
                            ThisLegend.LegendStyle = LegendStyle.Column;
                            string YTitle = XYDataGroup[j - i].Item1;
                            double Max = double.MinValue;
                            double Min = double.MaxValue;
                            double XMax = double.MinValue;
                            double XMin = double.MaxValue;
                            foreach (var v in TimeSeriesData)
                            {
                                string seriesName = v.Item2;
                                var s = OneChart.Series.Add(seriesName);
                                s.Name = seriesName;
                                s.Label = seriesName;
                                s.ChartArea = ChartAreaName;
                                s.Legend = ChartAreaName;
                                s.BorderWidth = 1;
                                s.XValueType = ChartValueType.Auto;
                                s.ChartType = SeriesChartType.FastLine;
                                foreach (var T in v.Item1)
                                {
                                    s.Points.AddXY(T.Item1, T.Item2);
                                    if (T.Item2 > Max) Max = T.Item2;
                                    if (T.Item2 < Min) Min = T.Item2;
                                    if (T.Item1 > XMax) XMax = T.Item1;
                                    if (T.Item1 < XMin) XMin = T.Item1;
                                }
                            }
                            //-----Configure the frequency chart------                
                            ThisChartArea.Position.X = 0;
                            ThisChartArea.Position.Y = (float)(100.0 / ChartCount * (j + TitleHeight));
                            ThisChartArea.Position.Width = 85;
                            ThisChartArea.Position.Height = (float)(100.0 / ChartCount);
                            //ThisChartArea.AxisX.LabelStyle.Format = "HH:mm:ss";
                            ThisChartArea.AxisY.LabelStyle.Format = "F4";
                            double NewMax, NewMin, Interval;
                            GetRangeAndInterval(Max, Min, YBinCount, out NewMax, out NewMin, out Interval);
                            ThisChartArea.AxisY.Minimum = NewMin;
                            ThisChartArea.AxisY.Maximum = NewMax;
                            ThisChartArea.AxisY.Interval = Interval;
                            GetRangeAndInterval(XMax, XMin, XBinCount, out NewMax, out NewMin, out Interval);
                            ThisChartArea.AxisX.Minimum = NewMin;
                            ThisChartArea.AxisX.Maximum = NewMax;
                            ThisChartArea.AxisX.Interval = Interval;
                            ThisChartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
                            ThisChartArea.AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                            ThisChartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
                            ThisChartArea.AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                            ThisChartArea.AxisX.MinorGrid.Enabled = true;
                            ThisChartArea.AxisX.MinorGrid.LineColor = Color.FromArgb(235, 235, 235);
                            ThisChartArea.AxisX.MinorGrid.LineDashStyle = ChartDashStyle.Dash;
                            ThisChartArea.AxisX.MinorGrid.LineWidth = 1;
                            ThisChartArea.AxisY.LineWidth = 1;
                            ThisChartArea.AxisX.LineWidth = 1;
                            ThisChartArea.AxisY.Title = YTitle;
                            ThisChartArea.AxisY.TitleFont = new Font("Arial", 12);
                            ThisChartArea.AxisY.TitleForeColor = Color.Blue;
                            if (BGImg != "" && System.IO.File.Exists(BGImg))
                            {
                                ThisChartArea.BackImage = BGImg;
                                ThisChartArea.BackImageWrapMode = ChartImageWrapMode.Unscaled;
                                ThisChartArea.BackImageAlignment = ChartImageAlignmentStyle.BottomRight;
                            }
                        }
                        catch (Exception ee)
                        {
                            SB.AppendLine("error occurs when plotting XY. " + ee.Message + ". Stack:" + ee.StackTrace);
                        }
                    }
                }

                //------Save Image -----
                string folder = imgFileName.Substring(0, imgFileName.LastIndexOf('\\') + 1);
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);
                OneChart.SaveImage(imgFileName, ChartImageFormat.Png);
                OneChart.ChartAreas.Clear();

            }
            catch (Exception e)
            {
                SB.AppendLine("error occurs when plotting. " + e.Message + ". Stack:" + e.StackTrace);
            }
            return SB.ToString();
        }
        public static string CreateMultiPlotImg(
            string imgFileName, 
            string TitleName, 
            List<Tuple<string, List<Tuple<IEnumerable<TimeStamped>, GetValue<TimeStamped>, string>>>> TimeSeriesGroup, 
            int ImgWidth, 
            int OnePlotHeight, 
            DateTime VerticalLineTime,
            DateTime StartTime, 
            List<Tuple<string, List<Tuple<IEnumerable<Tuple<double, double>>, string>>>> XYDataGroup, 
            bool IgnoreZero = false,
            int TimeLengthInSec=0,
            string BGImg=""
            )
        {
            if (PlotLogCFG != null)
                YWPLog.WriteDefaultLog("------------Start to plot------------", PlotLogCFG);
            int TwoPlotOneRow = 10;
            StringBuilder SB = new StringBuilder();
            //StringBuilder SB_Data = new StringBuilder();
            DateTime EndTime = DateTime.MaxValue;
            if (TimeLengthInSec > 0)
                EndTime = StartTime.AddSeconds(TimeLengthInSec);
            try
            {
                Chart OneChart = new Chart();
                double TitleHeight = 0.08;
                int ChartCount = 0;
                if (TimeSeriesGroup != null)
                    ChartCount = TimeSeriesGroup.Count;
                if (XYDataGroup != null) ChartCount += XYDataGroup.Count;
                if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\tChart Count = " + ChartCount.ToString(), PlotLogCFG);
                if (ChartCount < 1)
                {
                    if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\tNo Data, chart count<1. Return.", PlotLogCFG);
                    return "No Data";
                }
                OneChart.Height = (int)((ChartCount + TitleHeight) * OnePlotHeight);
                if (ChartCount >= TwoPlotOneRow)
                {
                    ImgWidth = (int)(ImgWidth * 1.5);
                    OneChart.Height = (int)(((ChartCount + 1) / 2 + TitleHeight) * OnePlotHeight);
                }

                int YBinCount = OnePlotHeight / 35;
                int XBinCount = ImgWidth / 60;
                
                OneChart.Width = ImgWidth;
                
                
                OneChart.Titles.Clear();
                OneChart.Titles.Add(TitleName).Text = TitleName;
                OneChart.Titles[0].Font = new Font("Arial", 10);
                OneChart.Titles[0].Position.X = 50;
                OneChart.Titles[0].Position.Y = 0;
                OneChart.Titles[0].Alignment = ContentAlignment.TopCenter;
                int i;
                int TimeSeriesCount = 0;
                if (TimeSeriesGroup != null)
                    TimeSeriesCount = TimeSeriesGroup.Count;
                for (i = 0; i < TimeSeriesCount; ++i)
                {
                    try
                    {
                        if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\t--- Start to plot chart " + i.ToString(), PlotLogCFG);
                        var TimeSeriesData = TimeSeriesGroup[i].Item2;
                        if (TimeSeriesData == null || TimeSeriesData.Count == 0)
                        {
                            if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\t\t\tNo data series is found. Continue to plot next chart.", PlotLogCFG);
                            continue;
                        }
                        //YWPDistribution MyDis = new YWPDistribution(59.5, 60.5, 200, "");
                        string ChartAreaName = i.ToString();
                        ChartArea ThisChartArea = new ChartArea(ChartAreaName);
                        Legend ThisLegend = new Legend(ChartAreaName);
                        OneChart.ChartAreas.Add(ThisChartArea);
                        OneChart.Legends.Add(ThisLegend);
                        ThisLegend.DockedToChartArea = ChartAreaName;
                        ThisLegend.IsDockedInsideChartArea = false;
                        ThisLegend.Docking = Docking.Right;
                        ThisLegend.BackColor = Color.FromArgb(250, 250, 250);// Color.LightGray;  
                        ThisLegend.LegendStyle = LegendStyle.Column;
                        //SB.AppendLine("Plot " + i.ToString() + "th picture.");
                        //ForcedOscillationGlobalInfo.G.Log("Plot " + i.ToString() + "th picture.");                        
                        string YTitle = TimeSeriesGroup[i].Item1;
                        double Max = double.MinValue;
                        double Min = double.MaxValue;
                        SB.AppendLine("Series count: " + TimeSeriesData.Count.ToString());
                        if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\t\t\tSeries count: " + TimeSeriesData.Count.ToString(), PlotLogCFG);
                        //ForcedOscillationGlobalInfo.G.Log("Series count: " + TimeSeriesData.Count.ToString());
                        if (ChartCount < TwoPlotOneRow)
                        {
                            ThisChartArea.Position.X = 0;
                            ThisChartArea.Position.Y = (float)(100.0 / ChartCount * (i + TitleHeight));
                            ThisChartArea.Position.Width = 88;
                            ThisChartArea.Position.Height = (float)(100.0 / ChartCount);
                        }
                        else
                        {
                            ThisChartArea.Position.X = i % 2 * 50;
                            ThisChartArea.Position.Y = (float)(100.0 / ((ChartCount+1)/2) * (i/2 + TitleHeight));
                            ThisChartArea.Position.Width = 43;
                            ThisChartArea.Position.Height = (float)(100.0 / ((ChartCount + 1) / 2));

                        }
                        if (BGImg != "")
                        {
                            ThisChartArea.BackImage = BGImg;
                            ThisChartArea.BackImageWrapMode = ChartImageWrapMode.Unscaled;
                            ThisChartArea.BackImageAlignment = ChartImageAlignmentStyle.BottomRight;
                        }
                        int c = 0;
                        foreach (var v in TimeSeriesData)
                        {
                            string seriesName = v.Item3;
                            Series s = null;
                            try
                            {
                                s = OneChart.Series.Add(seriesName);
                            }
                            catch(Exception eee)
                            {
                                if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\t\t\tException caught when add data to chart series. Continue to add next data series of this chart.\r\nMsg:"+eee.Message+"\r\n stack:"+eee.StackTrace, PlotLogCFG);
                                continue;
                            }
                            s.Name = seriesName;
                            s.Label = seriesName;
                            s.ChartArea = ChartAreaName;
                            s.Legend = ChartAreaName;
                            s.BorderWidth = 1;
                            s.XValueType = ChartValueType.DateTime;
                            s.ChartType = SeriesChartType.FastLine;
                            //SB.AppendLine(seriesName);
                            //SB_Data.AppendLine("Start/End Time should be:" + StartTime.ToString("HH:mm:ss.fff") + "," + EndTime.ToString("HH:mm:ss.fff"));
                            //SB_Data.AppendLine(seriesName);
                            foreach (var T in v.Item1)
                            {
                                //SB_Data.AppendLine("\t\t" + T.TimeStamp.ToString("HH:mm:ss.fff") + ", " + v.Item2(T).ToString("F6"));
                                if (T==null || T.TimeStamp.CompareTo(StartTime) < 0||T.TimeStamp.CompareTo(EndTime)>0) continue;
                                if (IgnoreZero && v.Item2(T) == 0) continue;
                                int index = s.Points.AddXY(T.TimeStamp, v.Item2(T));
                                //SB.Append("[" + T.TimeStamp.ToString("HH:mm:ss.fff") +","+ v.Item2(T).ToString("F3") + "]\t");
                                if (v.Item2(T) > Max) Max = v.Item2(T);
                                if (v.Item2(T) < Min) Min = v.Item2(T);   
                            }
                            c += 1;
                            //if (PlotLogCFG != null) YWPLog.WriteDefaultLog(SB_Data.ToString(), PlotLogCFG);
                            //SB_Data.Clear();
                        }
                        if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\t\t\tAdded data series count = " + c.ToString(), PlotLogCFG);
                        //-----Configure the frequency chart------                
                        //ThisChartArea.Position.X = 0;
                        //ThisChartArea.Position.Y = (float)(100.0 / ChartCount * (i + TitleHeight));
                        //ThisChartArea.Position.Width = 85;
                        //ThisChartArea.Position.Height = (float)(100.0 / ChartCount);
                        ThisChartArea.AxisX.LabelStyle.Format = "yyyy-MM-dd HH:mm:ss";
                        ThisChartArea.AxisY.LabelStyle.Format = "F4";
                        double NewMax, NewMin, Interval;
                        if (Max == Min) { Max += 1; Min -= 1; }
                        //GetRangeAndInterval(Max, Min, YBinCount, out NewMax, out NewMin, out Interval);
                        if (PlotLogCFG != null) YWPLog.WriteDefaultLog(string.Format("\t\t\t\tChart value range is [{0},{1}].", Min, Max), PlotLogCFG);
                        GetRangeAndInterval(TimeSeriesData, 0.0005, Max, Min, YBinCount, out NewMax, out NewMin, out Interval);
                        if (PlotLogCFG != null) YWPLog.WriteDefaultLog(string.Format("\t\t\t\tChart scale is determined. Before: [{0},{1}]. Result:[{2}:{3}:{4}].", Min,Max,NewMin,Interval,NewMax), PlotLogCFG);
                        ThisChartArea.AxisY.Minimum = NewMin;
                        ThisChartArea.AxisY.Maximum = NewMax;
                        ThisChartArea.AxisY.Interval = Interval;
                        ThisChartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
                        ThisChartArea.AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                        ThisChartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
                        ThisChartArea.AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                        //ThisChartArea.AxisX.MinorGrid.Interval = 1;
                        ThisChartArea.AxisX.MinorGrid.Enabled = true;
                        ThisChartArea.AxisX.MinorGrid.LineColor = Color.FromArgb(235, 235, 235);
                        ThisChartArea.AxisX.MinorGrid.LineDashStyle = ChartDashStyle.Dash;
                        ThisChartArea.AxisX.MinorGrid.LineWidth = 1;
                        ThisChartArea.AxisX.MinorGrid.IntervalType = DateTimeIntervalType.Seconds;
                        ThisChartArea.AxisY.LineWidth = 1;
                        ThisChartArea.AxisX.LineWidth = 1;
                        ThisChartArea.AxisY.Title = YTitle;
                        ThisChartArea.AxisY.TitleFont = new Font("Arial", 12);
                        ThisChartArea.AxisY.TitleForeColor = Color.Blue;
                        ThisChartArea.AxisX.ScaleView.Position = 0.5;
                        //if (i == TimeSeriesGroup.Count - 1)
                        //{
                        //    ThisChartArea.AxisX.Title = "UTC Time";
                        //    ThisChartArea.AxisX.TitleFont = new Font("Arial", 12);
                        //    ThisChartArea.AxisX.TitleForeColor = Color.Blue;
                        //}
                        ThisChartArea.AxisX.IntervalType = DateTimeIntervalType.Seconds;
                        StripLine OneStrip = new StripLine();
                        OneStrip.IntervalType = DateTimeIntervalType.Days;
                        OneStrip.Interval = 1;
                        OneStrip.IntervalOffsetType = DateTimeIntervalType.Seconds;
                        OneStrip.IntervalOffset = VerticalLineTime.TimeOfDay.TotalSeconds;
                        OneStrip.StripWidthType = DateTimeIntervalType.Seconds;
                        OneStrip.StripWidth = 1;
                        OneStrip.BackColor = Color.LightGray;
                        ThisChartArea.AxisX.StripLines.Add(OneStrip);
                        //if (System.IO.File.Exists("FOBackground.png"))
                        //{
                        //    ThisChartArea.BackImage = "FOBackground.png";
                        //    ThisChartArea.BackImageWrapMode = ChartImageWrapMode.Unscaled;
                        //    ThisChartArea.BackImageAlignment = ChartImageAlignmentStyle.BottomRight;
                        //}
                        if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\t\t\tChart configuration is done.", PlotLogCFG);

                    }
                    catch (Exception ee)
                    {
                        SB.AppendLine("error occurs when plotting. " + ee.Message + ". Stack:" + ee.StackTrace);
                        if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\t\t\tException caught while plot this chart. Msg:"+ee.Message+" stack:"+ee.StackTrace, PlotLogCFG);
                        //ForcedOscillationGlobalInfo.G.Log("error occurs when plotting. " + ee.Message + ". Stack:" + ee.StackTrace);
                    }
                }
                //----------------------------------------------------------------------
                if (XYDataGroup != null)
                {
                    
                    for (int j = i; j < XYDataGroup.Count + i; ++j)
                    {
                        if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\t--- Start to plot XY data chart "+j.ToString(), PlotLogCFG);
                        try
                        {
                            //break;
                            var TimeSeriesData = XYDataGroup[j - i].Item2;
                            if (TimeSeriesData == null || TimeSeriesData.Count == 0) continue;
                            string ChartAreaName = j.ToString();
                            ChartArea ThisChartArea = new ChartArea(ChartAreaName);
                            Legend ThisLegend = new Legend(ChartAreaName);
                            OneChart.ChartAreas.Add(ThisChartArea);
                            OneChart.Legends.Add(ThisLegend);
                            ThisLegend.DockedToChartArea = ChartAreaName;
                            ThisLegend.IsDockedInsideChartArea = false;
                            ThisLegend.Docking = Docking.Right;
                            ThisLegend.BackColor = Color.FromArgb(250, 250, 250);// Color.LightGray;  
                            ThisLegend.LegendStyle = LegendStyle.Column;
                            //FNetForcedOscillation.ForcedOscillationGlobalInfo.G.Log("Plot " + j.ToString() + "th picture.");
                            string YTitle = XYDataGroup[j - i].Item1;
                            double Max = double.MinValue;
                            double Min = double.MaxValue;
                            double XMax = double.MinValue;
                            double XMin = double.MaxValue;
                            //FNetForcedOscillation.ForcedOscillationGlobalInfo.G.Log("Series count: " + TimeSeriesData.Count.ToString());
                            foreach (var v in TimeSeriesData)
                            {
                                string seriesName = v.Item2;
                                var s = OneChart.Series.Add(seriesName);
                                s.Name = seriesName;
                                s.Label = seriesName;
                                s.ChartArea = ChartAreaName;
                                s.Legend = ChartAreaName;
                                s.BorderWidth = 1;
                                s.XValueType = ChartValueType.Auto;
                                s.ChartType = SeriesChartType.FastLine;
                                foreach (var T in v.Item1)
                                {
                                    s.Points.AddXY(T.Item1, T.Item2);
                                    if (T.Item2 > Max) Max = T.Item2;
                                    if (T.Item2 < Min) Min = T.Item2;
                                    if (T.Item1 > XMax) XMax = T.Item1;
                                    if (T.Item1 < XMin) XMin = T.Item1;
                                }
                            }
                            //-----Configure the frequency chart------                
                            ThisChartArea.Position.X = 0;
                            ThisChartArea.Position.Y = (float)(100.0 / ChartCount * (j + TitleHeight));
                            ThisChartArea.Position.Width = 85;
                            ThisChartArea.Position.Height = (float)(100.0 / ChartCount);
                            //ThisChartArea.AxisX.LabelStyle.Format = "HH:mm:ss";
                            ThisChartArea.AxisY.LabelStyle.Format = "F4";
                            double NewMax, NewMin, Interval;
                            GetRangeAndInterval(Max, Min, YBinCount, out NewMax, out NewMin, out Interval);
                            ThisChartArea.AxisY.Minimum = NewMin;
                            ThisChartArea.AxisY.Maximum = NewMax;
                            ThisChartArea.AxisY.Interval = Interval;
                            GetRangeAndInterval(XMax, XMin, XBinCount, out NewMax, out NewMin, out Interval);
                            ThisChartArea.AxisX.Minimum = NewMin;
                            ThisChartArea.AxisX.Maximum = NewMax;
                            ThisChartArea.AxisX.Interval = Interval;
                            ThisChartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
                            ThisChartArea.AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                            ThisChartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
                            ThisChartArea.AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                            ThisChartArea.AxisX.MinorGrid.Enabled = true;
                            ThisChartArea.AxisX.MinorGrid.LineColor = Color.FromArgb(235, 235, 235);
                            ThisChartArea.AxisX.MinorGrid.LineDashStyle = ChartDashStyle.Dash;
                            ThisChartArea.AxisX.MinorGrid.LineWidth = 1;
                            ThisChartArea.AxisY.LineWidth = 1;
                            ThisChartArea.AxisX.LineWidth = 1;
                            ThisChartArea.AxisY.Title = YTitle;
                            ThisChartArea.AxisY.TitleFont = new Font("Arial", 12);
                            ThisChartArea.AxisY.TitleForeColor = Color.Blue;
                            if (BGImg != "")
                            {
                                ThisChartArea.BackImage = BGImg;
                                ThisChartArea.BackImageWrapMode = ChartImageWrapMode.Unscaled;
                                ThisChartArea.BackImageAlignment = ChartImageAlignmentStyle.BottomRight;
                            }
                            //if (System.IO.File.Exists("FOBackground.png"))
                            //{
                            //    ThisChartArea.BackImage = "FOBackground.png";
                            //    ThisChartArea.BackImageWrapMode = ChartImageWrapMode.Unscaled;
                            //    ThisChartArea.BackImageAlignment = ChartImageAlignmentStyle.BottomRight;
                            //}
                        }
                        catch (Exception ee)
                        {
                            SB.AppendLine("error occurs when plotting XY. " + ee.Message + ". Stack:" + ee.StackTrace);
                            if (PlotLogCFG != null) YWPLog.WriteDefaultLog("\t\t\t\terror occurs when plotting XY. " + ee.Message + ". Stack:" + ee.StackTrace, PlotLogCFG);
                        }
                    }
                }
                //------Save Image -----
                string folder = imgFileName.Substring(0, imgFileName.LastIndexOf('\\') + 1);
                if (PlotLogCFG != null) YWPLog.WriteDefaultLog("Check folder. ", PlotLogCFG);
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);

                //if (System.IO.File.Exists(imgFileName))
                //    imgFileName += DateTime.Now.Millisecond.ToString() + ".png";
                OneChart.SaveImage(imgFileName, ChartImageFormat.Png);
                OneChart.ChartAreas.Clear();
                if (PlotLogCFG != null) YWPLog.WriteDefaultLog("Image is saved. Done.", PlotLogCFG);

            }
            catch (Exception e)
            {
                SB.AppendLine("error occurs when plotting. " + e.Message + ". Stack:" + e.StackTrace);
                if (PlotLogCFG != null) YWPLog.WriteDefaultLog("error occurs when plotting. " + e.Message + ". Stack:" + e.StackTrace, PlotLogCFG);
            }
            return SB.ToString();
        }

        /*
        public static string CreateMultiPlotImg(
            string imgFileName,
            string TitleName,
            List<Tuple<string, List<Tuple<IEnumerable<TimeStamped>, GetValue<TimeStamped>, string>>>> TimeSeriesGroup,
            int ImgWidth,
            int OnePlotHeight,
            DateTime VerticalLineTime,
            DateTime StartTime,
            List<Tuple<string, List<Tuple<IEnumerable<Tuple<double, double>>, string>>>> XYDataGroup,
            bool IgnalZero = false,
            int TimeLengthInSec = 0,
            string BGImg = ""
            )
        {
            StringBuilder SB = new StringBuilder();
            DateTime EndTime = DateTime.MaxValue;
            if (TimeLengthInSec > 0)
                EndTime = StartTime.AddSeconds(TimeLengthInSec);
            try
            {
                int YBinCount = OnePlotHeight / 35;
                int XBinCount = ImgWidth / 60;
                Chart OneChart = new Chart();
                OneChart.Width = ImgWidth;
                double TitleHeight = 0.08;
                double ChartCount = TitleHeight;
                if (TimeSeriesGroup != null)
                    ChartCount = TimeSeriesGroup.Count + TitleHeight;
                if (XYDataGroup != null) ChartCount += XYDataGroup.Count;
                if (ChartCount < 1)
                    return "No Data";
                OneChart.Height = (int)(ChartCount * OnePlotHeight);
                if (ChartCount >= 6)
                    OneChart.Height = (int)((ChartCount + 1) / 2 * OnePlotHeight);
                OneChart.Titles.Clear();
                OneChart.Titles.Add(TitleName).Text = TitleName;
                OneChart.Titles[0].Font = new Font("Arial", 10);
                OneChart.Titles[0].Position.X = 50;
                OneChart.Titles[0].Position.Y = 0;
                OneChart.Titles[0].Alignment = ContentAlignment.TopCenter;
                int i;
                int TimeSeriesCount = 0;
                if (TimeSeriesGroup != null)
                    TimeSeriesCount = TimeSeriesGroup.Count;
                for (i = 0; i < TimeSeriesCount; ++i)
                {
                    try
                    {
                        var TimeSeriesData = TimeSeriesGroup[i].Item2;
                        if (TimeSeriesData == null || TimeSeriesData.Count == 0) continue;
                        //YWPDistribution MyDis = new YWPDistribution(59.5, 60.5, 200, "");
                        string ChartAreaName = i.ToString();
                        ChartArea ThisChartArea = new ChartArea(ChartAreaName);
                        Legend ThisLegend = new Legend(ChartAreaName);
                        OneChart.ChartAreas.Add(ThisChartArea);
                        OneChart.Legends.Add(ThisLegend);
                        ThisLegend.DockedToChartArea = ChartAreaName;
                        ThisLegend.IsDockedInsideChartArea = false;
                        ThisLegend.Docking = Docking.Right;
                        ThisLegend.BackColor = Color.FromArgb(250, 250, 250);// Color.LightGray;  
                        ThisLegend.LegendStyle = LegendStyle.Column;
                        //SB.AppendLine("Plot " + i.ToString() + "th picture.");
                        //ForcedOscillationGlobalInfo.G.Log("Plot " + i.ToString() + "th picture.");                        
                        string YTitle = TimeSeriesGroup[i].Item1;
                        double Max = double.MinValue;
                        double Min = double.MaxValue;
                        SB.AppendLine("Series count: " + TimeSeriesData.Count.ToString());
                        //ForcedOscillationGlobalInfo.G.Log("Series count: " + TimeSeriesData.Count.ToString());
                        if (ChartCount < 6)
                        {
                            ThisChartArea.Position.X = 0;
                            ThisChartArea.Position.Y = (float)(100.0 / ChartCount * (i + TitleHeight));
                            ThisChartArea.Position.Width = 88;
                            ThisChartArea.Position.Height = (float)(100.0 / ChartCount);
                        }
                        else
                        {
                            ThisChartArea.Position.X = i % 2 * 50;
                            ThisChartArea.Position.Y = (float)(100.0 / ((ChartCount + 1) / 2) * ((i + 1) / 2 + TitleHeight));
                            ThisChartArea.Position.Width = 45;
                            ThisChartArea.Position.Height = (float)(100.0 / ((ChartCount + 1) / 2));

                        }
                        if (BGImg != "")
                        {
                            ThisChartArea.BackImage = BGImg;
                            ThisChartArea.BackImageWrapMode = ChartImageWrapMode.Unscaled;
                            ThisChartArea.BackImageAlignment = ChartImageAlignmentStyle.BottomRight;
                        }
                        foreach (var v in TimeSeriesData)
                        {
                            string seriesName = v.Item3;
                            Series s = null;
                            try
                            {
                                s = OneChart.Series.Add(seriesName);
                            }
                            catch (Exception eee)
                            { continue; }
                            s.Name = seriesName;
                            s.Label = seriesName;
                            s.ChartArea = ChartAreaName;
                            s.Legend = ChartAreaName;
                            s.BorderWidth = 1;
                            s.XValueType = ChartValueType.DateTime;
                            s.ChartType = SeriesChartType.FastLine;
                            //SB.AppendLine(seriesName);
                            foreach (var T in v.Item1)
                            {
                                if (T == null || T.TimeStamp.CompareTo(StartTime) < 0 || T.TimeStamp.CompareTo(EndTime) > 0) continue;
                                if (IgnalZero && v.Item2(T) == 0) continue;
                                int index = s.Points.AddXY(T.TimeStamp, v.Item2(T));
                                //SB.Append("[" + T.TimeStamp.ToString("HH:mm:ss.fff") +","+ v.Item2(T).ToString("F3") + "]\t");
                                if (v.Item2(T) > Max) Max = v.Item2(T);
                                if (v.Item2(T) < Min) Min = v.Item2(T);


                            }
                        }
                        //-----Configure the frequency chart------                
                        //ThisChartArea.Position.X = 0;
                        //ThisChartArea.Position.Y = (float)(100.0 / ChartCount * (i + TitleHeight));
                        //ThisChartArea.Position.Width = 85;
                        //ThisChartArea.Position.Height = (float)(100.0 / ChartCount);
                        ThisChartArea.AxisX.LabelStyle.Format = "yyyy-MM-dd HH:mm:ss";
                        ThisChartArea.AxisY.LabelStyle.Format = "F4";
                        double NewMax, NewMin, Interval;
                        if (Max == Min) { Max += 1; Min -= 1; }
                        //GetRangeAndInterval(Max, Min, YBinCount, out NewMax, out NewMin, out Interval);
                        GetRangeAndInterval(TimeSeriesData, 0.0005, Max, Min, YBinCount, out NewMax, out NewMin, out Interval);
                        ThisChartArea.AxisY.Minimum = NewMin;
                        ThisChartArea.AxisY.Maximum = NewMax;
                        ThisChartArea.AxisY.Interval = Interval;
                        ThisChartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
                        ThisChartArea.AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                        ThisChartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
                        ThisChartArea.AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                        //ThisChartArea.AxisX.MinorGrid.Interval = 1;
                        ThisChartArea.AxisX.MinorGrid.Enabled = true;
                        ThisChartArea.AxisX.MinorGrid.LineColor = Color.FromArgb(235, 235, 235);
                        ThisChartArea.AxisX.MinorGrid.LineDashStyle = ChartDashStyle.Dash;
                        ThisChartArea.AxisX.MinorGrid.LineWidth = 1;
                        ThisChartArea.AxisX.MinorGrid.IntervalType = DateTimeIntervalType.Seconds;
                        ThisChartArea.AxisY.LineWidth = 1;
                        ThisChartArea.AxisX.LineWidth = 1;
                        ThisChartArea.AxisY.Title = YTitle;
                        ThisChartArea.AxisY.TitleFont = new Font("Arial", 12);
                        ThisChartArea.AxisY.TitleForeColor = Color.Blue;
                        ThisChartArea.AxisX.ScaleView.Position = 0.5;
                        //if (i == TimeSeriesGroup.Count - 1)
                        //{
                        //    ThisChartArea.AxisX.Title = "UTC Time";
                        //    ThisChartArea.AxisX.TitleFont = new Font("Arial", 12);
                        //    ThisChartArea.AxisX.TitleForeColor = Color.Blue;
                        //}
                        ThisChartArea.AxisX.IntervalType = DateTimeIntervalType.Seconds;
                        StripLine OneStrip = new StripLine();
                        OneStrip.IntervalType = DateTimeIntervalType.Days;
                        OneStrip.Interval = 1;
                        OneStrip.IntervalOffsetType = DateTimeIntervalType.Seconds;
                        OneStrip.IntervalOffset = VerticalLineTime.TimeOfDay.TotalSeconds;
                        OneStrip.StripWidthType = DateTimeIntervalType.Seconds;
                        OneStrip.StripWidth = 1;
                        OneStrip.BackColor = Color.LightGray;
                        ThisChartArea.AxisX.StripLines.Add(OneStrip);
                        //if (System.IO.File.Exists("FOBackground.png"))
                        //{
                        //    ThisChartArea.BackImage = "FOBackground.png";
                        //    ThisChartArea.BackImageWrapMode = ChartImageWrapMode.Unscaled;
                        //    ThisChartArea.BackImageAlignment = ChartImageAlignmentStyle.BottomRight;
                        //}

                    }
                    catch (Exception ee)
                    {
                        SB.AppendLine("error occurs when plotting. " + ee.Message + ". Stack:" + ee.StackTrace);
                        //ForcedOscillationGlobalInfo.G.Log("error occurs when plotting. " + ee.Message + ". Stack:" + ee.StackTrace);
                    }
                }
                //----------------------------------------------------------------------
                if (XYDataGroup != null)
                {
                    for (int j = i; j < XYDataGroup.Count + i; ++j)
                    {
                        try
                        {
                            //break;
                            var TimeSeriesData = XYDataGroup[j - i].Item2;
                            if (TimeSeriesData == null || TimeSeriesData.Count == 0) continue;
                            string ChartAreaName = j.ToString();
                            ChartArea ThisChartArea = new ChartArea(ChartAreaName);
                            Legend ThisLegend = new Legend(ChartAreaName);
                            OneChart.ChartAreas.Add(ThisChartArea);
                            OneChart.Legends.Add(ThisLegend);
                            ThisLegend.DockedToChartArea = ChartAreaName;
                            ThisLegend.IsDockedInsideChartArea = false;
                            ThisLegend.Docking = Docking.Right;
                            ThisLegend.BackColor = Color.FromArgb(250, 250, 250);// Color.LightGray;  
                            ThisLegend.LegendStyle = LegendStyle.Column;
                            //FNetForcedOscillation.ForcedOscillationGlobalInfo.G.Log("Plot " + j.ToString() + "th picture.");
                            string YTitle = XYDataGroup[j - i].Item1;
                            double Max = double.MinValue;
                            double Min = double.MaxValue;
                            double XMax = double.MinValue;
                            double XMin = double.MaxValue;
                            //FNetForcedOscillation.ForcedOscillationGlobalInfo.G.Log("Series count: " + TimeSeriesData.Count.ToString());
                            foreach (var v in TimeSeriesData)
                            {
                                string seriesName = v.Item2;
                                var s = OneChart.Series.Add(seriesName);
                                s.Name = seriesName;
                                s.Label = seriesName;
                                s.ChartArea = ChartAreaName;
                                s.Legend = ChartAreaName;
                                s.BorderWidth = 1;
                                s.XValueType = ChartValueType.Auto;
                                s.ChartType = SeriesChartType.FastLine;
                                foreach (var T in v.Item1)
                                {
                                    s.Points.AddXY(T.Item1, T.Item2);
                                    if (T.Item2 > Max) Max = T.Item2;
                                    if (T.Item2 < Min) Min = T.Item2;
                                    if (T.Item1 > XMax) XMax = T.Item1;
                                    if (T.Item1 < XMin) XMin = T.Item1;
                                }
                            }
                            //-----Configure the frequency chart------                
                            ThisChartArea.Position.X = 0;
                            ThisChartArea.Position.Y = (float)(100.0 / ChartCount * (j + TitleHeight));
                            ThisChartArea.Position.Width = 85;
                            ThisChartArea.Position.Height = (float)(100.0 / ChartCount);
                            //ThisChartArea.AxisX.LabelStyle.Format = "HH:mm:ss";
                            ThisChartArea.AxisY.LabelStyle.Format = "F4";
                            double NewMax, NewMin, Interval;
                            GetRangeAndInterval(Max, Min, YBinCount, out NewMax, out NewMin, out Interval);
                            ThisChartArea.AxisY.Minimum = NewMin;
                            ThisChartArea.AxisY.Maximum = NewMax;
                            ThisChartArea.AxisY.Interval = Interval;
                            GetRangeAndInterval(XMax, XMin, XBinCount, out NewMax, out NewMin, out Interval);
                            ThisChartArea.AxisX.Minimum = NewMin;
                            ThisChartArea.AxisX.Maximum = NewMax;
                            ThisChartArea.AxisX.Interval = Interval;
                            ThisChartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
                            ThisChartArea.AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                            ThisChartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
                            ThisChartArea.AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                            ThisChartArea.AxisX.MinorGrid.Enabled = true;
                            ThisChartArea.AxisX.MinorGrid.LineColor = Color.FromArgb(235, 235, 235);
                            ThisChartArea.AxisX.MinorGrid.LineDashStyle = ChartDashStyle.Dash;
                            ThisChartArea.AxisX.MinorGrid.LineWidth = 1;
                            ThisChartArea.AxisY.LineWidth = 1;
                            ThisChartArea.AxisX.LineWidth = 1;
                            ThisChartArea.AxisY.Title = YTitle;
                            ThisChartArea.AxisY.TitleFont = new Font("Arial", 12);
                            ThisChartArea.AxisY.TitleForeColor = Color.Blue;
                            if (BGImg != "")
                            {
                                ThisChartArea.BackImage = BGImg;
                                ThisChartArea.BackImageWrapMode = ChartImageWrapMode.Unscaled;
                                ThisChartArea.BackImageAlignment = ChartImageAlignmentStyle.BottomRight;
                            }
                            //if (System.IO.File.Exists("FOBackground.png"))
                            //{
                            //    ThisChartArea.BackImage = "FOBackground.png";
                            //    ThisChartArea.BackImageWrapMode = ChartImageWrapMode.Unscaled;
                            //    ThisChartArea.BackImageAlignment = ChartImageAlignmentStyle.BottomRight;
                            //}
                        }
                        catch (Exception ee)
                        {
                            SB.AppendLine("error occurs when plotting XY. " + ee.Message + ". Stack:" + ee.StackTrace);
                        }
                    }
                }
                //------Save Image -----
                string folder = imgFileName.Substring(0, imgFileName.LastIndexOf('\\') + 1);
                if (!System.IO.Directory.Exists(folder))
                    System.IO.Directory.CreateDirectory(folder);
                //if (System.IO.File.Exists(imgFileName))
                //    imgFileName += DateTime.Now.Millisecond.ToString() + ".png";
                OneChart.SaveImage(imgFileName, ChartImageFormat.Png);
                OneChart.ChartAreas.Clear();

            }
            catch (Exception e)
            {
                SB.AppendLine("error occurs when plotting. " + e.Message + ". Stack:" + e.StackTrace);
            }
            return SB.ToString();
        }


        */
        /// <summary>
        /// Given maximum & minimum value and desired bin count, this function will return the best minmum & maximum value and interval for plotting.
        /// </summary>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <param name="binCount"></param>
        /// <param name="Max"></param>
        /// <param name="Min"></param>
        /// <param name="Interval"></param>
        public static void GetRangeAndInterval(double max, double min, int binCount, out double Max, out double Min, out double Interval)
        {
            Max = Math.Max(max, min);
            Min = Math.Min(max, min);
            max = Max;
            min = Min;
            if (binCount > 0) Interval = (max - min) / binCount;
            else Interval = (max - min);
            if (max == min || Interval <= 0.000001)
            {
                Interval = max - min;
                return;
            }
            double BinWidth = (max - min) / binCount;
            double Order = 1;
            while (BinWidth < 1) { BinWidth *= 10; Order /= 10; }
            while (BinWidth >= 10) { BinWidth /= 10; Order *= 10; }
            if (BinWidth <= 1.414) Interval = 1;
            else if (BinWidth <= 3.162) Interval = 2;
            else if (BinWidth <= 7.071) Interval = 5;
            else Interval = 10;
            Interval *= Order;
            Max = (int)(max / Interval) * Interval;
            while (Max < max) Max += Interval;
            Min = (int)(min / Interval) * Interval;
            while (Min > min) Min -= Interval;
        }
        /// <summary>
        /// Given maximum & minimum value and desired bin count, this function will return the best minmum & maximum value and interval for plotting.
        /// </summary>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <param name="binCount"></param>
        /// <param name="Max"></param>
        /// <param name="Min"></param>
        /// <param name="Interval"></param>
        public static void GetRangeAndInterval(double max, double min, int binCount, out double Max, out double Min, out double Interval, out string FormatString)
        {
            Max = Math.Max(max, min);
            Min = Math.Min(max, min);
            max = Max;
            min = Min;
            FormatString = "F5";
            if (binCount > 0) Interval = (max - min) / binCount;
            else Interval = (max - min);
            if (max == min) return;
            double BinWidth = (max - min) / binCount;
            double Order = 1;
            int OrderIndex = 0;
            while (BinWidth < 1) { BinWidth *= 10; Order /= 10; OrderIndex -= 1; }
            while (BinWidth >= 10) { BinWidth /= 10; Order *= 10; OrderIndex += 1; }
            if (BinWidth <= 1.414) Interval = 1;
            else if (BinWidth <= 3.162) Interval = 2;
            else if (BinWidth <= 7.071) Interval = 5;
            else Interval = 10;
            Interval *= Order;
            Max = (int)(max / Interval) * Interval;
            while (Max < max) Max += Interval;
            Min = (int)(min / Interval) * Interval;
            while (Min > min) Min -= Interval;
            if (OrderIndex >= 0) FormatString = "F0";
            else
                FormatString = "F" + (OrderIndex * (-1)).ToString();
        }
        /// <summary>
        /// Given a group of timeseries, maximum & minimum value of the timeseries and desired bin count, this function will return the best minmum & maximum value and interval for plotting the timeseries.
        /// Timeseries may have abnormal values which have significant deviation from most of the values, which mess up the scale of Y-axis and make the plot unreadable.
        /// This function will ignor the top and bottom 'ignorRatio' values and then calculate the best minmum, maximum value and interval for plotting.
        /// </summary>
        /// <param name="TimeSeries">Time series which contain all the value needed to plot in the same chart</param>
        /// <param name="ignorRatio">ratio of values to be ignored, [0,1).</param>
        /// <param name="max">max value of all values</param>
        /// <param name="min">min value of all values</param>
        /// <param name="binCount">desired bin count</param>
        /// <param name="Max">Out, Best maximum value for plotting</param>
        /// <param name="Min">Out, Best minimum value for plotting</param>
        /// <param name="Interval">Out, Best interval for plotting</param>
        public static void GetRangeAndInterval(List<Tuple<IEnumerable<TimeStamped>, GetValue<TimeStamped>, string>> TimeSeries,double ignorRatio,  double max, double min, int binCount, out double Max, out double Min, out double Interval)
        {
            Max = max;
            Min = min;
            Interval = 1;
            if (TimeSeries == null || TimeSeries.Count == 0) return;
            double BestMax = max;
            double BestMin = min;
            if (TimeSeries.Count > 100)
            {
                try
                {
                    YWPDistribution dis = new YWPDistribution(min, max, 2000, "dis");
                    foreach (var v in TimeSeries)
                    {
                        var Values = v.Item1;
                        var GetValueHandler = v.Item2;
                        foreach (var vv in Values)
                            dis.AddOneValue(GetValueHandler(vv));
                    }
                    int N = Math.Max((int)(dis.TotalCount * ignorRatio), 5);
                    BestMax = dis.GetNthUpper(N);
                    BestMin = dis.GetNthLower(N);
                }
                catch(Exception ee)
                {
                throw ee;
                }
            }
            GetRangeAndInterval(Math.Min(BestMax+(BestMax-BestMin)*0.2,max), Math.Max(BestMin+(BestMin-BestMax)*0.2,min), binCount, out Max, out Min, out Interval);            
        }
        public static void GetRangeAndInterval(YWPPlotCFG TimeSeries, double ignoreRatio, double max, double min, int binCount, out double Max, out double Min, out double Interval)
        {
            Max = max;
            Min = min;
            Interval = 1;
            if (TimeSeries == null || TimeSeries.MultipleDataSeries.Count == 0) return;
            double BestMax =0;
            double BestMin = 0;
            if (TimeSeries.ValueRangeType == YWPPlotCFG.DetermineValueRange.SpecifyUpperLowerValue)
            {
                BestMax = TimeSeries.UpperLimit;
                BestMin = TimeSeries.LowerLimit;
                GetRangeAndInterval(BestMax, BestMin, binCount, out Max, out Min, out Interval);
            }
            else
            {
                YWPDistribution dis = new YWPDistribution(min, max, 2000,  "dis");
                foreach (var v in TimeSeries.MultipleDataSeries)
                {
                    var Values = v.DataSeries;
                    var GetValueHandler = v.MethodToGetValue;
                    foreach (var vv in Values)
                        dis.AddOneValue(GetValueHandler(vv));
                }
                int N = Math.Max((int)(dis.TotalCount * ignoreRatio), 5);
                BestMax = dis.GetNthUpper(N);
                BestMin = dis.GetNthLower(N);
                if (TimeSeries.ValueRangeType == YWPPlotCFG.DetermineValueRange.None)
                {
                    GetRangeAndInterval(Math.Min(BestMax + (BestMax - BestMin) * 0.1, max), Math.Max(BestMin + (BestMin - BestMax) * 0.1, min), binCount, out Max, out Min, out Interval);
                }
                else if (TimeSeries.ValueRangeType == YWPPlotCFG.DetermineValueRange.SpecifyUpperLowerDifference)
                {
                    BestMax = dis.GetNPercentUpper(0.5) + TimeSeries.UpperLowerDifference / 2.0;
                    BestMin = BestMax - TimeSeries.UpperLowerDifference;
                    GetRangeAndInterval(BestMax, BestMin, binCount, out Max, out Min, out Interval);
                }
            }
        }

    }

    /// <summary>
    /// A series of data for plotting. e.g. a series of  frequency measurements of one FDR.
    /// </summary>
    public class YWPTimeSeriesForPlot
    {
        /// <summary>
        /// the series of time and value
        /// </summary>
        public IEnumerable<TimeStamped> DataSeries = null;
        /// <summary>
        /// Method to get the value from 'TimeStamped' objects.
        /// </summary>
        public GetValue<TimeStamped> MethodToGetValue = null;
        /// <summary>
        /// Name of the data series. It should be unique when multiple series are plotted in the same chart.
        /// </summary>
        public string DataSeriesName;

        public YWPTimeSeriesForPlot(string seriesName, IEnumerable<TimeStamped> data, GetValue<TimeStamped> getDataHandler)
        {
            DataSeriesName = seriesName;
            DataSeries = data;
            MethodToGetValue = getDataHandler;
        }
    }
    /// <summary>
    /// Data and configuration of a single plot. e.g. the frequency data of multiple FDRs.
    /// </summary>
    public class YWPPlotCFG
    {
        /// <summary>
        /// The plot name, which will be displayed along the Y-axis. (X-axis is time.)
        /// </summary>
        public string PlotName;
        /// <summary>
        /// whether or not specify the value upper/lower limitation of the plot.
        /// </summary>
        public DetermineValueRange ValueRangeType = DetermineValueRange.None;
        public double UpperLimit = 0;
        public double LowerLimit = 0;
        public double UpperLowerDifference = 0;
        /// <summary>
        /// The data of series which will be plotted in the same chart.
        /// </summary>
        public List<YWPTimeSeriesForPlot> MultipleDataSeries = new List<YWPTimeSeriesForPlot>();
        public YWPPlotCFG()
        { }
        public YWPPlotCFG(string plotName)
        {
            PlotName = plotName;
        }

        public enum DetermineValueRange
        {
            /// <summary>
            /// Dont' specify value range. Determine the range somewhere else.
            /// </summary>
            None,
            /// <summary>
            /// Specify the upper and lower value
            /// </summary>
            SpecifyUpperLowerValue,
            /// <summary>
            /// Specify the value difference between upper and lower value.
            /// </summary>
            SpecifyUpperLowerDifference
        }
    }

}
