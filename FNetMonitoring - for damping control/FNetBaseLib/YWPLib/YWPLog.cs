﻿/*******************************************************************************************
 *  
 * Class to log message to different files.
 * 
 * Developed by Wenpeng (Wayne) Yu in 2017/5  
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;

namespace FNetBaseLib.YWPLib
{    
    public class YWPLog
    {

        private readonly string RootFolder = "";
        private static Dictionary<string, MyStreamWriter> AllWriters = new Dictionary<string, MyStreamWriter>();
        private static YWPLog DefaultLoger = new YWPLog("Log\\", new YWPLogTypeCFG("LogDefault", "", true, true, YWPLogTypeCFG.ExpireType.Monthly));
        private readonly YWPLogTypeCFG DefaultLogCfg;
        private string GetKey(YWPLogTypeCFG logCFG)
        {
            if (logCFG == null) return "";
            return RootFolder.ToUpper() + logCFG.GetKey();
        }
        /// <summary>
        /// Specify the root folder of all log files. 
        /// If 'defaultLogCfg' is null, default log file will be %RootFolder%\Log\Log[yyyy-MM].txt
        /// </summary>
        public YWPLog(string rootFolder, YWPLogTypeCFG defaultLogCfg = null)
        {
            RootFolder = rootFolder;
            if (RootFolder != null && RootFolder.Length > 0 && !Directory.Exists(RootFolder))
                Directory.CreateDirectory(RootFolder);
            if (defaultLogCfg != null)
                DefaultLogCfg = defaultLogCfg;
            else
                DefaultLogCfg = new YWPLogTypeCFG("Log", "Log\\", true, true, YWPLogTypeCFG.ExpireType.Monthly);
        }
        public void WriteLog(string LogMsg, YWPLogTypeCFG logCFG = null)
        {
            if (logCFG == null) logCFG = DefaultLogCfg;
            string key = GetKey(logCFG);
            lock (AllWriters)
                if (!AllWriters.ContainsKey(key))
                    AllWriters.Add(key, new MyStreamWriter(RootFolder, logCFG));
            lock (AllWriters[key])
                AllWriters[key].Log(LogMsg);
        }
        public static void WriteDefaultLog(string LogMsg)
        {
            DefaultLoger.WriteLog(LogMsg);
        }
        public static void WriteDefaultLog(string LogMsg, YWPLogTypeCFG logCFG)
        {
            DefaultLoger.WriteLog(LogMsg, logCFG);
        }
        public void CloseWritter(YWPLogTypeCFG logCFG)
        {
            string key = GetKey(logCFG);
            if (!AllWriters.ContainsKey(key)) return;
            lock (AllWriters[key]) AllWriters[key].CloseWriter();
        }
        public void CloseAllWritter(YWPLogTypeCFG logCFG)
        {
            lock (AllWriters)
                foreach (var v in AllWriters)
                    v.Value.CloseWriter();
        }
        private class MyStreamWriter
        {
            private StreamWriter SW = null;
            private DateTime ExpiredTime;
            private string RootFolder;
            private YWPLogTypeCFG ThisLogType;
            public MyStreamWriter(string rootFolder, YWPLogTypeCFG logType)
            {
                ThisLogType = logType;
                RootFolder = rootFolder;
            }
            public void Log(string msg)
            {
                if (SW == null) SW = GetNewSW();
                else if (ThisLogType.LogExpireType != YWPLogTypeCFG.ExpireType.NeverExpire && DateTime.Now.CompareTo(ExpiredTime) >= 0)
                {
                    SW.Close();
                    SW = GetNewSW();
                }
                if (!ThisLogType.LogLocalTime) SW.WriteLine(msg);
                else SW.WriteLine(DateTime.Now.ToString("[MM/dd/yyyy HH:mm:ss.fff]\t") + msg);
                SW.Flush();
            }
            private StreamWriter GetNewSW()
            {
                StreamWriter sw = null;
                string FileNameAndPath = GetFileNameAndSetExpireTime();
                try
                { sw = new StreamWriter(FileNameAndPath + ".txt", ThisLogType.Append); }
                catch (Exception e)
                {
                    Random ra = new Random(DateTime.Now.Millisecond);
                    int tryCount = 0;
                    while (sw == null && tryCount++ < 10000)
                    {
                        try
                        { sw = new StreamWriter(FileNameAndPath + "_" + ra.Next(1000).ToString() + ".txt", ThisLogType.Append); }
                        catch (Exception ee) { sw = null; }
                    }
                    if (sw != null)
                    {
                        sw.WriteLine("**********************************************************");
                        sw.WriteLine("* File Access Confliction: " + FileNameAndPath + ".txt");
                        sw.WriteLine("**********************************************************");
                        sw.Flush();
                    }
                }
                return sw;
            }
            public void CloseWriter()
            {
                if(SW!=null)
                {
                    try { SW.Close();SW = null; }
                    catch (Exception e) { }
                }
            }
            /// <summary>
            /// No extension 
            /// </summary>
            private string GetFileNameAndSetExpireTime()
            {
                string Path = RootFolder + ThisLogType.SubFolder;
                if (Path!=null && Path.Length>0 && !Directory.Exists(Path)) Directory.CreateDirectory(Path);
                string FileName = Path + ThisLogType.TypeName;
                DateTime Now = DateTime.Now;
                switch (ThisLogType.LogExpireType)
                {
                    case YWPLogTypeCFG.ExpireType.NeverExpire:
                        ExpiredTime = DateTime.MaxValue;
                        break;
                    case YWPLogTypeCFG.ExpireType.Yearly:
                        ExpiredTime = new DateTime(Now.Year + 1, 1, 1);
                        FileName += string.Format(" [{0}]", Now.Year);
                        break;
                    case YWPLogTypeCFG.ExpireType.Monthly:
                        ExpiredTime = new DateTime(Now.AddMonths(1).Year, Now.AddMonths(1).Month, 1);
                        FileName += string.Format(" [{0}]", Now.ToString("yyyy-MM"));
                        break;
                    case YWPLogTypeCFG.ExpireType.Daily:
                        ExpiredTime = Now.AddDays(1).Date;
                        FileName += string.Format(" [{0}]", Now.ToString("yyyy-MM-dd"));
                        break;
                    case YWPLogTypeCFG.ExpireType.Hourly:
                        ExpiredTime = Now.AddHours(1).Date.AddHours(Now.AddHours(1).Hour);
                        FileName += string.Format(" [{0}]", Now.ToString("yyyy-MM-dd HH"));
                        break;
                    case YWPLogTypeCFG.ExpireType.Minutely:
                        ExpiredTime = Now.AddMinutes(1).Date.AddHours(Now.AddMinutes(1).Hour).AddMinutes(Now.AddMinutes(1).Minute);
                        FileName += string.Format(" [{0}]", Now.ToString("yyyy-MM-dd HH.mm"));
                        break;
                }
                return FileName;
            }
        }
    }
    
    public class YWPLogTypeCFG
    {
        public enum ExpireType { NeverExpire, Yearly, Monthly, Daily, Hourly, Minutely }

        public readonly string TypeName;
        public readonly string SubFolder;
        public readonly bool Append;
        public readonly ExpireType LogExpireType;
        public readonly bool LogLocalTime;
        public YWPLogTypeCFG(string typeName, string subFolder, bool append, bool logLocalTime, ExpireType expireType)
        {
            if (typeName != null && typeName.Length > 0)
                TypeName = typeName;
            else TypeName = "Log_NoTypeName";
            SubFolder = subFolder;
            Append = append;
            LogLocalTime = logLocalTime;
            LogExpireType = expireType;
        }
        public override string ToString() { return SubFolder.ToUpper() + TypeName.ToUpper(); }
        public string GetKey() { return SubFolder.ToUpper() + TypeName.ToUpper(); }
    }
}

