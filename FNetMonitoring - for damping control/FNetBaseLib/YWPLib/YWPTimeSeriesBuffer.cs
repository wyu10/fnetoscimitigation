﻿/*******************************************************************************************
 * This class is used to buffer time series with the ability of automatic old-data removing.
 * The buffer is implemented based on YWPQueue with the following requirements:
 * 
 * 1) Old data will be removed automatically according to its timestamp and 'BufferLenInSecond' with O(1).
 * 2) If 'GetValueHandler' is provided, the buffer could get the summary of all values in the buffer.
 * 
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;
using System.Collections.Generic;

namespace FNetBaseLib.YWPLib
{
    public class YWPTimeSeriesBuffer<T>:YWPQueue<T> where T:TimeStamped
    {
        private double _lengthInSecond=20;
        private double _maxTimeInterval;
        private double _totalValue = 0;
        private List<int> locker = new List<int>();
        private GetValue<T> _getValue = null;
        public string BufferName;
        public int BufferID;
        public double ValuePerSecond { get; set; }
        /// <summary>
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="BufferLenInSecond"></param>
        /// <param name="MaxTimeInterval">If the time span between the head and current pushing data is over this MaxTimeInterval, the data is considered discontinuous and the buffer will be cleared. 
        /// In second. MaxTimeInterval = 0: won't check the continuity. 
        /// </param>
        public YWPTimeSeriesBuffer(double ValuePerSec, double BufferLenInSecond, double MaxTimeInterval=0):base()
        {
            _lengthInSecond = BufferLenInSecond;
            _maxTimeInterval = MaxTimeInterval;
            ValuePerSecond = ValuePerSec;
        }
        public YWPTimeSeriesBuffer(double ValuePerSec, double BufferLenInSecond, GetValue<T> GetValueHandler, double MaxTimeInterval = 0) : base()
        {
            _lengthInSecond = BufferLenInSecond;
            _maxTimeInterval = MaxTimeInterval;
            ValuePerSecond = ValuePerSec;
            _getValue = GetValueHandler;
        }
        public double BuffLenInSecond { get { return _lengthInSecond; } }
        public new void Clear()
        {
            lock (locker)
            {
                base.Clear();
                _totalValue = 0;
            }
        }
        public new void Pop()
        {
            lock (locker)
            {
                if (_getValue != null && base.Tail != null) _totalValue -= _getValue(base.Tail);
                base.Pop();
            }
        }

        /// <summary>
        /// What if the time order is wrong? (Wayne)
        /// </summary>
        /// <param name="OneData"></param>
        /// <returns></returns>
        public new bool Push(T OneData) 
        {
            lock (locker)
            {
                if (OneData == null || (Count > 0 && OneData.TimeStamp.CompareTo(Head.TimeStamp) < 0))
                    return false;
                if (_maxTimeInterval > 0 && Count > 0 && OneData.TimeStamp.Subtract(Head.TimeStamp).TotalSeconds > _maxTimeInterval)
                    Clear();
                base.Push(OneData);
                if (_getValue != null) _totalValue += _getValue(OneData);
                if (Head == null)
                    throw new Exception("Buffer head is null!");
                RemoveExpired();
                return true;
            }
        }
        public double TotalValue { get { if (_getValue != null) return _totalValue; else return 0; } }
        public double AverageValue { get { if (_getValue != null && Count > 0) return _totalValue / Count; else return 0; } }
        private void RemoveExpired()
        {
            var h = base.Head;
            if (base.Count == 0 || base.Head == default(T)) return;
            if (_lengthInSecond > 86400 || _lengthInSecond < 0) return;
            DateTime expireTime = base.Head.TimeStamp.AddSeconds(-1 * _lengthInSecond);
            while (base.Count > 0 && base.Tail != null && base.Tail.TimeStamp.CompareTo(expireTime) < 0)Pop();
        }
        
        /// <summary>
        /// Return a integer to indicate if the T is inside buffer's time range. 0: on the boundary; >0 :inside <0: outside
        /// </summary>
        public int IsInside(DateTime Time)
        {
            if (Count <= 0) return -1;
            double thres = 0.5 / ValuePerSecond;
            double distance = Time.Subtract(Head.TimeStamp).TotalSeconds;
            if (distance <= 0 && distance > -1 * thres) return 0;
            distance = Time.Subtract(Tail.TimeStamp).TotalSeconds;
            if (distance >= 0 && distance < thres) return 0;
            return -1 * Time.CompareTo(Head.TimeStamp) * Time.CompareTo(Tail.TimeStamp);
        }
        private List<T> GetItemByTime(DateTime Time)
        {            
            if (Count == 0 ||IsInside(Time) < 0) return null;
            if (Count == 1) { if (Head.TimeStamp == Time) return new List<T>() { Head }; else return null; }
            List<T> result = new List<T>();
            int left = 0, right = Count - 1;
            int middle;
            while(right-left>1)
            {
                middle = (right + left) / 2;
                int c = Time.CompareTo(this[middle].TimeStamp);
                if (c == 0) return new List<T>() { this[middle] };
                if (c < 0) right = middle;
                else left = middle;
            }
            if (Time.CompareTo(this[left].TimeStamp) == 0) return new List<T>() { this[left] };
            if (Time.CompareTo(this[right].TimeStamp) == 0) return new List<T>() { this[right] };
            return new List<T>() { this[left], this[right] };
        }
        public T GetExactItemByTime(DateTime Time)
        {
            var v = GetItemByTime(Time);
            if (v == null || v.Count != 1) return null;
            return v[0];
        }
        public void RemoveOldData(DateTime OldTimeThreshold)
        {
            while (base.Count > 0 && base.Tail.TimeStamp.CompareTo(OldTimeThreshold) <= 0) Pop();
        }
        public FindValueResult GetValueByTime(DateTime Time,GetValue<T> GetValueHandler,out double Value)
        {
            Value = -1;
            var Ts = GetItemByTime(Time);
            if (Ts == null || Ts.Count == 0) return FindValueResult.TimeOutOfRange;
            if (Ts.Count == 1) { Value = GetValueHandler(Ts[0]); return FindValueResult.Original; }
            if(Ts.Count == 2) { Value = GetInterpolationByTime(Ts[0], Ts[1], Time, GetValueHandler);return FindValueResult.Interpolation; }
            return FindValueResult.Error;
        }
        public double GetInterpolationByTime(T A, T B, DateTime TimeInBetween, GetValue<T> GetValueHandler)
        {
            if (A == null || B == null || GetValueHandler == null) return -1;
            return (GetValueHandler(B) - GetValueHandler(A)) / B.TimeStamp.Subtract(A.TimeStamp).TotalSeconds * TimeInBetween.Subtract(A.TimeStamp).TotalSeconds + GetValueHandler(A);
        }
        
    }
    public enum FindValueResult:int
    {
        Error = 1,
        TimeOutOfRange = 2,
        Original = 4,
        Interpolation = 8
    }
}
