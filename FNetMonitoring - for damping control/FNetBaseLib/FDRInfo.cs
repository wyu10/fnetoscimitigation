﻿/*******************************************************************************************
 * Class of FDR information.
 * You may append new fields/properties to the class if necessary.
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib
{
    public class FDRInfo
    {
        public int FDRID { get; set; }
        public string FDRName { get; set; }
        /// <summary>
        /// In millisecond
        /// </summary>
        public int TimeOffset { get; set; }
        /// <summary>
        /// The offset which will be added to the timestamp of data extracted from openHistorian.
        /// Unit is millisecond.
        /// </summary>
        public int TimeOffsetForOpenHistorian { get; set; } = 0;
        public Grid GridName { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        /// <summary>
        /// Initialize FDRInfo
        /// </summary>
        /// <param name="fdrID">FDR ID, int</param>
        /// <param name="gridAbbreviation">Grid Abbreviation, such as E, W, T, Q</param>
        public FDRInfo(int fdrID, string gridAbbreviation)
        {
            FDRID = fdrID;
            GridName = ConvertToGrid(gridAbbreviation);
        }
        public static Grid ConvertToGrid(string gridAbbreviation)
        {
            switch (gridAbbreviation.ToUpper())
            {
                case "EI":
                case "E":
                    return Grid.E;
                case "WECC":
                case "W":
                case "WI":
                    return Grid.W;
                case "ERCOT":
                case "T":
                case "TI":
                    return Grid.T;
                case "QUEBEC":
                case "Q":
                case "QI":
                    return Grid.Q;
                default:
                    return Grid.NONE;
            }
        }
    }
    public enum Grid { NONE = 0, E = 1, W = 2, T = 3, Q = 4 }
}
