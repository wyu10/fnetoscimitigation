﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace FNetBaseLib
{
    public class FNetTriggerMeasurement : TimeStamped, IComparable<FNetTriggerMeasurement>
    {
        public readonly string Grid;
        private float _processedFrame = 0;
        private double _avgFDRcount = 0;
        private int _valueStatus = 0;
        public float ProcessedFrame
        {
            get { return _processedFrame; }
            set { _processedFrame = value; if (!float.IsNaN(value)) _valueStatus = _valueStatus | 1; }
        }
 
        public double avgFDRcount
        {
            get { return _avgFDRcount; }
            set { _avgFDRcount = value; if (!double.IsNaN(value)) _valueStatus = _valueStatus | 2; }
        }

        public FNetTriggerMeasurement(String grid, DateTime timestamp)
        {
            Grid = grid;
            TimeStamp = timestamp;
        }

        public int CompareTo(FNetTriggerMeasurement other)
        {
            return TimeStamp.CompareTo(other.TimeStamp) != 0 ? TimeStamp.CompareTo(other.TimeStamp) : Grid.CompareTo(other.Grid);
        }
    }
}
