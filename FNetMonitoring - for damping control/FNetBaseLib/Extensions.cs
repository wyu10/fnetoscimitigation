﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib
{
    public static class Extensions
    {
        public static double FractionalSecond(this DateTime datetime)
        {
            return datetime.Ticks % 10000000 / 10000000.0;
        }
        public static DateTime RemoveFractionalSec(this DateTime datetime)
        {
            return new DateTime(((long)(datetime.Ticks / 10000000)) * 10000000, datetime.Kind);
        }
        public static DateTime RemoveSecond(this DateTime datetime)
        {
            return new DateTime(((long)(datetime.Ticks / 10000000/60)) * 10000000*60, datetime.Kind);
        }
        public static double SecondsOfDay(this DateTime datetime)
        {
            return datetime.Subtract(datetime.Date).TotalSeconds;
        }
    }
}
