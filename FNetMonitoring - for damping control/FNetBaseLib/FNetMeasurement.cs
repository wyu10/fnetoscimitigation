﻿/*******************************************************************************************
 * Time stamp and measurement values.
 * Data structure for all triggers.
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib
{
    public delegate bool IsAPriorToB<T>(T A, T B);
    public delegate double GetValue<T>(T A);
    public class FNetMeasurement : TimeStamped, IComparable<FNetMeasurement>
    {
        /// <summary>
        /// FDR ID
        /// </summary>
        public readonly int FDRID;
        public string FDRName;        
        private float _frequency = 0;
        private float _angle = 0;
        private float _relativeAngle = 0;
        private float _voltage = 0;
        /// <summary>
        /// first bit is for Frequency, second bit for Angle, Third bit for Voltage
        /// 000: no value
        /// 001: has frequency
        /// 010: has Angle
        /// 100: has Voltage
        /// </summary>
        private int _valueStatus = 0;        
        public float Frequency
        {
            get { return _frequency; }
            set { _frequency = value; if(!float.IsNaN(value)) _valueStatus = _valueStatus | 1; }
        }
        /// <summary>
        /// frequency deviation from the average frequency of a time window.
        /// </summary>
        public float RelativeFreq = 0;
        /// <summary>
        /// Phase angle of voltage. Could be in degree or radian. Radian is recommended. 
        /// </summary>
        public float Angle
        {
            get { return _angle; }
            set { _angle = value; if(!float.IsNaN(value))_valueStatus = _valueStatus | 2; }
        }
        public float RelativeAngle
        { get { return _relativeAngle; } set { _relativeAngle = value; } }

        public float RelativeAng_Nom = 0;  //nominalized relative angle (substract first relative angle)
        /// <summary>
        /// Magnitude of voltage
        /// </summary>
        public float Voltage
        {
            get { return _voltage; }
            set {
                _voltage = value;
                if (!float.IsNaN(value))
                    _valueStatus = _valueStatus | 4;
            }
        }
        public int FPS = 10;
        private float _currentAng = 0;
        private float _currentMag = 0;
        public float CurrentAng
        {
            get { return _currentAng; }
            set { _currentAng = value; if (!float.IsNaN(value)) _valueStatus = _valueStatus | 8; }
        }
        public float CurrentMag
        {
            get { return _currentMag; }
            set { _currentMag = value; if (!float.IsNaN(value)) _valueStatus = _valueStatus | 16; }
        }
        public FNetMeasurement(int unitID, DateTime timestamp)
        {
            FDRID = unitID;
            TimeStamp = timestamp;
        }
        public int CompareTo(FNetMeasurement other)
        {
            return TimeStamp.CompareTo(other.TimeStamp) != 0 ? TimeStamp.CompareTo(other.TimeStamp) : FDRID.CompareTo(other.FDRID);
        }
        public bool HasFrequency { get { return (_valueStatus & 1) > 0; } }
        public bool HasAngle { get { return (_valueStatus & 2) > 0; } }
        public bool HasVoltage { get { return (_valueStatus & 4) > 0; } }
        public bool HasFreqAndAng { get { return HasFrequency && HasAngle; } }

        public bool HasCurrentAng { get { return (_valueStatus & 8) > 0; } }
        public bool HasCurrentMag { get { return (_valueStatus & 16) > 0; } }
        public FNetMeasurement GetCopy()
        {
            FNetMeasurement copy = new FNetMeasurement(FDRID, TimeStamp);
            copy.FDRName = FDRName;
            if (HasFrequency) copy.Frequency = Frequency;
            if (HasAngle) copy.Angle = Angle;
            if (HasVoltage) copy.Voltage = Voltage;
            copy.RelativeAngle = RelativeAngle;
            return copy;
        }
        public int Date { get { return Convert.ToInt32(TimeStamp.ToString("MMddyy")); } }
        public int Time { get { return Convert.ToInt32(TimeStamp.ToString("HHmmss")); } }
        public int ConvNum { get { return TimeStamp.Millisecond / 100 + 1; } }
        /// <summary>
        /// timestamp without fractional second
        /// </summary>
        public DateTime TimestampToSecond { get { return TimeStamp.RemoveFractionalSec(); } }
        public double TimestampFractionalSec { get { return TimeStamp.FractionalSecond(); } }

        public static GetValue<FNetMeasurement> GetFreq = (A) => A.Frequency;
        public static GetValue<TimeStamped> GetFreqGen = (A) => ((FNetMeasurement)A).Frequency;
        public static GetValue<FNetMeasurement> GetAng = (A) => A.Angle;
        public static GetValue<TimeStamped> GetAngGen = (A) => ((FNetMeasurement)A).Angle;
        public static GetValue<FNetMeasurement> GetRelAng = (A) => A.RelativeAngle;
        public static GetValue<TimeStamped> GetRelAngGen = (A) => ((FNetMeasurement)A).RelativeAngle;
        //public static GetValue<FNetMeasurement> GetVol = (A) => A.Voltage;
        //public static GetValue<TimeStamped> GetVolGen = (A) => ((FNetMeasurement)A).Voltage;
        public object UserObj = null;
    }
}
