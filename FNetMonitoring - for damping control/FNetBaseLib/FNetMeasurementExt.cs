﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib
{
    public class FNetMeasurementExt: FNetMeasurement
    {
        /// <summary>
        /// POW of Voltage.
        /// </summary>
        public List<int> POWVList = null;
        /// <summary>
        /// POW of Current
        /// </summary>
        public List<int> POWIList = null;
        public FNetMeasurementExt(int unitID, DateTime timestamp):base(unitID, timestamp)
        {
        }
    }
}
