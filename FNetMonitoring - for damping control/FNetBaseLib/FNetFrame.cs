﻿/*******************************************************************************************
 * Time stamp and measurement values at that time.
 * Data structure for all triggers.
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib
{
    public class FNetFrame
    {
        public List<FNetMeasurement> Measurements = new List<FNetMeasurement>();
        public DateTime TimeStamp; 
        public FNetMeasurement SysMedian = null;
        public FNetFrame(DateTime timestamp)
        {
            TimeStamp = timestamp;
        }
        public FNetFrame GetCopy()
        {
            FNetFrame copy = new FNetFrame(TimeStamp);
            foreach (var v in Measurements)
                copy.Measurements.Add(v);
            return copy;
        }
    }
    
}
