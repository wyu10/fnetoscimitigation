﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FNetBaseLib
{
    public class FNetCommon
    {
        public static string GetPath(string RootFolder, DateTime eventTime)
        {
            string Path = string.Format("{0}{1}\\{2}\\{3}\\", RootFolder, eventTime.Year, eventTime.Month, eventTime.Day);
            if (!Directory.Exists(Path)) Directory.CreateDirectory(Path);
            return Path;
        }
    }
}
