﻿/*******************************************************************************************
 * Base class of all FNetTrigger.
 * 
 * 1) All trigger implementation (subclass of FNetTriggerBase) need to override method PublishFrame(...)
 *    which is the entrance of trigger.
 * 2) Before sending data to trigger by invoking PublishFrame(...), one needs to invoke SetParameter(...) to
 *    set parameters to the trigger, such as thresholds and time window size. Be aware that the trigger must
 *    have settable property or public field whose name equal to 'ParameterName', so that the method 
 *    SetParameter(...) could find the property or field and set value to it.
 *    For example, SetParameter("DFDT_Threshold","0.001") requires the trigger have a public field or settable 
 *    property "DFDT_Threshold". Case sensitive.
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
 * Modified by Ferriad at 11/2/2017
 *  1. Restruct the exception handling in trigger object creation part by throwing all exception of the most shallow layer.
 *  2. Format the error message. 
 *  3. Fix a bug? around line #244 that it should be field name not property name.
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using FNetBaseLib.YWPLib;
//using MySql.Data.MySqlClient;
using System.Configuration;
using System.Net;
using System.Net.Sockets;

namespace FNetBaseLib
{
    public abstract class FNetTriggerBase
    {       

        public string MonitoredGrid;
        public int FramesPerSecond;
        public bool IfRemoveBadData = true;

        //private static MySqlConnection _mySQLCon = new MySqlConnection();
        private static DateTime _nextConnectTime = DateTime.MinValue;
        private static YWPLog _triggerLogger;
        private static YWPLogTypeCFG _logType_TriggerHeartBeat = new YWPLogTypeCFG("TriggerHeartBeat", "", true, true, YWPLogTypeCFG.ExpireType.Monthly);
        //public static string MonitorHost = "host";
        //public static int MonitorHostPort = 8000;
        public static string[] HeartBeatDestination;
        public static int HeartBeatPort = 0;
        public static string ThisNodeName = "trigger@powerit2";
        public static Dictionary<string, IPEndPoint> IPEndPointList = new Dictionary<string, IPEndPoint>();

        //public static string DBAddress;
        //public static string DBUser;
        //public static string DBPWD;
        //public static string DBName;
        //public static bool DBCredentialWorks = false;
        private static int _updateIntervalInSec = 30;
        //private static IPEndPoint RemoteEndPoint = null;
        private static Socket RemoteSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        private string _triggerType = "";
        private DateTime _lastUpdateTime = DateTime.Now;
        private DateTime _nextUpdateTime = DateTime.Now.AddSeconds(_updateIntervalInSec - 1);
        private int _processedFrames = 0;
        private int _processedFDRCounts = 0;
        private int _exceptionCounts = 0;
        public int ExceptionCountLastPeriod = 0;
        public int ProcessedFramesLastPeriod = 0;
        public double ProcessedFDRLastPeriod = 0;
        public double TimeRatioLastPeriod = 0;
        /// <summary>
        /// Value counts processed since the trigger started
        /// </summary>
        private Int64 _processedValueCountsTotal = 0;
        private Int64 _reportedCounts = 0;
        private Int64 _processedFramesTotal = 0;
        private double _timeConsumptionInSec = 0;
        private StringBuilder _errorMessages = new StringBuilder();
        private StringBuilder _missedFrames = new StringBuilder();
        private DateTime _lastFrameTime = DateTime.MinValue;
        private List<int> _locker = new List<int>();
        public static TriggerStatus TriggerInfoToPlot;
        static FNetTriggerBase()
        {
            _triggerLogger = new YWPLog("TriggerLog\\", new YWPLogTypeCFG("TriggerGeneralLog", "", true, true, YWPLogTypeCFG.ExpireType.Monthly));
            WriteGeneralLog("");
            WriteGeneralLog("");
            WriteGeneralLog("========================================================");
            WriteGeneralLog("Service is started.");
            WriteGeneralLog("========================================================");
            WriteGeneralLog("");
            WriteGeneralLog("");
        }
        //private static void ConnectToDB()
        //{
        //    try
        //    {
        //        if (DateTime.Now.CompareTo(_nextConnectTime) < 0) return;
        //        _mySQLCon = new MySqlConnection(string.Format("SERVER = {0}; DATABASE = {1}; UID = {2}; PASSWORD = {3};", DBAddress, DBName, DBUser, DBPWD));
        //        _mySQLCon.Open();
        //        DBCredentialWorks = true;
        //        _triggerLogger.WriteLog(string.Format("Database connected: SERVER = {0}; DATABASE = {1}; UID = {2}; PASSWORD = {3};", DBAddress, DBName, DBUser, DBPWD));
        //    }
        //    catch (Exception e)
        //    {
        //        _triggerLogger.WriteLog(string.Format("Failed to connect database: SERVER = {0}; DATABASE = {1}; UID = {2}; PASSWORD = {3};", DBAddress, DBName, DBUser, DBPWD));
        //        _nextConnectTime = DateTime.Now.AddSeconds(30);
        //        DBCredentialWorks = false;
        //    }
        //}
        private static void SendMsgToMonitor(string msg)
        {            
            var data = Encoding.ASCII.GetBytes(msg);
            if (IPEndPointList == null || IPEndPointList.Count == 0)
                WriteGeneralLog("Destination IP address of the message is unknown. Msg: " + msg);
            else
            {
                foreach (var ep in IPEndPointList)
                {
                    try { RemoteSocket.SendTo(data, data.Length, SocketFlags.None, ep.Value); }
                    catch (Exception e)
                    {
                        WriteGeneralLog(string.Format("Failed to send message to {0}. Msg: {1}. Exception message: {2}", ep.ToString(), msg, e.Message));
                    }
                }
            }
        }
        private static void UpdateInformation(TriggerStatus Info)
        {
            if (Info == null) return;
            _triggerLogger.WriteLog(Info.ToString(), _logType_TriggerHeartBeat);
            //try { SendMsgToMonitor(Info.ToJson(), MonitorHost, MonitorHostPort); } catch { } //Old heart beat monitor.

            try
            {
                SendMsgToMonitor(Info.Json_AveFDRCount());
                SendMsgToMonitor(Info.Json_ProcessedFrames());
            }
            catch { }

            //lock (_mySQLCon)
            //{
            //    try
            //    {
            //        if (_mySQLCon == null || _mySQLCon.State != System.Data.ConnectionState.Open)
            //            ConnectToDB();
            //        if (_mySQLCon != null && _mySQLCon.State == System.Data.ConnectionState.Open)
            //        {
            //            string cmdString = string.Format("insert into triggerheartbeat (`LocalTime`,DataTime,TriggerType,Grid,Status,ProcessedFrames,AveFDRCount,TimeConsumptionRatio,Message) values('{0}','{1}','{2}','{3}','{4}',{5},{6},{7},'{8}')",
            //                Info.LocalTime.ToString("yyyy-MM-dd HH:mm:ss"),
            //                Info.DataTime.ToString("yyyy-MM-dd HH:mm:ss"),
            //                Info.TriggerType, Info.Grid, Info.Status, Info.ProcessedFrames.ToString(), Info.AveFDRCount.ToString("F1"), Info.TimeConsumptionRatio.ToString("F6"), Info.Message);
            //            MySqlCommand cmd = new MySqlCommand(cmdString, _mySQLCon);
            //            cmd.ExecuteNonQuery();
            //        }
            //    }
            //    catch(Exception e)
            //    {
            //        _triggerLogger.WriteLog("Error occurs when try to update information. Error msg:" + e.Message + "\r\nStack:" + e.StackTrace);
            //    }
            //}
        }
  
        public static void WriteGeneralLog(string Msg)
        {
            _triggerLogger.WriteLog(Msg);
        }
        public static void WriteLog(string Msg,YWPLogTypeCFG logCFG)
        {
            _triggerLogger.WriteLog(Msg,logCFG);
        }
        public static FNetTriggerBase CreateTriggerObjByCFGName(string TriggerCFGName, string Grid,int FramePerSec, string TriggerCFGFile = "FNETTriggerConfiguration.config")
        {
            try
            {
                FNetTriggerBase TriggerObj = FNETTriggerCFGManager.CreateTriggerObj(TriggerCFGName, TriggerCFGFile);
                if (TriggerObj == null)
                {
                    WriteGeneralLog(string.Format("Failed to create trigger object. CFGName={0}, Grid={1}", TriggerCFGName, TriggerObj.MonitoredGrid));
                    throw new Exception("Trigger object is null..");
                }
                //TriggerObj.MonitoredGrid = Grid;
                TriggerObj.FramesPerSecond = FramePerSec;
                _triggerLogger.WriteLog("Trigger object is created according to configuration " + TriggerCFGName);
                return TriggerObj;
            }
            catch (Exception e)
            {
                _triggerLogger.WriteLog(string.Format("Failed to create trigger object according to configuration {0}. Error message: {1}", TriggerCFGName, e.Message));
                //throw;
            }
            return null;

        }
        TriggerStatus Info = null;
        public TriggerStatus DetectWithMonitor(FNetFrame frame)
        {
            //WriteGeneralLog(ThisNodeName + "_" + this._triggerType);
            string res = "";
            //if (frame == null) return "";
            if (frame == null) return null;
            //------Log missed frames------
            if (_lastFrameTime != DateTime.MinValue)
            {
                double span = frame.TimeStamp.Subtract(_lastFrameTime).TotalSeconds;
                if (span > 2.5 / FramesPerSecond)
                    _missedFrames.AppendLine(string.Format("({0}, {1})", _lastFrameTime.ToString("HH:mm:ss.fff"), frame.TimeStamp.ToString("HH:mm:ss.fff")));
                else if (span > 1.5 / FramesPerSecond)
                    _missedFrames.AppendLine(" " + _lastFrameTime.AddSeconds(1.0 / FramesPerSecond).ToString("HH:mm:ss.fff"));
            }
            _lastFrameTime = frame.TimeStamp;
            //------Run trigger logic--------
            
             
            _processedFrames += 1;
            _processedFDRCounts += frame.Measurements.Count;
            _processedFramesTotal += 1;
            DateTime start = DateTime.Now;
            //try
            //{

            if (IfRemoveBadData)
                RemoveBadUnit(frame);
            try
            {
                lock (_locker)
                    Detect(frame);
            }
            catch (Exception ee)
            {
                _exceptionCounts += 1;
                throw ee;
            }
            //}
            //catch(Exception e)
            //{
            //    _errorMessages.Append(string.Format("\r\n---------\r\n{0}\t{1}\r\nError Msg:{2}\r\nStack:\r\n{3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), frame.TimeStamp.ToString("HH:mm:ss.fff"), e.Message, e.StackTrace));
            //}
            //--------Collect information -----------------TriggerStatus Info = new TriggerStatus();
            
            _timeConsumptionInSec += DateTime.Now.Subtract(start).TotalSeconds;            
            //--------Update information to database-------
            if (DateTime.Now.CompareTo(_nextUpdateTime) >= 0)// && DateTime.Now.Second % _updateIntervalInSec == 0)
            {
                Info = new TriggerStatus();
                _processedValueCountsTotal += _processedFDRCounts;
                _reportedCounts += 1;
                _nextUpdateTime = DateTime.Now.AddSeconds(_updateIntervalInSec);
                //TriggerStatus Info = new TriggerStatus();
                Info.LocalTime = DateTime.Now;
                Info.DataTime = frame.TimeStamp;
                if (_triggerType == "" || _triggerType.Length < 2)
                    _triggerType = this.GetType().Name;
                Info.TriggerType = _triggerType;
                Info.Grid = MonitoredGrid;
                if (_errorMessages.Length > 0)
                {
                    Info.Status = "Error";
                    Info.Message = _errorMessages.ToString() + "\r\n";
                }
                else if (_processedFDRCounts < 0.7 * _processedValueCountsTotal / _reportedCounts && _processedValueCountsTotal / _processedFramesTotal>3.5)
                {
                    Info.Status = "Warning";
                    Info.Message += string.Format("Less than 70% data were received.[Cur:{0} ,Ave:{1}]", _processedFDRCounts, ((double)(_processedValueCountsTotal / _reportedCounts)).ToString("F3"));
                }
                else
                    Info.Status = "Normal";
                if (_missedFrames.Length > 0)
                    Info.Message += "***Missed Frames***\r\n" + _missedFrames.ToString();
                if (Info.Message.Length > 60000) 
                    Info.Message = Info.Message.Substring(0, 60000) + "\r\n....";
                Info.ProcessedFrames = _processedFrames;
                ExceptionCountLastPeriod = _exceptionCounts;
                if (_processedFrames > 0) Info.AveFDRCount = _processedFDRCounts / (double)_processedFrames;
                Info.TimeConsumptionRatio = _timeConsumptionInSec / (DateTime.Now.Subtract(_lastUpdateTime).TotalSeconds);
                ProcessedFramesLastPeriod = _processedFrames;
                ProcessedFDRLastPeriod = Info.AveFDRCount;
                TimeRatioLastPeriod = Info.TimeConsumptionRatio;
                UpdateInformation(Info);
                TriggerInfoToPlot = Info;
                //---------------Add TriggerInfo to Buffer-----------------
                //FNetTriggerMeasurement temp = new FNetTriggerMeasurement(Info.Grid, Info.LocalTime);
                //temp.ProcessedFrame = Info.ProcessedFrames;
                //temp.avgFDRcount = Info.AveFDRCount;
                //processedFrameBufferList.Push(temp);            

                //---------------Reset-----------------
                _lastUpdateTime = DateTime.Now;
                _nextUpdateTime = DateTime.Now.AddSeconds(_updateIntervalInSec);
                _processedFrames = 0;
                _processedFDRCounts = 0;
                _exceptionCounts = 0;
                _timeConsumptionInSec = 0;
                _errorMessages.Clear();
                _missedFrames.Clear();
            }
            return Info;
        }
        /// <summary>
        /// Main logic of trigger
        /// </summary>
        /// <param name="frame">All data of one time frame</param>
        public abstract void Detect(FNetFrame frame);
        /// <summary>
        /// Set trigger's parameters.
        /// Before sending data to trigger by invoking PublishFrame(...), one needs to invoke SetParameter(...) to
        /// set parameters to the trigger, such as thresholds and time window size.Be aware that the trigger must
        /// have settable property or public field whose name equal to 'ParameterName', so that the method 
        /// SetParameter(...) could find the property or field and set value to it.
        /// For example, SetParameter("DFDT_Threshold","0.001") requires the trigger have a public field or settable
        /// property named "DFDT_Threshold". Case sensitive.
        /// </summary>
        /// <param name="ParameterName">Name of parameter, which must be same with the corresponding settable property or public field of the trigger </param>
        /// <param name="ParameterValue">Value of the parameter in String format</param>
        public void SetParameter(string ParameterName,string ParameterValue)
        {
            Type ThisType = this.GetType();
            PropertyInfo OnePI = ThisType.GetProperty(ParameterName);
            if (OnePI != null)
            {
                MethodInfo MI = typeof(Convert).GetMethod("To" + OnePI.PropertyType.Name, new Type[] { typeof(string) });
                if (MI == null)
                    throw new Exception(String.Format("Error converting property name. Message: check \'{0}' format.", OnePI.PropertyType.Name));
                Object ParaValue = MI.Invoke(null, new object[] { ParameterValue });
                OnePI.SetValue(this, ParaValue);
            }
            else
            {
                var OneFI = ThisType.GetField(ParameterName);
                if (OneFI != null)
                {
                    MethodInfo MI = typeof(Convert).GetMethod("To" + OneFI.FieldType.Name, new Type[] { typeof(string) });
                    if (MI == null)
                        throw new Exception(String.Format("Error converting field name.  Message: check \'{0}' format.", OneFI.FieldType.Name));
                    Object ParaValue = MI.Invoke(null, new object[] { ParameterValue });
                    OneFI.SetValue(this, ParaValue);
                }
                else
                    throw new Exception(String.Format("Error finding Property/public field named '{0}'", ParameterName));
            }            
        }
        /// <summary>
        /// Set trigger's parameters.
        /// Before sending data to trigger by invoking PublishFrame(...), one needs to invoke SetParameter(...) to
        /// set parameters to the trigger, such as thresholds and time window size.Be aware that the trigger must
        /// have settable property or public field whose name equal to 'ParameterName', so that the method 
        /// SetParameter(...) could find the property or field and set value to it.
        /// For example, SetParameter("DFDT_Threshold","0.001") requires the trigger have a public field or settable
        /// property named "DFDT_Threshold". Case sensitive.
        /// </summary>
        public void SetParameter(ParameterAndValue ParaAndValue)
        {
            if(ParaAndValue!=null)
                SetParameter(ParaAndValue.ParameterName, ParaAndValue.ParameterValue);
        }
        /// <summary>
        /// Set trigger's parameters.
        /// Before sending data to trigger by invoking PublishFrame(...), one needs to invoke SetParameter(...) to
        /// set parameters to the trigger, such as thresholds and time window size.Be aware that the trigger must
        /// have settable property or public field whose name equal to 'ParameterName', so that the method 
        /// SetParameter(...) could find the property or field and set value to it.
        /// For example, SetParameter("DFDT_Threshold","0.001") requires the trigger have a public field or settable
        /// property named "DFDT_Threshold". Case sensitive.
        /// </summary>
        public void SetParameter(List<ParameterAndValue> ParaAndValues)
        {
            if (ParaAndValues != null)
                foreach(var v in ParaAndValues)
                    SetParameter(v.ParameterName, v.ParameterValue);
        }

        //------------------------------------------------------------
        //for check if any unit is islanded.
        private Dictionary<int, DataCheckStatus> _UnitsCheckStatus = new Dictionary<int, DataCheckStatus>();
        private int _AveWindowInSeconds = 20;//3;
        private double _FreqDevThreshold = 0.004;
        private YWPLogTypeCFG FreqDevLog = new YWPLogTypeCFG("FreqDev", "", true, false, YWPLogTypeCFG.ExpireType.Daily);

        /// <summary>
        /// Test if any unit is islanded or frequency has large deviation from system median freq.
        /// The islanded unit will be removed from the data list.
        /// </summary>
        /// <param name="frame"></param>
        private void RemoveBadUnit(FNetFrame frame)
        {
            double sysFreq = GetMedianFrequency(frame);
            for(int i=0;i<frame.Measurements.Count;i++) 
            {
                var d = frame.Measurements[i];
                if (!_UnitsCheckStatus.ContainsKey(d.FDRID))
                    _UnitsCheckStatus.Add(d.FDRID, new DataCheckStatus(d.FDRID));
                _UnitsCheckStatus[d.FDRID].AveFreqDev = 
                    _UnitsCheckStatus[d.FDRID].AveFreqDev / (FramesPerSecond * _AveWindowInSeconds) * (FramesPerSecond * _AveWindowInSeconds - 1) 
                    + (d.Frequency - sysFreq) / (FramesPerSecond * _AveWindowInSeconds);
                if (Math.Abs(_UnitsCheckStatus[d.FDRID].AveFreqDev) >= _FreqDevThreshold)
                    _UnitsCheckStatus[d.FDRID].SetFlag();
                if(_UnitsCheckStatus[d.FDRID].IsBadData())
                {
                    frame.Measurements.RemoveAt(i);
                    i -= 1;
                }
                if(frame.TimeStamp.Millisecond==0&&frame.TimeStamp.Second%5==0)
                {
                    WriteLog(string.Format("FreqDev,{0},{1},{2},{3}", frame.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss.fff"), d.FDRID, _UnitsCheckStatus[d.FDRID].AveFreqDev.ToString("F8"), _UnitsCheckStatus[d.FDRID].IsBadData()), FreqDevLog);
                }
            }
        }
        
        private double GetMedianFrequency(FNetFrame frame)
        {
            if (frame == null || frame.Measurements == null || frame.Measurements.Count == 0) return 0;
            var Freqs = frame.Measurements.Where(x => x.HasFrequency).Select(x => x.Frequency).ToList();
            if (Freqs.Count == 0) return 0;
            Freqs.Sort();
            return Freqs.Count % 2 == 0 ? (Freqs[Freqs.Count / 2] + Freqs[Freqs.Count / 2 - 1]) / 2 : Freqs[Freqs.Count / 2];
        }
        
        class DataCheckStatus
        {
            public int UnitID=0;
            public double AveFreqDev = 0;
            private bool BadDataFlag = false;
            private DateTime FlagExpireTime = DateTime.MinValue;
            public DataCheckStatus(int id)
            {
                UnitID = id;
            }
            public void SetFlag()
            {
                BadDataFlag = true;
                FlagExpireTime = DateTime.Now.AddMinutes(2);
            }
            public bool IsBadData()
            {
                if (!BadDataFlag) return false;
                if(DateTime.Now.CompareTo(FlagExpireTime)>=0)
                {
                    BadDataFlag = false;
                    FlagExpireTime = DateTime.MinValue;
                }
                return BadDataFlag;
            }

        }
    }
    public class ParameterAndValue
    {
        public string ParameterName;
        public string ParameterValue;
        public ParameterAndValue(string paraName,string paraValue)
        {
            ParameterName = paraName;
            ParameterValue = paraValue;
        }
        public ParameterAndValue(string paraName, object paraValue)
        {
            ParameterName = paraName;
            ParameterValue = paraValue.ToString();
        }
    }
    public class TriggerStatus
    {
        public DateTime LocalTime;
        public DateTime DataTime;
        public string TriggerType;
        public string Grid;
        public string Status = "Normal";
        public int ProcessedFrames = 0;
        public double AveFDRCount = 0;
        public double TimeConsumptionRatio = 0;
        public string Message = "";
        public override string ToString()
        {
            return string.Format("[UTC],{0},{1,30},{2,8},{3,8},{4,4},{5,4},{6,8},{7}",
                DataTime.ToString("yyyy-MM-dd HH:mm:ss"),
                TriggerType,
                Grid,
                Status,
                ProcessedFrames,
                AveFDRCount.ToString("F1"),
                TimeConsumptionRatio.ToString("F6"),
                Message.Replace("\r\n", " || ")
                );
        }
        public string Json_ProcessedFrames()
        {
            return string.Format("{{\"NodeName\":\"{0}\",\"StatusName\":\"{1}\",\"StatusValue\":{2} }}",
                FNetTriggerBase.ThisNodeName, TriggerType + "_" + Grid + "_ProcessedFrames", ProcessedFrames);
        }
        public string Json_AveFDRCount()
        {
            return string.Format("{{\"NodeName\":\"{0}\",\"StatusName\":\"{1}\",\"StatusValue\":{2}",
                FNetTriggerBase.ThisNodeName, TriggerType + "_" + Grid + "_AveFDRCount", AveFDRCount.ToString("F2"));
        }
        public string ToJson()
        {
            return string.Format("{{\"LocalTime\":\"{0}\",\"DataTime\":\"{1}\",\"TriggerType\":\"{2}\",\"Grid\":\"{3}\",\"Status\":\"{4}\",\"ProcessedFrames\":{5},\"AveFDRCount\":{6},\"TimeConsumptionRatio\":{7},\"Message\":\"{8}\"}}",
                LocalTime.ToString("yyyy-MM-dd HH:mm:ss"),
                DataTime.ToString("yyyy-MM-dd HH:mm:ss"),
                TriggerType,
                Grid,
                Status,
                ProcessedFrames,
                AveFDRCount.ToString("F3"),
                TimeConsumptionRatio.ToString("F8"),
                Message
                );
        }


    }
}
