﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FNetBaseLib.YWPLib;

namespace FNetBaseLib
{
    public class IslandingDetection
    {
        /// <summary>
        /// List of units which are islanding. Key:unit id, Value: Islanding status expire time. 
        /// </summary>
        private static Dictionary<int, DateTime> IslandingUnits = new Dictionary<int, DateTime>();
        private static YWPLogTypeCFG _logType_islanding = new YWPLogTypeCFG("IslandingFDRs", "", true, true, YWPLogTypeCFG.ExpireType.Yearly);
        private static Timer T;
        private static void RegularlyLog(object sender, ElapsedEventArgs e)
        {
            try { LogCurrentIslandingUnit(); }
            catch (Exception ex) { Log("Error occurs when regularly check islanding units. Error message:" + ex.Message); }
        }
        //TODO:dispose a static timer?
        private static void LogCurrentIslandingUnit()
        {
            List<int> Units = GetIslandedFDRss();
            if (Units.Count == 0) return;
            StringBuilder sb = new StringBuilder();
            foreach (var i in Units) sb.Append(string.Format("{0,4:####}\t", i));
            DateTime utcT= TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified), "Eastern Standard Time", "UTC");
            Log(string.Format("[{0:MM/dd/yyyy HH:mm}]\t{1}", utcT, sb.ToString()));
        }
        private static void Log(string Msg) { FNetTriggerBase.WriteLog(Msg, _logType_islanding); }
        static IslandingDetection()
        {
            try
            {
                T = new Timer(20000);
                T.Elapsed += new ElapsedEventHandler(RegularlyLog);
                T.AutoReset = true;
                T.Start();                
            }
            catch(Exception e)
            {
                Log("Failed to initialize IslandingDetection, error message:" + e.Message);
            }            
        }
        /// <summary>
        /// Set islanding units, whose islanding flag will expire in MinutesToExpire minutes.
        /// </summary>
        /// <param name="IslandedFDRIDList">List of units id.</param>
        /// <param name="MinutesToExpire">The islanding flag will expire in MinutesToExpire minutes. Default value is 5 minutes</param>
        public static void SetIslandingUnit(List<int> IslandedFDRIDList, double MinutesToExpire = 0.5)
        {
            foreach (int uid in IslandedFDRIDList)
                SetIslandingUnit(uid, MinutesToExpire);
        }
        /// <summary>
        /// Set islanding units, whose islanding flag will expire in MinutesToExpire minutes.
        /// </summary>
        /// <param name="IslandedFDRID">ID of islanding unit</param>
        /// <param name="MinutesToExpire">The islanding flag will expire in MinutesToExpire minutes. Default value is 5 minutes</param>
        public static void SetIslandingUnit(int IslandedFDRID, double MinutesToExpire = 0.5)
        {
            lock (IslandingUnits)
            {
                if (IslandingUnits.ContainsKey(IslandedFDRID)) IslandingUnits[IslandedFDRID] = DateTime.Now.AddMinutes(MinutesToExpire);
                else IslandingUnits.Add(IslandedFDRID, DateTime.Now.AddMinutes(MinutesToExpire));
            }
        }
        /// <summary>
        /// Check if a unit is islanding.
        /// </summary>
        public static bool IsFDRIslanded(int FDRID)
        {
            lock(IslandingUnits)
            {
                if (!IslandingUnits.ContainsKey(FDRID)) return false;
                if (IslandingUnits[FDRID].CompareTo(DateTime.Now) > 0) return true;
                else { IslandingUnits.Remove(FDRID);return false; }
            }
        }
        /// <summary>
        /// Get current islanding unit list.
        /// </summary>
        public static List<int> GetIslandedFDRss()
        {
            List<int> result = new List<int>(IslandingUnits.Count);
            lock(IslandingUnits)
            {
                foreach(var v in IslandingUnits)
                    if (v.Value.CompareTo(DateTime.Now) > 0)
                        result.Add(v.Key);
            }
            return result;
        }
        
    }
}
