﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FNetBaseLib
{
    public class SimpleParaCFG
    {
        public string CFGFile;
        public Dictionary<string, string> ParameterDic = new Dictionary<string, string>();
        public SimpleParaCFG(string cfgFile)
        {
            CFGFile = cfgFile;
            if (!File.Exists(cfgFile))
                return;
            using (StreamReader sr = new StreamReader(cfgFile))
            {
                string strLine;
                while(!sr.EndOfStream)
                {
                    strLine = sr.ReadLine();
                    if (strLine == "" || strLine.Length < 6 || strLine.StartsWith("#"))
                        continue;
                    string[] arr = strLine.Trim().Split('=');
                    if (arr.Length != 2)
                        continue;
                    string key = arr[0].Trim().Substring(1, arr[0].Length - 2);
                    string value= arr[1].Trim().Substring(1, arr[0].Length - 2);
                    if (!ParameterDic.ContainsKey(key))
                        ParameterDic.Add(key, value);
                    else
                        ParameterDic[key] = value;
                }
            }

        }
        public string GetValue(string key)
        {
            if (ParameterDic.ContainsKey(key))
                return ParameterDic[key];
            else
                return "";
        }
        public void SetValue(string key, string value)
        {
            if (ParameterDic.ContainsKey(key))
                ParameterDic[key] = value;
            else
                ParameterDic.Add(key, value);
        }
        public string this[string key]
        {
            get { return GetValue(key); }
            set { SetValue(key, value); }
        }

    }
}
