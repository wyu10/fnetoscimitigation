﻿/*******************************************************************************************
 * Base class of FNetMeasurement.
 * Data structure for all triggers.
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetBaseLib
{
    public abstract class TimeStamped
    {
        public DateTime TimeStamp;
    }
}
