﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNetMonitoring
{
    class CommandManager
    {
        private string _CommandDestination;

        private int _Port;
        private StatusReporter _CommandSender = null;
        public CommandManager(string cmdDestIP, int port)
        {
            _CommandDestination = cmdDestIP;
            _Port = port;
            _CommandSender = new StatusReporter(_CommandDestination, _Port);
        }

        public void SendRelayCommand(bool EnableWADC, bool EnablePSS, string WADCName)
        {
            if (_CommandSender == null)
                return;
            //string strcmd = "CMD:Relay="+(EnablePSS ? "AA" : "00") + (EnableWADC ? "55" : "00");
            string strcmd = string.Format("CMD:Relay={0}{1}@{2}", (EnablePSS ? "AA" : "00"), (EnableWADC ? "55" : "00"), WADCName);
            _CommandSender.ReportStatus(strcmd);
        }
        public void SendEnableWADCCommand(bool EnableWADC, string WADCName)
        {
            if (_CommandSender == null)
                return;
            string strcmd = string.Format("CMD:EnableWADC={0}@{1}", (EnableWADC ? "1" : "0"), WADCName);
            _CommandSender.ReportStatus(strcmd);
        }
    }
}
