﻿namespace FNetMonitoring
{
    partial class DisableMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NameAndInfo = new System.Windows.Forms.Label();
            this.cb_Disable = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NameAndInfo
            // 
            this.NameAndInfo.AutoSize = true;
            this.NameAndInfo.Location = new System.Drawing.Point(19, 16);
            this.NameAndInfo.Name = "NameAndInfo";
            this.NameAndInfo.Size = new System.Drawing.Size(35, 13);
            this.NameAndInfo.TabIndex = 0;
            this.NameAndInfo.Text = "Name";
            // 
            // cb_Disable
            // 
            this.cb_Disable.AutoSize = true;
            this.cb_Disable.Location = new System.Drawing.Point(63, 41);
            this.cb_Disable.Name = "cb_Disable";
            this.cb_Disable.Size = new System.Drawing.Size(133, 17);
            this.cb_Disable.TabIndex = 1;
            this.cb_Disable.Text = "Temperarily Disable for";
            this.cb_Disable.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(193, 38);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(63, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = "72";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(258, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "hours.";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(136, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Confirm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // DisableMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 111);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.cb_Disable);
            this.Controls.Add(this.NameAndInfo);
            this.Name = "DisableMonitor";
            this.Text = "Disable Monitor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label NameAndInfo;
        private System.Windows.Forms.CheckBox cb_Disable;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}