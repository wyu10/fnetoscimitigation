﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FNetBaseLib;
using FNetBaseLib.YWPLib;

namespace FNetMonitoring
{
    public class FNetMonitor
    {
        public readonly string NodeName;
        /// <summary>
        /// The reported status name, such as cpu usage, memory, FDR number...
        /// </summary>
        public readonly string StatusName;
        public readonly string Key;
        /// <summary>
        /// The interval of
        /// </summary>
        public static int MinMsgInterval = 300;
        
        public string[] Emails;

        private MonitorCFG _MonitorCfg;
        private DateTime _LastRptTime = DateTime.Now;
        private DateTime _NextMsgTime = DateTime.Now;
        private int _NextMsgIntervalInSec = 120;

        public static int BufferLengthInSec = 86400;
        private List<ValueMonitor> _MonitorList = new List<ValueMonitor>();
        public UIMonitor MonitorTreeNode;
        public YWPTimeSeriesBuffer<TimeStampedValue> ReceivedValues = new YWPTimeSeriesBuffer<TimeStampedValue>(0.03, BufferLengthInSec);
        public event PlotMultiSeries.AddDataPoints DataReceived;
        public DateTime TemperaryDisableUntil = DateTime.MinValue;
        public Group ThisGroup = null;
        public bool IsTrueGfg = true;
        public FNetMonitor(MonitorCFG cfg, bool isTrueCfg=true)
        {
            if (cfg == null)
                return;
            _MonitorCfg = cfg;
            NodeName = cfg.NodeName;
            StatusName = cfg.StatusName;
            Key = FNetMonitorManager.GetStatusKey(NodeName, StatusName);
            Emails = cfg.EmailReceivers.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            if (cfg.DurationInSec_First>0)
                _MonitorList.Add(new ValueMonitor(NodeName, StatusName, cfg.AlertLarger, cfg.AbnormalTH_First, cfg.DurationInSec_First));
            if (cfg.DurationInSec_Second > 0)
                _MonitorList.Add(new ValueMonitor(NodeName, StatusName + " (Second)", cfg.AlertLarger, cfg.AbnormalTH_Second, cfg.DurationInSec_Second));

            IsTrueGfg = isTrueCfg;
            MonitorTreeNode = new UIMonitor();
            MonitorTreeNode.ThisMonitor = this;
            MonitorTreeNode.Text = StatusName;
            if (!isTrueCfg)
                MonitorTreeNode.ForeColor = System.Drawing.Color.DarkRed;
            //add child nodes
            if (isTrueCfg)
            {
                if (cfg.DurationInSec_First == 0)
                    MonitorTreeNode.Nodes.Add("First: Disabled.");
                else
                {
                    if (cfg.DurationInSec_First < 86400)
                        MonitorTreeNode.Nodes.Add(string.Format("First: {0} ({1} sec)", cfg.AbnormalTH_First.ToString("F3"), cfg.DurationInSec_First.ToString()));
                    else
                        MonitorTreeNode.Nodes.Add(string.Format("First: {0} ({1} days)", cfg.AbnormalTH_First.ToString("F3"), (cfg.DurationInSec_First / 86400.0).ToString("F1")));
                }
                if (cfg.DurationInSec_Second == 0)
                    MonitorTreeNode.Nodes.Add("Second: Disabled.");
                else
                {
                    if (cfg.DurationInSec_Second < 86400)
                        MonitorTreeNode.Nodes.Add(string.Format("Second: {0} ({1} sec)", cfg.AbnormalTH_Second.ToString("F3"), cfg.DurationInSec_Second.ToString()));
                    else
                        MonitorTreeNode.Nodes.Add(string.Format("Second: {0} ({1} days)", cfg.AbnormalTH_Second.ToString("F3"), (cfg.DurationInSec_Second / 86400.0).ToString("F1")));
                }
                if (cfg.AbnormalTH_NoRptTimeInSec > 0)
                {
                    if (cfg.AbnormalTH_NoRptTimeInSec < 86400)
                        MonitorTreeNode.Nodes.Add("No-Report Alert: " + cfg.AbnormalTH_NoRptTimeInSec.ToString() + " sec");
                    else
                        MonitorTreeNode.Nodes.Add("No-Report Alert: " + (cfg.AbnormalTH_NoRptTimeInSec / 86400.0).ToString("F1") + " days");
                }
                else
                    MonitorTreeNode.Nodes.Add("No-Report Alert: Disabled.");
                MonitorTreeNode.Nodes.Add(cfg.EmailReceivers);
            }
            if(cfg.GroupName!=null &&cfg.GroupName.Length>0)
            {
                if (!Group.GroupDic.ContainsKey(cfg.GroupName))
                    Group.GroupDic.Add(cfg.GroupName, new Group(cfg.GroupName));
                Group.GroupDic[cfg.GroupName].Monitores.Add(this);
                this.ThisGroup = Group.GroupDic[cfg.GroupName];
            }

        }
        public string ReportStatusValue(double statusValue, DateTime timestamp)
        {
            if (_MonitorCfg == null)
                return "";
            if (DataReceived != null)
            {
                //DataReceived(new List<PlotMultiSeries.SeriesNameAndTimedValue>() { new PlotMultiSeries.SeriesNameAndTimedValue(Key, DateTime.Now, statusValue) });
                DataReceived(new List<PlotMultiSeries.SeriesNameAndTimedValue>() { new PlotMultiSeries.SeriesNameAndTimedValue(Key, timestamp, statusValue) });
            }
            //ReceivedValues.Push(new TimeStampedValue(DateTime.Now, statusValue));
            lock (ReceivedValues)
            {
                ReceivedValues.Push(new TimeStampedValue(timestamp, statusValue));
            }
            _LastRptTime = DateTime.Now;
            if (DateTime.Now.CompareTo(TemperaryDisableUntil) < 0)
                return "";
            StringBuilder sb = new StringBuilder();
            foreach (var v in _MonitorList)
                sb.Append(v.ReportStatusValue(statusValue));
            return sb.ToString();
        }
        public string CheckNoReport()
        {           
            if (_MonitorCfg == null || _MonitorCfg.AbnormalTH_NoRptTimeInSec <= 0)
                return "";
            if (DateTime.Now.CompareTo(TemperaryDisableUntil) < 0)
                return "";
            StringBuilder sb = new StringBuilder();
            if(_NextMsgTime.CompareTo(DateTime.Now)<0 &&_LastRptTime.AddSeconds(_MonitorCfg.AbnormalTH_NoRptTimeInSec).CompareTo(DateTime.Now)<0)
            {
                //Generate message
                sb.AppendLine("-----------------------");
                sb.AppendLine("* Node Name: " + NodeName);
                sb.AppendLine("* Status Name: " + StatusName);
                sb.AppendLine("Last Report Time: " + _LastRptTime.ToString("yyyy-MM-dd HH:mm:ss"));
                sb.AppendLine("No-report Duration: " + DateTime.Now.Subtract(_LastRptTime).TotalMinutes.ToString() + " minutes");
                //refresh report time
                RefreshNextMsgTimeAndInterval(ref _NextMsgTime, ref _NextMsgIntervalInSec);
            }
            return sb.ToString();
        }
        public static void RefreshNextMsgTimeAndInterval(ref DateTime nextMsgTime, ref int nextMsgInterval)
        {
            if (DateTime.Now.Subtract(nextMsgTime).TotalSeconds > MinMsgInterval * 2 && DateTime.Now.Subtract(nextMsgTime).TotalSeconds > nextMsgInterval >> 1)    //Reset interval because of time out
                nextMsgInterval = MinMsgInterval;
            else
            {
                nextMsgInterval += nextMsgInterval;
                if (nextMsgInterval > 3600)
                    nextMsgInterval = 3600;
            }
            nextMsgTime = DateTime.Now.AddSeconds(nextMsgInterval);
        }
    }
    public class ValueMonitor
    {
        public string NodeName;
        public string StatusName;
        public bool AlertLarger;
        public double Threshold;
        public int DurationInSecond = 0;

        private DateTime _AbnormalTime = DateTime.MaxValue;
        private int _AbnormalRptCnt = 0;
        private double _AbnormalValueAve = 0;
        private double _ShortTermAve = 0;
        private double _LongTermAve = 0;
        private int _CurAveCount = 0;

        private DateTime _NextMsgTime = DateTime.Now;
        private int _NextMsgIntervalInSec = 120;

        private const int _ShortTermAveCnt = 500;
        private const int _LongTermAveCnt = 10000;
        public ValueMonitor(string nodeName, string statusName, bool alertLarger, double threshold, int duration)
        {
            NodeName = nodeName;
            StatusName = statusName;
            AlertLarger = alertLarger;
            Threshold = threshold;
            DurationInSecond = duration;
        }
        public string ReportStatusValue(double statusValue)
        {
            //----------------------------------------------------
            //Calculate avearge value
            if (_CurAveCount < _ShortTermAveCnt)
            {
                _CurAveCount++;
                _ShortTermAve = _ShortTermAve / _CurAveCount * (_CurAveCount - 1) + statusValue / _CurAveCount;
                _LongTermAve = _LongTermAve / _CurAveCount * (_CurAveCount - 1) + statusValue / _CurAveCount;
            }
            else
            {
                _ShortTermAve = _ShortTermAve / _ShortTermAveCnt * (_ShortTermAveCnt - 1) + statusValue / _ShortTermAveCnt;
                _LongTermAve = _LongTermAve / _LongTermAveCnt * (_LongTermAveCnt - 1) + statusValue / _LongTermAveCnt;
            }
            //----------------------------------------------------
            //detect abnormal value and calculate abnormal average.
            if ((AlertLarger && statusValue < Threshold) || (!AlertLarger && statusValue > Threshold))   //normal
                //if (AlertLarger != (statusValue > Threshold))   //normal
                _AbnormalTime = DateTime.MaxValue;
            else        //Abnormal
            {
                if (_AbnormalTime.CompareTo(DateTime.Now) > 0) //first abnormal
                {
                    _AbnormalTime = DateTime.Now;
                    _AbnormalRptCnt = 0;
                    _AbnormalValueAve = 0;
                }
                _AbnormalRptCnt++;
                _AbnormalValueAve = _AbnormalValueAve / _AbnormalRptCnt * (_AbnormalRptCnt - 1) + statusValue / _AbnormalRptCnt;
            }
            //----------------------------------------------------
            //Generate msg
            //if(Can generate msg, and abnormal, and duration > duration threshold)
            StringBuilder sb = new StringBuilder();
            if(_NextMsgTime.CompareTo(DateTime.Now)<=0 && _AbnormalTime.CompareTo(DateTime.Now)<0 && _AbnormalTime.AddSeconds(DurationInSecond).CompareTo(DateTime.Now)<0)
            {
                //Generate msg
                sb.AppendLine("-----------------------");
                sb.AppendLine("* Node Name: " + NodeName);
                sb.AppendLine("* Status Name: " + StatusName);
                sb.AppendLine(string.Format("Alert Threshold: {0}({1} sec)", Threshold.ToString(), DurationInSecond.ToString()));
                sb.AppendLine("Current Value: " + statusValue.ToString());
                sb.AppendLine(string.Format("Abnormal Value Average: {0}({1})",_AbnormalValueAve.ToString("F6"), _AbnormalRptCnt.ToString()));
                sb.AppendLine(string.Format("Long/Short Term Average: {0}/{1} ", _LongTermAve.ToString("F6"), _ShortTermAve.ToString("F6")));
                sb.AppendLine("Abnormal Duration: " + DateTime.Now.Subtract(_AbnormalTime).TotalMinutes.ToString("F2") + " minutes");
                sb.AppendLine("Abnormal Start Time: " + _AbnormalTime.ToString("yyyy-MM-dd HH:mm:ss"));                
                sb.AppendLine("Current Time: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                //sb.AppendLine("-----------------------");
                //Refresh msg interval and next msg time.
                FNetMonitor.RefreshNextMsgTimeAndInterval(ref _NextMsgTime, ref _NextMsgIntervalInSec);
                //if (DateTime.Now.Subtract(_NextMsgTime).TotalSeconds > 180 && DateTime.Now.Subtract(_NextMsgTime).TotalSeconds > _NextMsgIntervalInSec >> 1)
                //    _NextMsgIntervalInSec = 60;
                //else
                //{
                //    _NextMsgIntervalInSec += _NextMsgIntervalInSec >> 1;
                //    if (_NextMsgIntervalInSec > 1200)
                //        _NextMsgIntervalInSec = 1200;
                //}
                //_NextMsgTime = DateTime.Now.AddSeconds(_NextMsgIntervalInSec);
            }
            return sb.ToString();
        }

    }
}
