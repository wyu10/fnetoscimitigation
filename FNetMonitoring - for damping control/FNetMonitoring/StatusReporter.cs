﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace FNetMonitoring
{
    class StatusReporter
    {

        private string _DestinationAddress;
        private int _DestinationPort;
        private Socket _RemoteSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        private IPEndPoint _RemoteEndPoint = null;
        public StatusReporter(string destinationAddress, int destPort)
        {
            _DestinationAddress = destinationAddress;
            _DestinationPort = destPort;
            //FNetMonitorManager.DisplayMsg("destination address and port:" + destinationAddress + ", " + destPort);
            try
            {
                _RemoteEndPoint = new IPEndPoint(IPAddress.Parse(destinationAddress), destPort);
                
            }
            catch(Exception e)
            {
                var IPList = Dns.GetHostEntry(destinationAddress).AddressList;
                if (IPList.Length > 0)
                    _RemoteEndPoint = new IPEndPoint(IPList[0], destPort);
            }
            //FNetMonitorManager.DisplayMsg(string.Format("destination {0}:{1}", _RemoteEndPoint.Address.ToString(), _RemoteEndPoint.Port.ToString()));
        }
        public void ReportStatus(string nodeName, string statusName, double statusValue)
        {
            ReportStatus(new Status(nodeName, statusName, statusValue));
        }
        public void ReportStatus(Status status)
        {
            if (status == null || _RemoteEndPoint == null || _RemoteSocket==null)
                return;
            string json = status.JsonString;
            var data = Encoding.ASCII.GetBytes(json);
            try { _RemoteSocket.SendTo(data, data.Length, SocketFlags.None, _RemoteEndPoint); }
            catch (Exception e) { }
        }
        public void ReportStatus(string cmdStr)
        {
            if (cmdStr == null || cmdStr.Length==0 || _RemoteEndPoint == null || _RemoteSocket == null)
                return;
            var data = Encoding.ASCII.GetBytes(cmdStr);
            try
            {
                FNetMonitorManager.DisplayMsg(string.Format("GUI sent command: {0}, destination {1}:{2}.", cmdStr, _RemoteEndPoint.Address.ToString(), _RemoteEndPoint.Port.ToString()));
                int n =_RemoteSocket.SendTo(data, data.Length, SocketFlags.None, _RemoteEndPoint);
                FNetMonitorManager.DisplayMsg(string.Format("{0} bytes were sent.", n.ToString()));
            }
            catch (Exception e)
            {
                FNetMonitorManager.DisplayMsg(string.Format("Exception caught when send command to {0}:{1}, Exception: {2}", _RemoteEndPoint.Address, _RemoteEndPoint.Port, e.Message));
            }
        }
    }
}
