﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FNetMonitoring
{
    public partial class DisableMonitor : Form
    {
        private FNetMonitor _ThisMonitor = null;
        public DisableMonitor()
        {
            InitializeComponent();
        }
        public void AttachToMonitor(FNetMonitor monitor)
        {
            if (monitor == null)
                return;
            _ThisMonitor = monitor;
            NameAndInfo.Text = string.Format("Name:{0}, Disabled Until:{1}", _ThisMonitor.NodeName, _ThisMonitor.TemperaryDisableUntil.ToString("yyyy-MM-dd HH:mm"));
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (_ThisMonitor == null)
                return;
            try
            {
                bool disable = cb_Disable.Checked;
                double hours = Convert.ToDouble(textBox1.Text);
                if (disable)
                    _ThisMonitor.TemperaryDisableUntil = DateTime.Now.AddHours(hours);
                else
                    _ThisMonitor.TemperaryDisableUntil = DateTime.MinValue;
            }
            catch { }
            this.Close();
            this.Dispose();
        }
    }
}
