﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
//using MySql.Data.MySqlClient;
using System.Configuration;
using System.Drawing;

namespace FNetMonitoring
{
    public class FNetMonitorManager
    {
        public static FNetMonitorManager ThisManager = null;
        public List<MonitorCFG> MonitorCFGList;
        public Dictionary<string, int> NodeNameDic = new Dictionary<string, int>();
        private Dictionary<string, FNetMonitor> _MonitorList = new Dictionary<string, FNetMonitor>();
        private Dictionary<string, AlertSender> _AlertSenderList = new Dictionary<string, AlertSender>();
        private List<StatusReporter> HeartBeatReporterList = new List<StatusReporter>();
        private CommandManager _CMDManager = null;

        private System.Timers.Timer _MonitorTimer;
        private int _ReceivedMsgCount = 0;
        private readonly string _HeartBeatStatusName = "Received_Report_Count";
        private ReportReceiver _Receiver;

        public static bool IsClosing = false;

        //----------Parameters------------
        public string MonitorCFGFile;
        public string ThisNodeName;
        /// <summary>
        /// receive and send status report with the same port (port at server side)
        /// </summary>
        public int Port = 8498;
        public string[] HeartBeatDestination;
        public string CommandDestination;
        public int CommandPort;
        public static int IOIntervalToFlushInSec = 0;
        public bool MonitorEventAndOsciRcd = false;
        public string EventOsci_Server = "powerit2.eecs.utk.edu";
        public string EventOsci_DBUser = "AppUser";
        public string EventOsci_DBPwd = "FnetUTK";
        public string EventOsci_DBName = "fnetserver";
        private DateTime NextTestEventOsciTime = DateTime.MinValue;
        private bool EnableDailyMsg = false;

        private DateTime NextDailyMessageTime = DateTime.Now.AddMinutes(-1);
        public string[] DailyMsgEmails;
        private YWPLogTypeCFG ReceivedRptLogCFG;
        private YWPLogCategory Log_Archive = new YWPLogCategory(true);
        public TreeView UITree;
        public TextBox MessageBox;
        public ComboBox NodeDropdownList;
        public Dictionary<string, Node> NodeList = new Dictionary<string, Node>();
        private Dictionary<string, YWPLogTypeCFG> m_LogCFGs = new Dictionary<string, YWPLogTypeCFG>();
        /// <summary>
        /// True: if there is no monitor correspond to a received report, a monitor will be created automatically.
        /// </summary>
        public bool AutoCreateMonitor = true;
        public Form1 GUI;
        public FNetMonitorManager(Form1 gui, string thisNodeName, string monitorCFGFile, string heartBeatDest, int port,string cmdDest,int cmdPort, int ioFlushInterval, TreeView uitree,TextBox messageBox, ComboBox nodeComboBox)
        {
            GUI = gui;
            ThisManager = this;
            LoadPara();
            ThisNodeName = thisNodeName;
            MonitorCFGFile = monitorCFGFile;
            Port = port;
            
            if (heartBeatDest!=null)
                HeartBeatDestination = heartBeatDest.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            IOIntervalToFlushInSec = ioFlushInterval;
            UITree = uitree;
            MessageBox = messageBox;
            NodeDropdownList = nodeComboBox;
            if (HeartBeatDestination != null)
                foreach (var dest in HeartBeatDestination)
                    HeartBeatReporterList.Add(new StatusReporter(dest, Port));
            CommandDestination = cmdDest;
            CommandPort = cmdPort;
            if (CommandDestination != null && CommandDestination.Length > 7)
                _CMDManager = new CommandManager(CommandDestination, CommandPort);
            InitialMonitors();  //not for Damping control GUI
            InitialTimer();
            InitialRptReceiver();
        }
        private void LoadPara()
        {
            try { MonitorEventAndOsciRcd = Convert.ToBoolean(ConfigurationManager.AppSettings["MonitorEventAndOsciRcd"]); } catch { }
            try { EventOsci_Server = ConfigurationManager.AppSettings["EventOsci_Server"]; } catch { }
            try { EventOsci_DBUser = ConfigurationManager.AppSettings["EventOsci_DBUser"]; } catch { }
            try { EventOsci_DBPwd = ConfigurationManager.AppSettings["EventOsci_DBPwd"]; } catch { }
            try { EventOsci_DBName = ConfigurationManager.AppSettings["EventOsci_DBName"]; } catch { }
            try { EnableDailyMsg = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableDailyMsg"]); } catch { }
            try { DailyMsgEmails = ConfigurationManager.AppSettings["DailyMsgEmails"].Trim().Split(';'); } catch { }
        }
        private void InitialMonitors()
        {
            ReceivedRptLogCFG = new YWPLogTypeCFG("ReceivedReport", "", true, true, YWPLogTypeCFG.ExpireType.Monthly, IOIntervalToFlushInSec, YWPLogTypeCFG.FileType.csv);

            MonitorCFGList = MonitorCFG.LoadMonitorCFG(MonitorCFGFile);
            if (MonitorCFGList == null) return;
            foreach (var cfg in MonitorCFGList)
            {
                FNetMonitor monitor = new FNetMonitor(cfg);
                string key = GetStatusKey(cfg.NodeName, cfg.StatusName);
                if (!_MonitorList.ContainsKey(key))
                {
                    _MonitorList.Add(key, monitor);
                    foreach (var email in monitor.Emails)
                        if (!_AlertSenderList.ContainsKey(email.Trim()))
                            _AlertSenderList.Add(email.Trim(), new AlertSender(email.Trim()));
                    if (!NodeList.ContainsKey(monitor.NodeName))
                    {
                        Node oneN = new Node(monitor.NodeName);
                        NodeList.Add(monitor.NodeName, oneN);
                    }
                    NodeList[monitor.NodeName].AddMonitor(monitor);
                }
            }
            foreach (var v in NodeList)
                UITree.Nodes.Add(v.Value.ThisUINode);
            UITree.Sort();
        }
        private void InitialTimer()
        {
            _MonitorTimer = new System.Timers.Timer();
            _MonitorTimer.AutoReset = true;
            _MonitorTimer.Interval = 30000;
            _MonitorTimer.Elapsed += _MonitorTimer_Elapsed;
            _MonitorTimer.Start();
        }
        private void InitialRptReceiver()
        {
            _Receiver = new ReportReceiver(Port, this);
        }
        private void _MonitorTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            CheckNoRptAndSendMsg();
            SendHeartBeat();
            try
            {
                //UpdateEventCountFromDB(EventOsci_Server, EventOsci_DBUser, EventOsci_DBPwd, EventOsci_DBName);
                //ReportDailyMessage();
            }
            catch { }
        }
        delegate void ShowMsg(string msg);
        private void DisplayMessage(string msg)
        {
            if (MessageBox.InvokeRequired)
            {
                MessageBox.Invoke(new ShowMsg(DisplayMessage), new object[] { msg });
                return;
            }
            if (msg.StartsWith("ALERT:"))
                GUI.DisplayAlert(msg);
            else
            {
                if (MessageBox.Text.Length > 30000)
                    MessageBox.Text = MessageBox.Text.Substring(0, 15000);
                MessageBox.Text = DateTime.Now.ToString("HH:mm:ss.fff") + " |\t" + msg + "\r\n" + MessageBox.Text;
            }
        }
        
        public static void DisplayMsg(string msg)
        {
            ThisManager.DisplayMessage(msg);
        }
        private void CheckNoRptAndSendMsg()
        {
            foreach (var m in _MonitorList)
            {
                string msg = m.Value.CheckNoReport();
                if (msg.Trim().Length < 10)
                    continue;
                foreach (var email in m.Value.Emails)
                {
                    if (!_AlertSenderList.ContainsKey(email.Trim()))
                        _AlertSenderList.Add(email.Trim(), new AlertSender(email.Trim()));
                    _AlertSenderList[email.Trim()].AppendMessage(msg);
                }
            }
            foreach (var s in _AlertSenderList)
                s.Value.CheckSendMessage();
        }
        private void SendHeartBeat()
        {
            foreach (var sender in HeartBeatReporterList)
                sender.ReportStatus(ThisNodeName, _HeartBeatStatusName, _ReceivedMsgCount);
            _ReceivedMsgCount = 0;
        }
        public void SendRelayStatusCommand(bool EnableWADC, bool EnablePSS, string WADCName)
        {
            if (_CMDManager == null) return;
            _CMDManager.SendRelayCommand(EnableWADC, EnablePSS, WADCName);
        }
        public void SendEnableWADCCmd(bool EnableWADC, string WADCName)
        {
            if (_CMDManager == null) return;
            _CMDManager.SendEnableWADCCommand(EnableWADC, WADCName);
        }
        public static string GetStatusKey(string nodeName, string statusName)
        {
            return nodeName + "-" + statusName;
        }


        public void ReportStatusValue(string nodeName, string statusName, double statusValue, DateTime timestamp)
        {
            string logStr = string.Format("{0},{1},{2},{3}", nodeName, statusName, timestamp.ToString("yyyy-MM-dd HH:mm:ss.fff"), statusValue.ToString());
            string key = GetStatusKey(nodeName, statusName);
            if (!_MonitorList.ContainsKey(key))  //Can't find corresponding monitor, may need to report exception?
            {
                if (!AutoCreateMonitor)
                {
                    logStr += ",Can't find corresponding monitor.";
                    //ArchiveReport(logStr);
                    ArchiveReport(logStr, key);
                    return;
                }
                else
                {
                    FNetMonitor m = new FNetMonitor(new MonitorCFG(nodeName, statusName), false);
                    _MonitorList.Add(key, m);
                    AddMonitor(m);
                    //foreach (var email in monitor.Emails)
                    //    if (!_AlertSenderList.ContainsKey(email.Trim()))
                    //        _AlertSenderList.Add(email.Trim(), new AlertSender(email.Trim()));
                    //if (!NodeList.ContainsKey(m.NodeName))
                    //{
                    //    Node oneN = new Node(m.NodeName);
                    //    NodeList.Add(m.NodeName, oneN);
                    //    AddMonitor(oneN);
                    //    //UITree.Nodes.Add(oneN.ThisUINode);
                    //    //if(UITree.InvokeRequired)
                    //    //    UITree.Invoke()
                    //}
                    //NodeList[m.NodeName].AddMonitor(m);
                }                
            }
            //ArchiveReport(logStr);
            ArchiveReport(logStr, key);
            _ReceivedMsgCount++;
            FNetMonitor monitor = _MonitorList[key];
            string msg = monitor.ReportStatusValue(statusValue, timestamp);
            if (msg.Trim().Length < 10)
                return;
            foreach (var email in monitor.Emails)
            {
                if (!_AlertSenderList.ContainsKey(email.Trim()))
                    _AlertSenderList.Add(email.Trim(), new AlertSender(email.Trim()));
                _AlertSenderList[email.Trim()].AppendMessage(msg);
            }
        }
        private void AddMonitor(FNetMonitor oneM)
        {
            if (UITree.InvokeRequired)
                UITree.Invoke(new AddM(AddMonitor), new object[] { oneM });
            else
            {
                if (!NodeList.ContainsKey(oneM.NodeName))
                {
                    Node oneN = new Node(oneM.NodeName);
                    NodeList.Add(oneM.NodeName, oneN);
                    UITree.Nodes.Add(oneN.ThisUINode);
                }
                NodeList[oneM.NodeName].AddMonitor(oneM);
                UITree.Sort();
            }

        }
        delegate void AddM(FNetMonitor oneN);
        private void ArchiveReport(string msg)
        {
            YWPLog.WriteDefaultLog(Log_Archive, msg, ReceivedRptLogCFG);
        }
        private void ArchiveReport(string msg, string key)
        {
            if(!m_LogCFGs.ContainsKey(key))
            {
                m_LogCFGs.Add(key, new YWPLogTypeCFG(key, "", true, true, YWPLogTypeCFG.ExpireType.Daily, IOIntervalToFlushInSec, YWPLogTypeCFG.FileType.csv));
            }
            YWPLog.WriteDefaultLog(Log_Archive, msg, m_LogCFGs[key]);

        }
        public void ReportStatusValue(string RptJson)
        {
            try
            {
                Status s = Status.ReadFromJson(RptJson);
                //YWPLog.WriteDefaultLog(Log_Archive, string.Format("Received:{0}\r\nParsed: time={1}", RptJson, s.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                if (s != null)
                {
                    if(!NodeNameDic.ContainsKey(s.NodeName))
                    {
                        NodeNameDic.Add(s.NodeName, 0);
                        AddNodeNameToDropDownList(s.NodeName);
                    }
                    if (s.StatusName == "EnableWADC" || s.StatusName == "Relay")
                        UpdateWADCStatus(s.NodeName, s.StatusName, s.StatusValue, s.TimeStamp);
                    else
                        ReportStatusValue(s.NodeName, s.StatusName, s.StatusValue, s.TimeStamp);
                }
                else
                {
                    DisplayMessage(RptJson);
                }
            }
            catch(Exception e)
            {
            }
        }
        public void UpdateWADCStatus(string nodeName, string statusName, double statusValue, DateTime timestamp)
        {
            if (statusName != "EnableWADC" && statusName!="Relay") return;
            if (!NodeList.ContainsKey(nodeName)) return;
            Node n = NodeList[nodeName];
            n.UpdateNode(statusName, statusValue);
            
        }
        delegate void UpdateN(Node n);
        
        delegate void AddNode(string nodeName);
        private void AddNodeNameToDropDownList(string NodeName)
        {
            if (NodeDropdownList == null) return;
            if(NodeDropdownList.InvokeRequired)
            {
                NodeDropdownList.Invoke(new AddNode(AddNodeNameToDropDownList), NodeName);
                return;
            }
            NodeDropdownList.Items.Add(NodeName);
        }
       

        //private void UpdateEventCountFromDB(string addresss, string username, string password, string database, string port = "3306")
        //{
        //    if (!MonitorEventAndOsciRcd) return;
        //    if (DateTime.Now.CompareTo(NextTestEventOsciTime) < 0)
        //        return;
        //    string connectionString = "server=" + addresss + ";uid=" + username + ";pwd=" + password + ";port=" + port + ";database=" + database + ";";
        //    MySqlConnection conn = new MySqlConnection(connectionString);
        //    try { conn.Open(); }
        //    catch (MySqlException ex)
        //    {
        //        YWPLog.WriteDefaultLog(YWPLogCategory.Type_Server_Exception, "Failed to connect to MySQL database for FDR configuration.", ex, YWPLog.DefaultExceptionLog);
        //        return;
        //    }
        //    MySqlCommand cmd = conn.CreateCommand();
        //    var now = DateTime.UtcNow.AddHours(-2);
        //    cmd.CommandText = string.Format("select location, count(*) COU from fnetevent where date>='{0}' and time >='{1}' group by location", now.ToString("yyyy-MM-dd"), now.ToString("HH:mm:ss"));
        //    MySqlDataReader result = cmd.ExecuteReader();
        //    if (result != null)
        //    {
        //        while (result.Read())
        //        {
        //            string nodeName = "database@" + addresss.ToLower();
        //            string statusName = "EventRecord_" + result["location"].ToString();
        //            double value = Convert.ToInt32(result["COU"]);
        //            ReportStatusValue(nodeName, statusName, value, DateTime.UtcNow);
        //        }
        //    }
        //    result.Dispose();
        //    cmd.CommandText= string.Format("select location, count(*) COU from fnetosci where date>'{0}' or date='{0}' and time >='{1}' group by location", now.ToString("yyyy-MM-dd"), now.ToString("HH:mm:ss"));
        //    result = cmd.ExecuteReader();
        //    if (result != null)
        //    {
        //        while (result.Read())
        //        {
        //            string nodeName = "database@" + addresss.ToLower();
        //            string statusName = "OsciRecord_" + result["location"].ToString();
        //            double value = Convert.ToInt32(result["COU"]);
        //            ReportStatusValue(nodeName, statusName, value, DateTime.UtcNow);
        //        }
        //    }
        //    conn.Close();
        //    NextTestEventOsciTime = DateTime.Now.AddHours(1);
        //}

        //private void ReportDailyMessage()
        //{
        //    if (!EnableDailyMsg || DateTime.Now.CompareTo(NextDailyMessageTime) < 0)
        //        return;
        //    if (DateTime.Now.Hour != 12)
        //        return;
        //    int EventCount = GetDailyEventCount(EventOsci_Server, EventOsci_DBUser, EventOsci_DBPwd, EventOsci_DBName);
        //    string sub = "Daily Message From " + ThisNodeName.ToUpper();
        //    string msg = "Monitor is running and alert emails can be delivered.";
        //    msg += string.Format("\r\n*** {0} event records in last 24 hours are found in the database. ***", EventCount.ToString());
        //    foreach(var v in DailyMsgEmails)
        //    {
        //        AlertSender.Sender.Send(sub, msg, v);
        //    }
        //    NextDailyMessageTime = NextDailyMessageTime.AddHours(23);
        //}
        //private int GetDailyEventCount(string addresss, string username, string password, string database, string port = "3306")
        //{
        //    string connectionString = "server=" + addresss + ";uid=" + username + ";pwd=" + password + ";port=" + port + ";database=" + database + ";";
        //    using (MySqlConnection conn = new MySqlConnection(connectionString))
        //    {
        //        try { conn.Open(); }
        //        catch (MySqlException ex)
        //        {
        //            YWPLog.WriteDefaultLog(YWPLogCategory.Type_Server_Exception, "Failed to connect to MySQL database for FDR configuration.", ex, YWPLog.DefaultExceptionLog);
        //            return 0;
        //        }
        //        MySqlCommand cmd = conn.CreateCommand();
        //        var now = DateTime.UtcNow.AddHours(-24);
        //        cmd.CommandText = string.Format("select count(*) COU from fnetevent where date>'{0}' or date='{0}' and time >='{1}'", now.ToString("yyyy-MM-dd"), now.ToString("HH:mm:ss"));
        //        MySqlDataReader result = cmd.ExecuteReader();
        //        if (result != null)
        //            while (result.Read())
        //                return Convert.ToInt32(result["COU"]);
        //        return 0;
        //    }
        //}

    }
}
