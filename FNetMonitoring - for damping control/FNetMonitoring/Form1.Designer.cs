﻿namespace FNetMonitoring
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.MonitorTreeView = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.MonitorPlot = new FNetMonitoring.PlotMultiSeries();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cb_WADCName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_EnableWADC = new System.Windows.Forms.CheckBox();
            this.cb_EnablePSS = new System.Windows.Forms.CheckBox();
            this.bt_SendStatusCMD = new System.Windows.Forms.Button();
            this.messageBox = new System.Windows.Forms.TextBox();
            this.lb_Notification = new System.Windows.Forms.Label();
            this.MonitorMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.temporarilyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableWADCButton = new System.Windows.Forms.ToolStripMenuItem();
            this.TreeImgList = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.MonitorMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.MonitorTreeView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel2);
            this.splitContainer1.Size = new System.Drawing.Size(830, 591);
            this.splitContainer1.SplitterDistance = 176;
            this.splitContainer1.TabIndex = 0;
            // 
            // MonitorTreeView
            // 
            this.MonitorTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MonitorTreeView.ImageIndex = 0;
            this.MonitorTreeView.ImageList = this.TreeImgList;
            this.MonitorTreeView.Location = new System.Drawing.Point(0, 0);
            this.MonitorTreeView.Name = "MonitorTreeView";
            this.MonitorTreeView.SelectedImageIndex = 0;
            this.MonitorTreeView.Size = new System.Drawing.Size(176, 591);
            this.MonitorTreeView.TabIndex = 0;
            this.MonitorTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.MonitorTreeView_AfterSelect);
            this.MonitorTreeView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MonitorTreeView_MouseClick);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.splitContainer2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lb_Notification, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(650, 591);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.messageBox);
            this.splitContainer2.Size = new System.Drawing.Size(644, 565);
            this.splitContainer2.SplitterDistance = 458;
            this.splitContainer2.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.MonitorPlot, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(644, 458);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // MonitorPlot
            // 
            this.MonitorPlot.ChartTitle = "My Chart";
            this.MonitorPlot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MonitorPlot.Location = new System.Drawing.Point(3, 73);
            this.MonitorPlot.Name = "MonitorPlot";
            this.MonitorPlot.Size = new System.Drawing.Size(638, 382);
            this.MonitorPlot.TabIndex = 0;
            this.MonitorPlot.XLabelStyle = "HH:mm:ss";
            this.MonitorPlot.YLabelStyle = "F2";
            this.MonitorPlot.YTitle = "MyYName";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(638, 64);
            this.panel1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cb_WADCName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cb_EnableWADC);
            this.groupBox1.Controls.Add(this.cb_EnablePSS);
            this.groupBox1.Controls.Add(this.bt_SendStatusCMD);
            this.groupBox1.Location = new System.Drawing.Point(3, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(621, 49);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Send Status Command to WADC";
            // 
            // cb_WADCName
            // 
            this.cb_WADCName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_WADCName.FormattingEnabled = true;
            this.cb_WADCName.Location = new System.Drawing.Point(86, 19);
            this.cb_WADCName.Name = "cb_WADCName";
            this.cb_WADCName.Size = new System.Drawing.Size(137, 21);
            this.cb_WADCName.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "WADC Name:";
            // 
            // cb_EnableWADC
            // 
            this.cb_EnableWADC.AutoSize = true;
            this.cb_EnableWADC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_EnableWADC.Location = new System.Drawing.Point(229, 20);
            this.cb_EnableWADC.Name = "cb_EnableWADC";
            this.cb_EnableWADC.Size = new System.Drawing.Size(103, 19);
            this.cb_EnableWADC.TabIndex = 0;
            this.cb_EnableWADC.Text = "Enable WADC";
            this.cb_EnableWADC.UseVisualStyleBackColor = true;
            // 
            // cb_EnablePSS
            // 
            this.cb_EnablePSS.AutoSize = true;
            this.cb_EnablePSS.Checked = true;
            this.cb_EnablePSS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_EnablePSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_EnablePSS.Location = new System.Drawing.Point(338, 20);
            this.cb_EnablePSS.Name = "cb_EnablePSS";
            this.cb_EnablePSS.Size = new System.Drawing.Size(92, 19);
            this.cb_EnablePSS.TabIndex = 1;
            this.cb_EnablePSS.Text = "Enable PSS";
            this.cb_EnablePSS.UseVisualStyleBackColor = true;
            // 
            // bt_SendStatusCMD
            // 
            this.bt_SendStatusCMD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_SendStatusCMD.Location = new System.Drawing.Point(436, 18);
            this.bt_SendStatusCMD.Name = "bt_SendStatusCMD";
            this.bt_SendStatusCMD.Size = new System.Drawing.Size(176, 23);
            this.bt_SendStatusCMD.TabIndex = 2;
            this.bt_SendStatusCMD.Text = "Send Status Command";
            this.bt_SendStatusCMD.UseVisualStyleBackColor = true;
            this.bt_SendStatusCMD.Click += new System.EventHandler(this.Bt_SendStatusCMD_Click);
            // 
            // messageBox
            // 
            this.messageBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messageBox.Location = new System.Drawing.Point(0, 0);
            this.messageBox.Multiline = true;
            this.messageBox.Name = "messageBox";
            this.messageBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.messageBox.Size = new System.Drawing.Size(644, 103);
            this.messageBox.TabIndex = 0;
            // 
            // lb_Notification
            // 
            this.lb_Notification.AutoSize = true;
            this.lb_Notification.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Notification.Location = new System.Drawing.Point(3, 571);
            this.lb_Notification.Name = "lb_Notification";
            this.lb_Notification.Size = new System.Drawing.Size(78, 16);
            this.lb_Notification.TabIndex = 2;
            this.lb_Notification.Text = "(notification)";
            // 
            // MonitorMenuStrip
            // 
            this.MonitorMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.temporarilyToolStripMenuItem,
            this.enableWADCButton});
            this.MonitorMenuStrip.Name = "MonitorMenuStrip";
            this.MonitorMenuStrip.Size = new System.Drawing.Size(178, 48);
            // 
            // temporarilyToolStripMenuItem
            // 
            this.temporarilyToolStripMenuItem.Name = "temporarilyToolStripMenuItem";
            this.temporarilyToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.temporarilyToolStripMenuItem.Text = "Temporarily Disable";
            this.temporarilyToolStripMenuItem.Click += new System.EventHandler(this.TemporarilyToolStripMenuItem_Click);
            // 
            // enableWADCButton
            // 
            this.enableWADCButton.Name = "enableWADCButton";
            this.enableWADCButton.Size = new System.Drawing.Size(177, 22);
            this.enableWADCButton.Text = "Disable WADC";
            this.enableWADCButton.Click += new System.EventHandler(this.enableWADCButton_Click);
            // 
            // TreeImgList
            // 
            this.TreeImgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("TreeImgList.ImageStream")));
            this.TreeImgList.TransparentColor = System.Drawing.Color.Transparent;
            this.TreeImgList.Images.SetKeyName(0, "chart.png");
            this.TreeImgList.Images.SetKeyName(1, "00.png");
            this.TreeImgList.Images.SetKeyName(2, "01.png");
            this.TreeImgList.Images.SetKeyName(3, "10.png");
            this.TreeImgList.Images.SetKeyName(4, "11.png");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 591);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Monitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.MonitorMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView MonitorTreeView;
        private PlotMultiSeries MonitorPlot;
        private System.Windows.Forms.ContextMenuStrip MonitorMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem temporarilyToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox messageBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bt_SendStatusCMD;
        private System.Windows.Forms.CheckBox cb_EnablePSS;
        private System.Windows.Forms.CheckBox cb_EnableWADC;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lb_Notification;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cb_WADCName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem enableWADCButton;
        private System.Windows.Forms.ImageList TreeImgList;
    }
}

