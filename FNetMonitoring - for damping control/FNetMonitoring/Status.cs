﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;

namespace FNetMonitoring
{
    [DataContract]
    public class Status
    {
        /// <summary>
        /// server name or ip or domain name, which identifies where the status comes from. 
        /// </summary>
        [DataMember]
        public string NodeName;
        [DataMember]
        public string StatusName;
        [DataMember]
        public double StatusValue = 0;
        [DataMember]
        public string Timestamp = "";

        public DateTime TimeStamp;
        //{
        //    get
        //    {
        //        //return DateTime.Now;
        //        try { return DateTime.Parse(Timestamp); }
        //        catch {
        //            return DateTime.Now;
        //        }
        //    }
        //}

        private static DataContractJsonSerializer _Serializer = new DataContractJsonSerializer(typeof(Status));

        public Status()
        {
        }
        public Status(string nodeName, string statusName, double value)
        {
            NodeName = nodeName;
            StatusName = statusName;
            StatusValue = value;
        }
        public static string GetJsonString(Status status)
        {
            if (status == null)
                return "";
            using (MemoryStream ms = new MemoryStream())
            {
                _Serializer.WriteObject(ms, status);
                return Encoding.Default.GetString(ms.ToArray());
            }
        }
        public static Status ReadFromJson(string jsonString)
        {
            try
            {
                var v= (Status)_Serializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(jsonString)));
                v.TimeStamp = DateTime.Now;
                if(v.Timestamp!=null&&v.Timestamp.Length>10)
                {
                    try { v.TimeStamp = DateTime.Parse(v.Timestamp); } catch { }
                }
                return v;
            }
            catch(Exception e) { return null; }
        }
        public string JsonString
        {
            get { return GetJsonString(this); }
        }
    }
}
