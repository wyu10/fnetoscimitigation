﻿/*******************************************************************************************
 *  *
 * Class to log message to different files.
 * 
 * Developed by Wenpeng (Wayne) Yu in 2017/5  
********************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FNetMonitoring
{    
    public class YWPLog
    {
        public static bool StopLogging = false;
        public static YWPLogTypeCFG DefaultExceptionLog = new YWPLogTypeCFG("ExceptionLog", "", true, true, YWPLogTypeCFG.ExpireType.Monthly, FNetMonitorManager.IOIntervalToFlushInSec);
        public static YWPLogTypeCFG DefaultLogCfg = new YWPLogTypeCFG("Log", "", true, true, YWPLogTypeCFG.ExpireType.Monthly, FNetMonitorManager.IOIntervalToFlushInSec);

        private readonly string RootFolder = "";
        private static Dictionary<string, MyStreamWriter> AllWriters = new Dictionary<string, MyStreamWriter>();
        private static YWPLog DefaultLoger = new YWPLog("Log\\", new YWPLogTypeCFG("LogDefault", "", true, true, YWPLogTypeCFG.ExpireType.Monthly, FNetMonitorManager.IOIntervalToFlushInSec));
        private string GetKey(YWPLogTypeCFG logCFG)
        {
            if (logCFG == null) return "";
            return RootFolder.ToUpper() + logCFG.GetKey();
        }
        /// <summary>
        /// Specify the root folder of all log files. 
        /// If 'defaultLogCfg' is null, default log file will be %RootFolder%\Log\Log[yyyy-MM].txt
        /// </summary>
        public YWPLog(string rootFolder, YWPLogTypeCFG defaultLogCfg = null)
        {
            RootFolder = rootFolder;
            if (RootFolder != null && RootFolder.Length > 0 && !Directory.Exists(RootFolder))
                Directory.CreateDirectory(RootFolder);
            if (defaultLogCfg != null)
                DefaultLogCfg = defaultLogCfg;
        }
        public void WriteLog(YWPLogCategory thisLogType, string LogMsg, YWPLogTypeCFG logCFG = null)
        {
            if(StopLogging)
            {
                if(AllWriters.Count>0)
                {
                    CloseAllWritter();
                    AllWriters.Clear();
                }
                return;
            }
            if (!thisLogType.EnableLog)
                return;
            if (logCFG == null) logCFG = DefaultLogCfg;
            string key = GetKey(logCFG);
            lock (AllWriters)
                if (!AllWriters.ContainsKey(key))
                    AllWriters.Add(key, new MyStreamWriter(RootFolder, logCFG));
            lock (AllWriters[key])
                AllWriters[key].Log(LogMsg);
        }
        public void WriteLog(YWPLogCategory thisLogLevel, Exception ex, YWPLogTypeCFG logCFG = null)
        {
            if (ex == null) return;
            string msg = "Exception. Message:" + ex.Message + "\r\n" + ex.StackTrace;
            WriteLog(thisLogLevel, msg, logCFG);
        }
        public void WriteLog(YWPLogCategory thisLogLevel, string msgstr, Exception ex, YWPLogTypeCFG logCFG = null)
        {
            if (ex == null)
            {
                WriteLog(thisLogLevel, msgstr, logCFG);
                return;
            }
            string msg = msgstr + "\r\nMessage:" + ex.Message + "\r\n" + ex.StackTrace;
            WriteLog(thisLogLevel, msg, logCFG);
        }
        public static void WriteDefaultLog(YWPLogCategory thisLogLevel, string LogMsg, YWPLogTypeCFG logCFG = null, YWPLogTypeCFG SecondaryLogCFG = null)
        {
            DefaultLoger.WriteLog(thisLogLevel, LogMsg, logCFG);
            if(SecondaryLogCFG!=null)
                DefaultLoger.WriteLog(thisLogLevel, LogMsg, SecondaryLogCFG);
        }
        public static void WriteDefaultLog(YWPLogCategory thisLogLevel, Exception ex, YWPLogTypeCFG logCFG = null)
        {
            DefaultLoger.WriteLog(thisLogLevel, ex, logCFG);
        }
        public static void WriteDefaultLog(YWPLogCategory thisLogLevel, string msg, Exception ex, YWPLogTypeCFG logCFG = null)
        {
            DefaultLoger.WriteLog(thisLogLevel, msg, ex, logCFG);
        }
        public void CloseWritter(YWPLogTypeCFG logCFG)
        {
            string key = GetKey(logCFG);
            if (!AllWriters.ContainsKey(key)) return;
            lock (AllWriters[key]) AllWriters[key].CloseWriter();
        }
        public void CloseAllWritter()
        {
            lock (AllWriters)
                foreach (var v in AllWriters)
                    v.Value.CloseWriter();
        }
        
    }
    public class MyStreamWriter
    {

        private StreamWriter SW = null;
        private StringBuilder StringBuffer = new StringBuilder();
        private DateTime TimeToFlush;
        private string CurrentFilePath = "";
        private DateTime TimeInCurrentFileName;
        private DateTime ExpiredTime;
        private string RootFolder;
        private YWPLogTypeCFG ThisLogType;
        public delegate void FileHandler(string FilePath, DateTime TimeInFileName);
        private event FileHandler FileRenewed;
        public MyStreamWriter(string rootFolder, YWPLogTypeCFG logType, FileHandler FileRenewedHandler=null)
        {
            ThisLogType = logType;
            RootFolder = rootFolder;
            if (FileRenewedHandler != null)
                FileRenewed += FileRenewedHandler;
            TimeToFlush = DateTime.Now.AddSeconds(logType.IOIntervalToFlushInSec);
        }
        private DateTime GetCurrentTime(bool UTCTime = false)
        {
            return UTCTime ? DateTime.UtcNow : DateTime.Now;
        }
        public void CheckExpiration()
        {
            if (SW == null || ThisLogType.LogExpireType == YWPLogTypeCFG.ExpireType.NeverExpire)
                return;
            if (GetCurrentTime(ThisLogType.UseUTCTime).CompareTo(ExpiredTime) >= 0)
            {
                SW.Close();
                SW = null;
            }
        }
        public void Log(string msg)
        {
            if (SW == null) SW = GetNewSW();
            else if (ThisLogType.LogExpireType != YWPLogTypeCFG.ExpireType.NeverExpire && GetCurrentTime(ThisLogType.UseUTCTime).CompareTo(ExpiredTime) >= 0)
            {
                SW.Close();
                if (FileRenewed != null)
                {
                    try
                    {
                        FileRenewed(CurrentFilePath, TimeInCurrentFileName);
                    }
                    catch(Exception e)
                    {
                        //FNetServerManager.LogException(YWPLogCategory.Type_Server_Exception, 0, "Exception is caught in event handler when log file renewed.", e);
                    }
                }
                SW = GetNewSW();                
            }
            string StrToLog = msg;
            if (ThisLogType.LogLocalTime)
            {
                if(ThisLogType.LogFileType == YWPLogTypeCFG.FileType.txt)
                    StrToLog = DateTime.Now.ToString("[MM/dd/yyyy HH:mm:ss.fff][UTC ") + DateTime.UtcNow.ToString("HH") + "]\t" + msg;
                else
                    StrToLog = DateTime.Now.ToString(" MM/dd/yyyy HH:mm:ss.fff")+",UTC " + DateTime.UtcNow.ToString("HH") + "," + msg;
            }
            if (ThisLogType.IOIntervalToFlushInSec <= 0)
            {
                SW.WriteLine(StrToLog);
                SW.Flush();
            }
            else
            {
                StringBuffer.AppendLine(StrToLog);
                if(DateTime.Now.CompareTo(TimeToFlush)>=0)
                {
                    TimeToFlush = DateTime.Now.AddSeconds(ThisLogType.IOIntervalToFlushInSec);
                    SW.Write(StringBuffer.ToString());
                    StringBuffer.Clear();
                    SW.Flush();
                }
            }

        }
        private StreamWriter GetNewSW()
        {
            StreamWriter sw = null;
            string FileNameAndPath = GetFileNameAndSetExpireTime();
            try
            {
                bool FileExist = File.Exists(FileNameAndPath + FileExtension);
                sw = new StreamWriter(FileNameAndPath + FileExtension, ThisLogType.Append);
                if (!FileExist && ThisLogType.FirstLineContent.Length > 0)
                    sw.WriteLine(ThisLogType.FirstLineContent);
                CurrentFilePath = FileNameAndPath + FileExtension;
            }
            catch (Exception e)
            {
                Random ra = new Random(DateTime.Now.Millisecond);
                int tryCount = 0;
                while (sw == null && tryCount++ < 10000)
                {
                    try
                    { sw = new StreamWriter(FileNameAndPath + "_" + ra.Next(1000).ToString() + FileExtension, ThisLogType.Append); }
                    catch (Exception ee) { sw = null; }
                }
                if (sw != null)
                {
                    sw.WriteLine("**********************************************************");
                    sw.WriteLine("* File Access Confliction: " + FileNameAndPath + FileExtension);
                    sw.WriteLine("**********************************************************");
                    sw.Flush();
                }
            }
            return sw;
        }
        public void CloseWriter()
        {
            if (SW != null)
            {
                try { SW.Close(); SW = null; }
                catch (Exception e) { }
            }
        }
        /// <summary>
        /// File name extension 
        /// </summary>
        private string FileExtension
        {
            get { return "." + ThisLogType.LogFileType.ToString(); }
        }
        /// <summary>
        /// No extension 
        /// </summary>
        private string GetFileNameAndSetExpireTime()
        {
            string Path = RootFolder + ThisLogType.SubFolder;
            if (Path != null && Path.Length > 0 && !Directory.Exists(Path)) Directory.CreateDirectory(Path);
            //string FileName = Path + ThisLogType.TypeName;
            string Postfix = "";
            DateTime Now = GetCurrentTime(ThisLogType.UseUTCTime);
            DateTime TimeInFileName;
            switch (ThisLogType.LogExpireType)
            {
                case YWPLogTypeCFG.ExpireType.NeverExpire:
                    ExpiredTime = DateTime.MaxValue;
                    TimeInCurrentFileName = DateTime.MinValue;
                    break;
                case YWPLogTypeCFG.ExpireType.Yearly:
                    ExpiredTime = new DateTime(Now.Year + 1, 1, 1);
                    TimeInCurrentFileName = ExpiredTime.AddYears(-1);
                    Postfix= string.Format(" [{0}]", Now.Year);
                    break;
                case YWPLogTypeCFG.ExpireType.Monthly:
                    ExpiredTime = new DateTime(Now.AddMonths(1).Year, Now.AddMonths(1).Month, 1);
                    TimeInCurrentFileName = ExpiredTime.AddMonths(-1);
                    Postfix = string.Format(" [{0}]", Now.ToString("yyyy-MM"));
                    break;
                case YWPLogTypeCFG.ExpireType.Daily:
                    ExpiredTime = Now.AddDays(1).Date;
                    TimeInCurrentFileName = ExpiredTime.AddDays(-1);
                    Postfix = string.Format(" [{0}]", Now.ToString("yyyy-MM-dd"));
                    break;
                case YWPLogTypeCFG.ExpireType.Hourly:
                case YWPLogTypeCFG.ExpireType.Hour2:
                case YWPLogTypeCFG.ExpireType.Hour3:
                case YWPLogTypeCFG.ExpireType.Hour4:
                case YWPLogTypeCFG.ExpireType.Hour6:
                case YWPLogTypeCFG.ExpireType.Hour8:
                case YWPLogTypeCFG.ExpireType.Hour12:
                    TimeInFileName = Now.Date.AddHours((Now.Hour / (int)ThisLogType.LogExpireType) * (int)ThisLogType.LogExpireType);
                    ExpiredTime = TimeInFileName.AddHours((int)ThisLogType.LogExpireType);
                    TimeInCurrentFileName = ExpiredTime.AddHours(-1 * (int)ThisLogType.LogExpireType);
                    Postfix = string.Format(" [{0}]", TimeInFileName.ToString("yyyy-MM-dd HH"));
                    break;
                case YWPLogTypeCFG.ExpireType.Minutely:
                    ExpiredTime = Now.AddMinutes(1).Date.AddHours(Now.AddMinutes(1).Hour).AddMinutes(Now.AddMinutes(1).Minute);
                    TimeInCurrentFileName = ExpiredTime.AddMinutes(-1);
                    Postfix = string.Format(" [{0}]", Now.ToString("yyyy-MM-dd HH.mm"));
                    break;
                case YWPLogTypeCFG.ExpireType.Minute5:
                case YWPLogTypeCFG.ExpireType.Minute10:
                case YWPLogTypeCFG.ExpireType.Minute20:
                case YWPLogTypeCFG.ExpireType.Minute30:
                    int minInterval = (int)(ThisLogType.LogExpireType) - 1000;
                    TimeInFileName = Now.Date.AddHours(Now.Hour).AddMinutes((Now.Minute / minInterval) * minInterval);
                    ExpiredTime = TimeInFileName.AddMinutes(minInterval);
                    TimeInCurrentFileName = TimeInFileName;
                    Postfix = string.Format(" [{0}]", TimeInFileName.ToString("yyyy-MM-dd HH.mm"));
                    break;

            }
            if(ThisLogType.TimeAsSubFolder)
            {
                if (Postfix.Length > 0)
                    Path += Postfix.Trim().Replace("[","").Replace("]","") + "\\";
                if (Path != null && Path.Length > 0 && !Directory.Exists(Path)) Directory.CreateDirectory(Path);
                if (ThisLogType.TimeAsFileName)
                    return Path + ThisLogType.TypeName + Postfix;
                else
                    return Path + ThisLogType.TypeName;
            }
            else
            {
                return Path + ThisLogType.TypeName + Postfix;
            }
        }
    }
    public class YWPLogTypeCFG
    {
        public enum ExpireType : int
        {
            NeverExpire = 100,
            Yearly = 101,
            Monthly = 102,
            Daily = 103,
            Minutely = 1001,
            Minute5=1005,
            Minute10=1010,
            Minute20=1020,
            Minute30=1030,
            Hourly = 1,
            Hour2 = 2,
            Hour3 = 3,
            Hour4 = 4,
            Hour6 = 6,
            Hour8 = 8,
            Hour12 = 12
        }
        public enum FileType
        {
            txt,
            csv
        }

        public readonly string TypeName;
        public readonly string SubFolder;
        public readonly bool Append;
        public readonly ExpireType LogExpireType;
        public readonly bool LogLocalTime;
        public readonly FileType LogFileType = YWPLogTypeCFG.FileType.txt;
        public readonly bool UseUTCTime = false;
        public readonly bool TimeAsSubFolder = false;
        public readonly bool TimeAsFileName = false;
        public readonly string FirstLineContent = "";
        public readonly double IOIntervalToFlushInSec = 0;

        public YWPLogTypeCFG(string typeName, string subFolder, bool append, bool logLocalTime, ExpireType expireType, double intervalToFlushInSec, FileType logFileType= FileType.txt, bool UTCTime=false, bool timeAsSubFolder=false, string firstLineContent="",bool timeAsFileName=false)
        {
            if (typeName != null && typeName.Length > 0)
                TypeName = typeName;
            else TypeName = "Log_NoTypeName";
            SubFolder = subFolder;
            Append = append;
            LogLocalTime = logLocalTime;
            LogExpireType = expireType;
            LogFileType = logFileType;
            UseUTCTime = UTCTime;
            TimeAsSubFolder = timeAsSubFolder;
            TimeAsFileName = timeAsFileName;
            FirstLineContent = firstLineContent;
            IOIntervalToFlushInSec = intervalToFlushInSec;
        }
        public override string ToString() { return SubFolder.ToUpper() + TypeName.ToUpper(); }
        public string GetKey() { return SubFolder.ToUpper() + TypeName.ToUpper(); }
        //public enum SubfolderMethod : int
        //{
        //    NoSubfolder = 100,
        //    Yearly = 101,
        //    Monthly = 102,
        //    Daily = 103,
        //    Minutely = 104,
        //    TimeAsSubfolder = 105,
        //    Hourly = 1,
        //    Hour2 = 2,
        //    Hour3 = 3,
        //    Hour4 = 4,
        //    Hour6 = 6,
        //    Hour8 = 8,
        //    Hour12 = 12            
        //}
       
    }
    public class YWPLogCategory
    {
        public bool EnableLog = true;
        public YWPLogCategory(bool enableLog)
        {
            EnableLog = enableLog;
        }
        
        public static YWPLogCategory Type_MissingData_Detail = new YWPLogCategory(false);
        public static YWPLogCategory Type_MissingData_Normal = new YWPLogCategory(false);
        public static YWPLogCategory Type_MissingData_Important = new YWPLogCategory(false);

        public static YWPLogCategory Type_WrongData_Detail = new YWPLogCategory(false);
        public static YWPLogCategory Type_WrongData_Normal = new YWPLogCategory(false);
        public static YWPLogCategory Type_WrongData_Important = new YWPLogCategory(true);

        public static YWPLogCategory Type_Parser_Msg = new YWPLogCategory(false);
        public static YWPLogCategory Type_Parser_Exception = new YWPLogCategory(true);

        public static YWPLogCategory Type_Data_Calibration = new YWPLogCategory(true); //
        public static YWPLogCategory Type_Data_Full = new YWPLogCategory(true);    //
        public static YWPLogCategory Type_DataArchive = new YWPLogCategory(true);
        public static YWPLogCategory Type_DataArchive_Exception = new YWPLogCategory(true);

        public static YWPLogCategory Type_DataFwd_Exception = new YWPLogCategory(true);
        
        public static YWPLogCategory Type_Statistic_Normal = new YWPLogCategory(false);
        public static YWPLogCategory Type_Statistic_Important = new YWPLogCategory(true);
        public static YWPLogCategory Type_Statistic_Exception = new YWPLogCategory(true);

        public static YWPLogCategory Type_SpecifiedOutput = new YWPLogCategory(false);

        public static YWPLogCategory Type_Connection_Exception = new YWPLogCategory(true);
        public static YWPLogCategory Type_Connection_RegularRpt = new YWPLogCategory(true);
        public static YWPLogCategory Type_Connection_Msg = new YWPLogCategory(true);

        public static YWPLogCategory Type_ServerStatus_Msg = new YWPLogCategory(true);
        public static YWPLogCategory Type_ServerStatus_Exception = new YWPLogCategory(true);
    
        public static YWPLogCategory Type_Application_Msg = new YWPLogCategory(false);
        public static YWPLogCategory Type_Application_FullData = new YWPLogCategory(false);
        public static YWPLogCategory Type_Application_Exception = new YWPLogCategory(true);

        public static YWPLogCategory Type_Server_Exception = new YWPLogCategory(true);

        public static YWPLogCategory Type_DataSaver_Msg = new YWPLogCategory(false);
        public static YWPLogCategory Type_DataSaver_Exception = new YWPLogCategory(true);

        public static YWPLogCategory Type_CFG_Exception = new YWPLogCategory(true);
    }    
    public static class Extensions
    {
        public static DateTime GetCurrentTime(this YWPLogTypeCFG.ExpireType exprieType, bool UseUTCTime=false)
        {
            DateTime now = UseUTCTime ? DateTime.UtcNow : DateTime.Now;
            switch (exprieType)
            {                
                case YWPLogTypeCFG.ExpireType.NeverExpire:
                    return DateTime.MinValue;
                case YWPLogTypeCFG.ExpireType.Yearly:
                    return new DateTime(now.Year, 1, 1);                    
                case YWPLogTypeCFG.ExpireType.Monthly:
                    return new DateTime(now.Year, now.Month, 1);                    
                case YWPLogTypeCFG.ExpireType.Daily:
                    return now.Date;                    
                case YWPLogTypeCFG.ExpireType.Hourly:
                case YWPLogTypeCFG.ExpireType.Hour2:
                case YWPLogTypeCFG.ExpireType.Hour3:
                case YWPLogTypeCFG.ExpireType.Hour4:
                case YWPLogTypeCFG.ExpireType.Hour6:
                case YWPLogTypeCFG.ExpireType.Hour8:
                case YWPLogTypeCFG.ExpireType.Hour12:
                    return now.Date.AddHours((now.Hour / (int)exprieType) * (int)exprieType);
                case YWPLogTypeCFG.ExpireType.Minutely:
                    return new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0);
            }
            return DateTime.MinValue;
        }
        public static DateTime GetPreviousTime(this YWPLogTypeCFG.ExpireType exprieType, bool UseUTCTime = false)
        {
            DateTime now = UseUTCTime ? DateTime.UtcNow : DateTime.Now;
            switch (exprieType)
            {
                case YWPLogTypeCFG.ExpireType.NeverExpire:
                    return DateTime.MinValue;
                case YWPLogTypeCFG.ExpireType.Yearly:
                    return new DateTime(now.Year, 1, 1).AddYears(-1);
                case YWPLogTypeCFG.ExpireType.Monthly:
                    return new DateTime(now.Year, now.Month, 1).AddMonths(-1);
                case YWPLogTypeCFG.ExpireType.Daily:
                    return now.Date.AddDays(-1);
                case YWPLogTypeCFG.ExpireType.Hourly:
                case YWPLogTypeCFG.ExpireType.Hour2:
                case YWPLogTypeCFG.ExpireType.Hour3:
                case YWPLogTypeCFG.ExpireType.Hour4:
                case YWPLogTypeCFG.ExpireType.Hour6:
                case YWPLogTypeCFG.ExpireType.Hour8:
                case YWPLogTypeCFG.ExpireType.Hour12:
                    return now.Date.AddHours((now.Hour / (int)exprieType) * (int)exprieType).AddHours(-1 * (int)exprieType);
                case YWPLogTypeCFG.ExpireType.Minutely:
                    return new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0).AddMinutes(-1);
            }
            return DateTime.MinValue;
        }
        public static DateTime GetNextTime(this YWPLogTypeCFG.ExpireType exprieType, bool UseUTCTime = false)
        {
            DateTime now = UseUTCTime ? DateTime.UtcNow : DateTime.Now;
            switch (exprieType)
            {
                case YWPLogTypeCFG.ExpireType.NeverExpire:
                    return DateTime.MaxValue;
                case YWPLogTypeCFG.ExpireType.Yearly:
                    return new DateTime(now.Year, 1, 1).AddYears(1);
                case YWPLogTypeCFG.ExpireType.Monthly:
                    return new DateTime(now.Year, now.Month, 1).AddMonths(1);
                case YWPLogTypeCFG.ExpireType.Daily:
                    return now.Date.AddDays(1);
                case YWPLogTypeCFG.ExpireType.Hourly:
                case YWPLogTypeCFG.ExpireType.Hour2:
                case YWPLogTypeCFG.ExpireType.Hour3:
                case YWPLogTypeCFG.ExpireType.Hour4:
                case YWPLogTypeCFG.ExpireType.Hour6:
                case YWPLogTypeCFG.ExpireType.Hour8:
                case YWPLogTypeCFG.ExpireType.Hour12:
                    return now.Date.AddHours((now.Hour / (int)exprieType) * (int)exprieType).AddHours((int)exprieType);
                case YWPLogTypeCFG.ExpireType.Minutely:
                    return new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0).AddMinutes(1);
            }
            return DateTime.MaxValue;
        }


    }
}

