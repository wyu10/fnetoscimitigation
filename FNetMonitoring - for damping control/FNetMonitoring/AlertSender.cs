﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

namespace FNetMonitoring
{
    public class AlertSender
    {
        public string ReceiverEmail;
        private StringBuilder ReportContent = new StringBuilder();
        private DateTime _NextMsgTime = DateTime.Now;
        private int _NextMsgIntervalInSec = 120;
        public static Alerter Sender = null;
        public AlertSender(string email)
        {
            ReceiverEmail = email;
            if (Sender == null)
                Sender = new Alerter("smtp.gmail.com", 587, "fnet.monitor@gmail.com", "FnetPWD");
                

        }
        public void AppendMessage(string msg)
        {
            if (msg.Length > 5)
                ReportContent.Append(msg);
        }
        public void CheckSendMessage()
        {
            if (ReportContent.Length == 0) return;
            if (_NextMsgTime.CompareTo(DateTime.Now) < 0)
            {
                //refresh report time
                FNetMonitor.RefreshNextMsgTimeAndInterval(ref _NextMsgTime, ref _NextMsgIntervalInSec);
                //Send message
                if (Sender.Send("Attention is needed for abnormal status", "** Alert From " + FNetMonitorManager.ThisManager.ThisNodeName + " **\r\n" + ReportContent.ToString(), ReceiverEmail))
                    ReportContent.Clear();
            }
        }


        public class Alerter
        {
            private string fromHost = "smtp.gmail.com";
            private int fromPort = 587;
            private MailAddress fromAddress;
            private string fromPassword;
            private SmtpClient smtpClient;
            public Alerter(string FromHost, int FromPort, string FromAccount, string Password)
            {
                fromHost = FromHost;
                fromPort = FromPort;
                fromAddress = new MailAddress(FromAccount);
                fromPassword = Password;
                smtpClient = new SmtpClient();
                smtpClient.Host = fromHost;
                smtpClient.Port = fromPort;
                smtpClient.EnableSsl = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(fromAddress.Address, fromPassword);
            }

            public bool Send(string subject, string body, string toAddress)
            {
                if (toAddress.Length < 3 || toAddress.IndexOf('@') < 0)
                    return false;
                MailMessage message = new MailMessage(fromAddress, new MailAddress(toAddress));
                message.Subject = subject;
                message.Body = body;
                try
                {
                    SmtpClient client = new SmtpClient();
                    client.Host = fromHost;
                    client.Port = fromPort;
                    client.EnableSsl = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(fromAddress.Address, fromPassword);
                    client.Send(message);
                    return true;
                }
                catch (Exception ex) when (ex is SmtpException || ex is SmtpFailedRecipientsException)
                {


                }
                return false;
            }
        }
    }
}
