﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Threading;

namespace FNetMonitoring
{
    public partial class Form1 : Form
    {
        private FNetMonitorManager _MonitorManager;
        public string MonitorCFGFile = "MonitorCFG.txt";
        public string HeartBeatDestination="";
        public string CommandDestination = "";
        public int Port = 8498;
        public int CommandPort = 8497;
        public string ThisNodeName;
        public int IOIntervalToFlushInSec = 0;
        /// <summary>
        /// Monitor that is currently displaying
        /// </summary>
        public UIMonitor CurrentMonitor = null;
        public static bool IsClosing = false;
        
        public Form1()
        {
            InitializeComponent();
            InitialMonitor();
        }
        public Label NotificationLable { get { return this.lb_Notification; } }

        private void InitialMonitor()
        {
            try { MonitorCFGFile = ConfigurationManager.AppSettings["MonitorCFGFile"]; } catch { }
            try { HeartBeatDestination = ConfigurationManager.AppSettings["HeartBeatDestination"]; } catch { }
            try { CommandDestination = ConfigurationManager.AppSettings["CommandDestination"]; } catch { }
            try { Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]); } catch { }
            try { CommandPort =Convert.ToInt32( ConfigurationManager.AppSettings["CommandPort"]); } catch { }
            try { ThisNodeName = ConfigurationManager.AppSettings["ThisNodeName"]; } catch { }
            try { IOIntervalToFlushInSec = Convert.ToInt32(ConfigurationManager.AppSettings["IOIntervalToFlushInSec"]); } catch { }
            FNetMonitor.BufferLengthInSec = 86400;
            try { FNetMonitor.BufferLengthInSec = Convert.ToInt32(ConfigurationManager.AppSettings["BufferLengthInSec"]); } catch { }
            _MonitorManager = new FNetMonitorManager(this, ThisNodeName, MonitorCFGFile, HeartBeatDestination, Port,CommandDestination, CommandPort,  IOIntervalToFlushInSec, this.MonitorTreeView, this.messageBox, this.cb_WADCName);
            //FNetMonitorManager.ThisManager = _MonitorManager;
           
            MonitorPlot.YTitle = "Received Value";
            MonitorPlot.ChartTitle = "Plot of the received monitored value";
            MonitorPlot.XLabelStyle = "HH:mm:ss";
            MonitorPlot.YLabelStyle = "F3";
            MonitorPlot.InitializeDetails(FNetMonitor.BufferLengthInSec);


        }

        public string AlertMessage = "";
        public Thread BlinkAlertThread=null;
        public DateTime AlertExpireTime = DateTime.Now;
        bool IterationFlag = false;
        delegate void FuncNoPara();
        public void DisplayAlert(string msg)
        {
            AlertMessage = msg;
            AlertExpireTime = DateTime.Now.AddSeconds(10);
            if(BlinkAlertThread==null)
            {
                BlinkAlertThread = new Thread(new ThreadStart(BlinkAlert));
                BlinkAlertThread.Start();
            }
        }
        public void BlinkAlert()
        {
            
            while(true)
            {
                if (IsClosing) break;
                if (DateTime.Now.CompareTo(AlertExpireTime) >= 0)
                {
                    RemoveAlert();
                    Thread.Sleep(200);
                }
                else
                {
                    IterationFlag = !IterationFlag;
                    RefreshAlert();
                    Thread.Sleep(1000);
                }
            }
        }
        private void RemoveAlert()
        {
            if (lb_Notification.InvokeRequired)
            {
                lb_Notification.Invoke(new FuncNoPara(RemoveAlert));
                return;
            }
            lb_Notification.Text = "(notification)";
            lb_Notification.ForeColor = Color.Black;
            lb_Notification.BackColor = Color.LightGray;
            
        }
        private void RefreshAlert()
        {
            if (lb_Notification.InvokeRequired)
            {
                lb_Notification.Invoke(new FuncNoPara(RefreshAlert));
                return;
            }
            lb_Notification.Text = AlertMessage;
            if(IterationFlag)
            {
                lb_Notification.ForeColor = Color.Red;
                lb_Notification.BackColor = Color.Black;
            }
            else
            {
                lb_Notification.BackColor = Color.Red;
                lb_Notification.ForeColor = Color.Black;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //MonitorCFG.saveToFile();
            var list= MonitorCFG.LoadMonitorCFG("test.txt");
        }

        private void MonitorTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (!(MonitorTreeView.SelectedNode is UIMonitor)) return;
                UIMonitor selectedNode = (UIMonitor)MonitorTreeView.SelectedNode;
                if (selectedNode != CurrentMonitor)
                {
                    if (CurrentMonitor != null)
                    {
                        if (CurrentMonitor.ThisMonitor.ThisGroup == null)
                            CurrentMonitor.ThisMonitor.DataReceived -= MonitorPlot.AddData;
                        else
                        {
                            foreach (var v in CurrentMonitor.ThisMonitor.ThisGroup.Monitores)
                                v.DataReceived -= MonitorPlot.AddData;
                        }
                        MonitorPlot.Clear();
                        MonitorPlot.LatestTimestamp = DateTime.MinValue;
                    }
                    MonitorPlot.ChartTitle = selectedNode.ThisMonitor.Key;
                    List<PlotMultiSeries.SeriesNameAndTimedValue> SeriesDataList = new List<PlotMultiSeries.SeriesNameAndTimedValue>();
                    if (selectedNode.ThisMonitor.ThisGroup == null)
                    {
                        lock (selectedNode.ThisMonitor.ReceivedValues)
                        {
                            foreach (var v in selectedNode.ThisMonitor.ReceivedValues)
                                SeriesDataList.Add(new PlotMultiSeries.SeriesNameAndTimedValue(selectedNode.ThisMonitor.Key, v.TimeStamp, v.Value));
                        }
                    }
                    else
                    {
                        foreach (var m in selectedNode.ThisMonitor.ThisGroup.Monitores)
                            lock (m.ReceivedValues)
                            {
                                foreach (var v in m.ReceivedValues)
                                    SeriesDataList.Add(new PlotMultiSeries.SeriesNameAndTimedValue(m.Key, v.TimeStamp, v.Value));
                            }
                    }
                    MonitorPlot.AddData(SeriesDataList);

                    if (selectedNode.ThisMonitor.ThisGroup == null)
                        selectedNode.ThisMonitor.DataReceived += MonitorPlot.AddData;
                    else
                        foreach (var m in selectedNode.ThisMonitor.ThisGroup.Monitores)
                            m.DataReceived += MonitorPlot.AddData;
                    CurrentMonitor = selectedNode;
                }
                //else
                //{
                //    selectedNode.ThisMonitor.DataReceived -= MonitorPlot.AddData;
                //    CurrentMonitor = null;
                //}
            }
            catch(Exception ee)
            {

            }
        }

        private void MonitorTreeView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;
            MonitorTreeView.SelectedNode = MonitorTreeView.GetNodeAt(e.X, e.Y);
            if (!(MonitorTreeView.SelectedNode is UINode)) return;
            UINode selectedNode = (UINode)MonitorTreeView.SelectedNode;
            enableWADCButton.Text = selectedNode.Node.WADCEnabled ? "Disable WADC" : "Enable WADC";
            temporarilyToolStripMenuItem.Visible = false;
            enableWADCButton.Visible = true;
            MonitorMenuStrip.Show(MonitorTreeView, e.X, e.Y);

        }

        private void TemporarilyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!(MonitorTreeView.SelectedNode is UIMonitor)) return;
            UIMonitor selectedNode = (UIMonitor)MonitorTreeView.SelectedNode;
            DisableMonitor DisableWin = new DisableMonitor();
            DisableWin.AttachToMonitor(selectedNode.ThisMonitor);
            DisableWin.ShowDialog();
        }

        private void Bt_SendStatusCMD_Click(object sender, EventArgs e)
        {
            _MonitorManager.SendRelayStatusCommand(cb_EnableWADC.Checked, cb_EnablePSS.Checked, cb_WADCName.Text);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            IsClosing = true;
        }

        private void enableWADCButton_Click(object sender, EventArgs e)
        {
            if (!(MonitorTreeView.SelectedNode is UINode)) return;
            UINode selectedNode = (UINode)MonitorTreeView.SelectedNode;
            _MonitorManager.SendEnableWADCCmd(!selectedNode.Node.WADCEnabled, selectedNode.Node.NodeName);
        }
    }
}
