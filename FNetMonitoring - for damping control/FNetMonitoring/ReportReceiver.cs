﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Net.NetworkInformation;

namespace FNetMonitoring
{
    public class ReportReceiver
    {
        private FNetMonitorManager MonitorManager;
        private int PortToReceive;
        private UdpClient ThisUDPClient;
        private IPEndPoint ThisIPEndPoint;

        public ReportReceiver(int portToReceive, FNetMonitorManager monitorManager)
        {
            PortToReceive = portToReceive;
            MonitorManager = monitorManager;
            StartToListen();
        }

        private void StartToListen()
        {
            try
            {
                if (PortToReceive > 65535) return;
                ThisUDPClient = new UdpClient(PortToReceive);
                ThisIPEndPoint = new IPEndPoint(IPAddress.Any, PortToReceive);
                try
                {
                    ThisUDPClient.BeginReceive(new AsyncCallback(ReceiveCallback), null);
                }
                catch { }
            }
            catch (Exception e)
            {
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            if (FNetMonitorManager.IsClosing) return;
            try
            {
                byte[] ReceivedByte;
                try { ReceivedByte = ThisUDPClient.EndReceive(ar, ref ThisIPEndPoint); }
                catch { return; }
                if (ReceivedByte != null && ReceivedByte.Length > 0)
                {
                    string content = Encoding.ASCII.GetString(ReceivedByte, 0, ReceivedByte.Length).Trim();
                    if (MonitorManager != null && content.Length > 5)
                        MonitorManager.ReportStatusValue(content);
                }
            }
            catch { }
            ThisUDPClient.BeginReceive(new AsyncCallback(ReceiveCallback), null);
        }
    }
}
