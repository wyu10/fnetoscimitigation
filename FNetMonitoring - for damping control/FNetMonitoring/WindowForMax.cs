﻿using System.Collections.Generic;
namespace FNetMonitoring
{
    public class WindowForMax<T>
    {
        public delegate int Compare<Ty>(Ty A, Ty B);
        private Compare<T> _isAPriorToB;
        private LinkedList<ObjWithIndex> _window = new LinkedList<ObjWithIndex>();
        private long _curIndex = 0;
        private long _windowLenght = 0;
        private int _count;
        public delegate bool CheckExpiration(T LatestItem, T OldestItem);
        private CheckExpiration CheckExpirationHandler = null;
        public WindowForMax(long windowLength, Compare<T> CompareHandler, CheckExpiration checkExpirationMethod=null)
        {
            _windowLenght = windowLength;
            _isAPriorToB = CompareHandler;
            if (checkExpirationMethod != null)
                CheckExpirationHandler = checkExpirationMethod;
        }
        public void Push(T OneItem)
        {
            ObjWithIndex O = new ObjWithIndex(++_curIndex, OneItem);
            while (_window.Count > 0 && _isAPriorToB(OneItem, _window.First.Value.Obj) > 0)
                _window.RemoveFirst();
            _window.AddFirst(O);
            if(_count< _windowLenght)_count++;

            while (_window.Count > 0 &&
                ((CheckExpirationHandler!=null && CheckExpirationHandler(OneItem,_window.Last.Value.Obj))||(CheckExpirationHandler==null &&_window.Last.Value.Index <= _curIndex - _windowLenght)))
                _window.RemoveLast();
        }
        public void SetWindowLength(long NewLength)
        {
            _windowLenght = NewLength;
        }
        public int Count { get { return _count; } }
        public void Clear()
        {
            _window.Clear();
            _count = 0;
            _curIndex = 0;
        }
        public T Max
        {
            get { return _window.Count > 0 ? _window.Last.Value.Obj : default(T); }
        }
        private struct ObjWithIndex
        {
            public ObjWithIndex(long index, T obj)
            {
                Obj = obj;
                Index = index;
            }
            public long Index;
            public T Obj;
        }
    }
    public class WindowForMaxAndMin<T>
    {
        private WindowForMax<T> _windowForMax;
        private WindowForMax<T> _windowForMin;
        public long MaxWindowLength = 0;
        public WindowForMaxAndMin(long windowLength, WindowForMax<T>.Compare<T> CompareHandler, WindowForMax<T>.CheckExpiration CheckExpirationMethod=null)
        {
            _windowForMax = new WindowForMax<T>(windowLength, CompareHandler, CheckExpirationMethod);
            _windowForMin = new WindowForMax<T>(windowLength, new WindowForMax<T>.Compare<T>((A, B) => CompareHandler(B, A)), CheckExpirationMethod);
            MaxWindowLength = windowLength;
        }
        public int Count { get { return _windowForMax.Count; } }
        public void SetWindowLength(long NewLength)
        {
            _windowForMax.SetWindowLength(NewLength);
            _windowForMin.SetWindowLength(NewLength);
            MaxWindowLength = NewLength;
        }
        public void Clear()
        {
            _windowForMax.Clear();
            _windowForMin.Clear();
        }
        public void Push(T OneItem)
        {
            _windowForMax.Push(OneItem);
            _windowForMin.Push(OneItem);
        }
        public T Max { get { return _windowForMax.Max; } }
        public T Min { get { return _windowForMin.Max; } }
    }
}
