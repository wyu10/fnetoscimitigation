﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using FNetBaseLib.YWPLib;

namespace FNetMonitoring
{
    public partial class PlotMultiSeries : UserControl
    {
        private Dictionary<string, Series> _plotSerie = new Dictionary<string, Series>();
        private SeriesChartType _seriesType = SeriesChartType.FastLine;
        private ChartValueType _xValueType = ChartValueType.DateTime;
        private string _chartAreaName= "ChartAreaName";
        private string _yTitle = "MyYName";
        private string _chartTitle = "ChartTitle";
        private string _yLabelStyle = "F2";
        private string _xLabelStyle = "HH:mm:ss";
        private double _titleHeight = 10;
        private ChartArea _chartArea;
        private Legend _legend;
        public delegate void AddDataPoints(List<SeriesNameAndTimedValue> SeriesDataList);
        public delegate void AddDataPoints2();
        private delegate void DelegateWithString(string str);
        private delegate void DelegateWithoutPara();
        private WindowForMaxAndMin<ValueWithTimeStamp> _toGetMaxMin;
        private double _windowLenInSec = 60;
        private double _startTime = -1;
        //private bool _startTimeInitialized = false;
        public int AxisYBinCount = 6;
        private DateTime _lastRptTime = DateTime.MinValue;
        private double _lastRptValue = 0;
        public PlotMultiSeries()
        {
            InitializeComponent();
            ChartTitle = "My Chart";
            RptTime.Text = "";
        }
        public string YTitle
        {
            get { return _yTitle; }
            set
            {
                _yTitle = value;
                if (_chartArea != null)
                    _chartArea.AxisY.Title = value;
            }
        }
        public string ChartTitle
        {
            get { return _chartTitle; }
            set
            {
                _chartTitle = value;
                MainChart.Titles.Clear();
                MainChart.Titles.Add(value);
                MainChart.Titles[0].ForeColor = Color.Blue;
                MainChart.Titles[0].Font = new Font("Arial", 10);
                MainChart.Titles[0].Position.X = 50;
                MainChart.Titles[0].Position.Y = 0;
                MainChart.Titles[0].Position.Height = 100;
                MainChart.Titles[0].Alignment = ContentAlignment.TopCenter;
            }
        }
        public string XLabelStyle
        {
            get { return _xLabelStyle; }
            set
            {
                _xLabelStyle = value;
                if (_chartArea != null)
                    _chartArea.AxisX.LabelStyle.Format = _xLabelStyle;
            }
        }
        public string YLabelStyle
        {
            get { return _yLabelStyle; }
            set
            {
                _yLabelStyle = value;
                if (_chartArea != null)
                    _chartArea.AxisY.LabelStyle.Format = _yLabelStyle;
            }
        }
        public void InitializeDetails(double PlotWindowLenInSec)
        {
            try
            {
                _windowLenInSec = PlotWindowLenInSec;
                _toGetMaxMin = new WindowForMaxAndMin<ValueWithTimeStamp>(20000,
                    new WindowForMax<ValueWithTimeStamp>.Compare<ValueWithTimeStamp>((x, y) => x.Value.CompareTo(y.Value)),
                    new WindowForMax<ValueWithTimeStamp>.CheckExpiration((LatestItem, OldestItem) => OldestItem.TimeStamp.AddSeconds(_windowLenInSec).CompareTo(LatestItem.TimeStamp) < 0));
                _chartArea = new ChartArea(_chartAreaName);
                _legend = new Legend(_chartAreaName);
                MainChart.ChartAreas.Add(_chartArea);
                MainChart.Legends.Add(_legend);
                _legend.DockedToChartArea = _chartAreaName;
                _legend.IsDockedInsideChartArea = true;
                _legend.Docking = Docking.Left;
                _legend.BackColor = Color.Transparent;
                _legend.BorderWidth = 1;
                _legend.BorderColor = Color.LightGray;
                //_legend.BackColor = Color.FromArgb(239, 250, 250);// Color.LightGray;  
                _legend.LegendStyle = LegendStyle.Column;
                //-----Configure the frequency chart------
                _chartArea.BorderWidth = 2;
                _chartArea.BorderDashStyle = ChartDashStyle.Solid;
                _chartArea.IsSameFontSizeForAllAxes = true;
                _chartArea.Position.X = 0;
                _chartArea.Position.Width = 100;
                int rel = (int)(_titleHeight / MainChart.Height * 100);
                if (rel >= 0)
                {
                    _chartArea.Position.Y = rel;
                    _chartArea.Position.Height = 100 - rel;
                }

                _chartArea.AxisX.LabelStyle.Format = _xLabelStyle;
                _chartArea.AxisY.LabelStyle.Format = _yLabelStyle;
                //_chartArea.AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount;
                _chartArea.AxisX.LabelStyle.TruncatedLabels = false;
                _chartArea.AxisX.LabelStyle.IsEndLabelVisible = false;
                _chartArea.AxisX.LineWidth = 3;
                _chartArea.AxisY.LineWidth = 3;
                _chartArea.AxisY.LabelStyle.TruncatedLabels = true;
                _chartArea.AxisX.LabelAutoFitMaxFontSize = 8;
                _chartArea.AxisY.LabelAutoFitMaxFontSize = 8;
                _chartArea.AxisY.IsMarginVisible = false;
                _chartArea.AxisX.IsMarginVisible = false;
                _chartArea.AxisY.Minimum = 0;
                _chartArea.AxisY.Maximum = 1000;
                _chartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
                _chartArea.AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                _chartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
                _chartArea.AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
                _chartArea.AxisX.MinorGrid.Enabled = false;
                //_chartArea.AxisX.MinorGrid.Interval = 1;
                //_chartArea.AxisX.MinorGrid.LineColor = Color.FromArgb(235, 235, 235);
                //_chartArea.AxisX.MinorGrid.LineDashStyle = ChartDashStyle.Dash;
                //_chartArea.AxisX.MinorGrid.LineWidth = 1;
                //_chartArea.AxisX.MinorGrid.IntervalType = DateTimeIntervalType.Seconds;
                _chartArea.AxisY.LineWidth = 1;
                _chartArea.AxisX.LineWidth = 1;
                _chartArea.AxisY.Title = _yTitle;
                _chartArea.AxisY.TitleFont = new Font("Arial", 8);
                _chartArea.AxisX.ScaleView.Position = 0.0;
                _chartArea.AxisY.ScaleView.Position = 0.0;
            }
            catch(Exception e)
            {
                //FNetServerManager.LogException(YWPLogCategory.Type_Application_Exception, 0, "", e);
            }
        }
        public DateTime LatestTimestamp = DateTime.MinValue;
        List<SeriesNameAndTimedValue> SeriesDataList = new List<SeriesNameAndTimedValue>();
        DateTime NextRefreshTime = DateTime.Now;

        public void AddData(List<SeriesNameAndTimedValue> SeriesData)
        {
            foreach (var v in SeriesData) SeriesDataList.Add(v);
            if (DateTime.Now.CompareTo(NextRefreshTime) < 0) return;
            NextRefreshTime = DateTime.Now.AddMilliseconds(300);
            if (SeriesDataList == null || SeriesDataList.Count < 1) return;
            try
            {
                if (MainChart.InvokeRequired)
                {
                    MainChart.Invoke(new AddDataPoints2(AdddataToChart));
                    return;
                }
                else
                    AdddataToChart();
            }
            catch(Exception e)
            {
                //FNetServerManager.LogException(YWPLogCategory.Type_Application_Exception, 0, "", e);
            }
        }
        public void ClearBuf() { SeriesDataList.Clear(); }
        void AdddataToChart()
        {
            try
            {
                double LatestOADate;
                foreach (var SeriesData in SeriesDataList)
                {
                    if (!_plotSerie.ContainsKey(SeriesData.SeriesName))
                        _plotSerie.Add(SeriesData.SeriesName, CreateNewSeries(SeriesData.SeriesName));
                    var series = _plotSerie[SeriesData.SeriesName];
                    //--------------------------------------------------
                    if (SeriesData.TimeStamp.CompareTo(LatestTimestamp) > 0)
                        LatestTimestamp = SeriesData.TimeStamp;
                    if (double.IsNaN(SeriesData.Value))
                        continue;
                    series.Points.AddXY(SeriesData.TimeStamp.ToOADate(), SeriesData.Value);
                    if (_lastRptTime.CompareTo(SeriesData.TimeStamp) < 0)
                    {
                        _lastRptTime = SeriesData.TimeStamp;
                        _lastRptValue = SeriesData.Value;
                    }
                    _toGetMaxMin.Push(SeriesData);
                    //--------------------------------------------------
                    //foreach (var point in SeriesData.SeriesData)
                    //{
                    //    if (point.TimeStamp.CompareTo(Now) > 0)
                    //        Now = point.TimeStamp;
                    //    series.Points.AddXY(point.TimeStamp.ToOADate(), point.Value);
                    //    _toGetMaxMin.Push(point);
                    //}
                    if (_startTime < 0)
                        _startTime = LatestTimestamp.AddSeconds(-10).ToOADate();
                    LatestOADate = LatestTimestamp.AddSeconds(-1 * _windowLenInSec).ToOADate();
                    //while (series.Points.Count > _MaxPointCnt)
                    while (series.Points.Count > 0 && series.Points.First().XValue < LatestOADate)
                        series.Points.RemoveAt(0);

                }
                RptTime.Text = "Last Report Time and Value: " + _lastRptTime.ToString("yyyy-MM-dd HH:mm:ss") + ", " + _lastRptValue.ToString("F6");
                LatestOADate = Math.Max(LatestTimestamp.AddSeconds(-1 * _windowLenInSec).ToOADate(), _startTime);
                _chartArea.AxisX.Minimum = LatestOADate;
                _chartArea.AxisX.Maximum = LatestTimestamp.ToOADate();
                double Max, Min, Interval;
                GetRangeAndInterval(_toGetMaxMin.Max.Value, _toGetMaxMin.Min.Value, AxisYBinCount, out Max, out Min, out Interval);

                _chartArea.AxisY.Minimum = Min;
                _chartArea.AxisY.Maximum = Max;
                if (Interval > 0)
                    _chartArea.AxisY.Interval = Interval;
            }
            catch (Exception ee) { }
            SeriesDataList.Clear();
        }
        private Series CreateNewSeries(string name)
        {
            Series s = MainChart.Series.Add(name);
            s.Name = name;
            s.Label = name;
            s.ChartArea = _chartAreaName;
            s.Legend = _chartAreaName;
            s.BorderWidth = 1;
            s.XValueType = _xValueType;
            s.ChartType = _seriesType;
            return s;
        }
        public void RemoveOneSeries(string name)
        {
            if(MainChart.InvokeRequired)
            {
                MainChart.Invoke(new DelegateWithString(RemoveOneSeries), name);
                return;
            }
            if(_plotSerie.ContainsKey(name))
            {
                _plotSerie[name].Points.Clear();
                MainChart.Series.Remove(_plotSerie[name]);
                _plotSerie.Remove(name);
            }
        }
        public void Clear()
        {
            if (MainChart.InvokeRequired)
            {
                MainChart.Invoke(new DelegateWithoutPara(Clear));
                return;
            }
            foreach (var v in _plotSerie)
                v.Value.Points.Clear();
            _plotSerie.Clear();
            MainChart.Series.Clear();
            _toGetMaxMin.Clear();
            _lastRptTime = DateTime.MinValue;
            ClearBuf();
        }
        /// <summary>
        /// Given maximum & minimum value and desired bin count, this function will return the best minmum & maximum value and interval for plotting.
        /// </summary>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <param name="binCount"></param>
        /// <param name="Max"></param>
        /// <param name="Min"></param>
        /// <param name="Interval"></param>
        private static void GetRangeAndInterval(double max, double min, int binCount, out double Max, out double Min, out double Interval)
        {
            if(max<min)
            {
                double t = min;
                min = max;
                max = t;
            }
            if(max == min ||max-min<0.00000001)
            {
                max += 0.01;
                min -= 0.01;
            }
            Max = max;
            Min = min;
            if (binCount > 0) Interval = (max - min) / binCount;
            else Interval = (max - min);
            if (max == min) return;
            double BinWidth = (max - min) / binCount;
            double Order = 1;
            while (BinWidth < 1) { BinWidth *= 10; Order /= 10; }
            while (BinWidth >= 10) { BinWidth /= 10; Order *= 10; }
            if (BinWidth <= 1.414) Interval = 1;
            else if (BinWidth <= 3.162) Interval = 2;
            else if (BinWidth <= 7.071) Interval = 5;
            else Interval = 10;
            Interval *= Order;
            Max = (int)(max / Interval) * Interval;
            while (Max < max) Max += Interval;
            Min = (int)(min / Interval) * Interval;
            while (Min > min) Min -= Interval;
        }








        public class ValueWithTimeStamp
        {
            public DateTime TimeStamp;
            public double Value;
            public ValueWithTimeStamp(DateTime timestamp, double val)
            {
                TimeStamp = timestamp;
                Value = val;
            }
        }
        //public class SeriesNameAndTimedValue
        //{
        //    public string SeriesName;
        //    public List<ValueWithTimeStamp> SeriesData = new List<ValueWithTimeStamp>();
        //    public SeriesNameAndTimedValue(string name)
        //    {
        //        SeriesName = name;
        //    }
        //    public void AddValue(DateTime time, double value)
        //    {
        //        SeriesData.Add(new ValueWithTimeStamp(time, value));
        //    }
        //}
        public class SeriesNameAndTimedValue: ValueWithTimeStamp
        {
            public string SeriesName;
            public SeriesNameAndTimedValue(string name, DateTime time, double value):base(time,value)
            {
                SeriesName = name;
            }
        }

        private void MainChart_SizeChanged(object sender, EventArgs e)
        {
            if (_chartArea != null && MainChart.Height > _titleHeight)
            {
                int rel = (int)(_titleHeight / MainChart.Height * 100);
                _chartArea.Position.Y = rel;
                _chartArea.Position.Height = 100 - rel;
            }
        }
    }
}
