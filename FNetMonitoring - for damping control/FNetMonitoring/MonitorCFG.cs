﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace FNetMonitoring
{
    [DataContract]
    public class MonitorCFG
    {
        /// <summary>
        /// Can be a name of computer or program which report status to the monitor
        /// </summary>
        [DataMember]
        public string NodeName;
        /// <summary>
        /// The reported status name, such as cpu usage, memory, FDR number...
        /// </summary>
        [DataMember]
        public string StatusName;
        /// <summary>
        /// specify if larger value is better.
        /// </summary>
        [DataMember]
        public bool AlertLarger = true;
        /// <summary>
        /// threshold for first level abnormal
        /// </summary>
        [DataMember]
        public double AbnormalTH_First;
        /// <summary>
        /// the duration of first level abnormal before sending alerts. 0: disable.
        /// </summary>
        [DataMember]
        public int DurationInSec_First = 90;
        /// <summary>
        /// threshold for second level abnormal
        /// </summary>
        [DataMember]
        public double AbnormalTH_Second;
        /// <summary>
        /// the duration of first level abnormal before sending alerts. 0: disable.
        /// </summary>
        [DataMember]
        public int DurationInSec_Second = 90;
        /// <summary>
        /// the duration of no reports before sending alert. 0: Don't capture no report abnormal.
        /// </summary>
        [DataMember]
        public int AbnormalTH_NoRptTimeInSec;

        /// <summary>
        /// Pre-defined alert message which will be sent to subscribers.
        /// </summary>
        [DataMember]
        public string AlertMessage;
        /// <summary>
        /// Email list. Use ; as delimiter
        /// </summary>
        [DataMember]
        public string EmailReceivers;

        [DataMember]
        public int ReportIntervalInSec_Second;

        [DataMember]
        public string GroupName = "";
        public MonitorCFG()
        { }
        public MonitorCFG(string nodeName, string statusName)
        {
            NodeName = nodeName;
            StatusName = statusName;
            DurationInSec_First = 0;
            DurationInSec_Second = 0;
            AbnormalTH_NoRptTimeInSec = 0;
            EmailReceivers = "";
        }
        public static List<MonitorCFG> MonitorCFGList;
        
        public static List<MonitorCFG> LoadMonitorCFG(string fileName)
        {
            if (!File.Exists(fileName))
                return null;
            StreamReader sr = new StreamReader(fileName);
            StringBuilder sb = new StringBuilder();
            string oneLine;
            var ser = new DataContractJsonSerializer(typeof(MonitorCFG));
            List<MonitorCFG> cfgList = new List<MonitorCFG>();
            while (!sr.EndOfStream)
            {
                oneLine = sr.ReadLine().Trim();
                if (oneLine.Length < 1 || oneLine[0] == '#')
                    continue;
                sb.AppendLine(oneLine);
                if(oneLine.Trim().EndsWith("}"))
                {
                    try
                    {
                        MonitorCFG oneCFG =(MonitorCFG)ser.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString())));
                        //if(oneCFG.Group!=null && oneCFG.Group.Length>0)
                        //{
                        //    if (!GroupDic.ContainsKey(oneCFG.Group)) GroupDic.Add(oneCFG.Group, new Group(oneCFG.Group));
                        //    GroupDic[oneCFG.Group].Monitores.Add(oneC)
                        //}
                        cfgList.Add(oneCFG);
                    }
                    catch(Exception e)
                    { }
                    sb.Clear();
                }
            }
            sr.Close();
            return cfgList;
        }
        public static void saveToFile()
        {
            MonitorCFG oneCFG = new MonitorCFG();
            oneCFG.NodeName = "Test Node";
            oneCFG.StatusName = "Test status";
            oneCFG.AlertLarger = true;
            oneCFG.AbnormalTH_First = 10;
            oneCFG.AbnormalTH_Second = 20;
            oneCFG.AbnormalTH_NoRptTimeInSec = 200;
            oneCFG.DurationInSec_First = 30;
            oneCFG.DurationInSec_Second = 80;
            oneCFG.AlertMessage = "Test Message;\r\ntest message 2;";
            oneCFG.EmailReceivers = "wyu10@utk.edu;wyu900@gmail.com";
            var ser = new DataContractJsonSerializer(typeof(MonitorCFG));
            var ms = new MemoryStream();
            ser.WriteObject(ms, oneCFG);
            byte[] json = ms.ToArray();
            StreamWriter sw = new StreamWriter("test.txt");
            sw.WriteLine(Encoding.UTF8.GetString(json, 0, json.Length));
            sw.Close();
        }

    }
    public class Group
    {
        public static Dictionary<string, Group> GroupDic = new Dictionary<string, Group>();
        public string GroupName = "";
        public List<FNetMonitor> Monitores = new List<FNetMonitor>();
        public Group(string name)
        { GroupName = name; }
    }
}
