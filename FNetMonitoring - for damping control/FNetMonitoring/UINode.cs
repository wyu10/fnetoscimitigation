﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FNetMonitoring
{
    public class UINode:TreeNode
    {
        public Node Node;
    }
    public class UIMonitor:TreeNode
    {
        public FNetMonitor ThisMonitor;
    }
    public class Node
    {
        public string NodeName;
        public UINode ThisUINode;
        public bool WADCEnabled = false;
        public int RelayCode = 0;   //Bit 1: Relay WADC enable;  Bit 0: Relay PSS enable
        public int CurrentImgIndex = 1;

        public Node(string nodeName)
        {
            NodeName = nodeName;
            ThisUINode = new UINode();
            ThisUINode.Text = nodeName;
            ThisUINode.Node = this;
            ThisUINode.ImageIndex = CurrentImgIndex;
            ThisUINode.SelectedImageIndex = CurrentImgIndex;
            ThisUINode.ForeColor = Color.Gray;
        }
        private List<FNetMonitor> MonitorList = new List<FNetMonitor>();
        public void AddMonitor(FNetMonitor oneM)
        {
            MonitorList.Add(oneM);
            ThisUINode.Nodes.Add(oneM.MonitorTreeNode);
        }
        public void UpdateNode(string statusName, double statusValue)
        {

            if (statusName == "EnableWADC")
            {
                WADCEnabled = statusValue > 0 ? true : false;
                ThisUINode.ForeColor = WADCEnabled ? Color.Green : Color.Gray;
            }
            else if (statusName == "Relay")
            {
                RelayCode = (int)statusValue;
                if (CurrentImgIndex != RelayCode + 1)
                {
                    CurrentImgIndex = RelayCode + 1;
                    ThisUINode.ImageIndex = ThisUINode.SelectedImageIndex = RelayCode + 1;
                }
            }            
        }




    }
}
