﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNETOsciMitigation
{
    public enum CommandType : ushort
    {
        /// <summary>
        /// 0001 Turn off transmission of data frames.
        /// </summary>
        DisableRealTimeData = (ushort)Bits.Bit00,
        /// <summary>
        /// 0010 Turn on transmission of data frames.
        /// </summary>
        EnableRealTimeData = (ushort)Bits.Bit01,
        /// <summary>
        /// 0011 Send header file.
        /// </summary>
        SendHeaderFrame = (ushort)Bits.Bit00 | (ushort)Bits.Bit01,
        /// <summary>
        /// 0100 Send configuration file 1.
        /// </summary>
        SendConfigurationFrame1 = (ushort)Bits.Bit02,
        /// <summary>
        /// 0101 Send configuration file 2.
        /// </summary>
        SendConfigurationFrame2 = (ushort)Bits.Bit00 | (ushort)Bits.Bit02,
        /// <summary>
        /// 0110 Send configuration frame 3.
        /// </summary>
        SendConfigurationFrame3 = (ushort)Bits.Bit01 | (ushort)Bits.Bit02,
        /// <summary>
        /// 1000 Receive extended frame for IEEE C37.118 / receive reference phasor for IEEE 1344.
        /// </summary>
        ReceiveExtendedFrame = (ushort)Bits.Bit03,
        /// <summary>
        /// Reserved bits.
        /// </summary>
        ReservedBits = ushort.MaxValue & ~((ushort)Bits.Bit00 | (ushort)Bits.Bit01 | (ushort)Bits.Bit02 | (ushort)Bits.Bit03)
    }
}
