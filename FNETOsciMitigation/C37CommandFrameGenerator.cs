﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNETOsciMitigation
{
    public class C37CommandFrameGenerator
    {
        /// <summary>
        /// Default length of Command frame without ExtData
        /// </summary>
        public static int ImageLength = 18;
        public static byte[] GetCommandImg(DateTime timestamp, ushort IDCode, CommandType Command, byte[] extFrame)
        {
            int extLength = 0;
            if (extFrame != null)
                extLength = extFrame.Length;
            byte[] buffer = new byte[ImageLength + extLength];
            buffer[0] = 0xAA;
            buffer[1] = 0x41;   //Sync AA-41
            FNetBigEndian.CopyBytes((ushort)(ImageLength + extLength), buffer, 2);   //frame length
            FNetBigEndian.CopyBytes(IDCode, buffer, 4);                              //Unit ID.
            //FNetBigEndian.CopyBytes((int)Math.Truncate(DateTime.UtcNow.Subtract(Common.StartTime).TotalSeconds), buffer, 6);    //SOC
            FNetBigEndian.CopyBytes((int)Math.Truncate(timestamp.Subtract(Common.StartTime).TotalSeconds), buffer, 6);    //SOC
            //Timebase of fractional second is microsecond (us).
            FNetBigEndian.CopyBytes(timestamp.Millisecond*1000, buffer, 10);    //fractional secondSOC
            FNetBigEndian.CopyBytes((ushort)Command, buffer, 14);                    //CMD code
            //append Extended data.
            for (int i = 0; i < extLength; i++)
                buffer[16 + i] = extFrame[i];
            AppendChecksum(buffer, buffer.Length - 2);
            return buffer;
        }
        public static void AppendChecksum(byte[] buffer, int startIndex)
        {
            CrcCCITT ccitt = new CrcCCITT();
            ccitt.Update(buffer, 0, startIndex);
            FNetBigEndian.CopyBytes(ccitt.Value, buffer, startIndex);
        }

        //public static byte[] GetCommandImg(ushort IDCode, CommandType Command)
        //{
        //    byte[] buffer = new byte[ImageLength];
        //    buffer[0] = 0xAA;
        //    buffer[1] = 0x41;
        //    buffer[3] = 18; //frame length
        //    FNetBigEndian.CopyBytes(IDCode, buffer, 4);
        //    FNetBigEndian.CopyBytes((int)Math.Truncate(DateTime.UtcNow.Subtract(Common.StartTime).TotalSeconds), buffer, 6);    //SOC
        //    FNetBigEndian.CopyBytes((ushort)Command, buffer, 14);
        //    AppendChecksum(buffer, 16);
        //    return buffer;
        //}      
        
    }
    public class Common
    {
        public static DateTime StartTime = new DateTime(1970, 1, 1);
        public const uint TimeQualityFlagsMask = (uint)(Bits.Bit31 | Bits.Bit30 | Bits.Bit29 | Bits.Bit28 | Bits.Bit27 | Bits.Bit26 | Bits.Bit25 | Bits.Bit24);
    }
    //FNetBigEndian
    public static class FNetBigEndian
    {
        public static short ToInt16(byte[] buffer, int startIndex)
        {
            return (short)((int)buffer[startIndex] << 8 | (int)buffer[startIndex + 1]);
        }
        public static ushort ToUInt16(byte[] buffer, int startIndex)
        {
            return (ushort)ToInt16(buffer, startIndex);
        }
        public static uint ToUInt32(byte[] buffer, int startIndex)
        {
            return (uint)ToInt32(buffer, startIndex);
        }
        public static int ToInt32(byte[] buffer, int startIndex)
        {
            return (int)buffer[startIndex + 0] << 24 |
                   (int)buffer[startIndex + 1] << 16 |
                   (int)buffer[startIndex + 2] << 8 |
                   (int)buffer[startIndex + 3];
        }
        public static int ToInt32(byte[] buffer, int startIndex, int length)
        {
            int res = 0;
            for (int i = 0; i < length; i++)
                res = res << 8 | buffer[startIndex + i];
            return res;
        }
        public static int CopyBytes(ushort value, byte[] destinationArray, int destinationIndex)
        {
            return CopyBytes((short)value, destinationArray, destinationIndex);
        }
        public static int CopyBytes(short value, byte[] destinationArray, int destinationIndex)
        {
            destinationArray[destinationIndex] = (byte)(value >> 8);
            destinationArray[destinationIndex + 1] = (byte)(value);
            return 2;
        }
        public static int CopyBytes(uint value, byte[] destinationArray, int destinationIndex)
        {
            return CopyBytes((int)value, destinationArray, destinationIndex);
        }
        public static int CopyBytes(int value, byte[] destinationArray, int destinationIndex)
        {
            destinationArray[destinationIndex + 0] = (byte)(value >> 24);
            destinationArray[destinationIndex + 1] = (byte)(value >> 16);
            destinationArray[destinationIndex + 2] = (byte)(value >> 8);
            destinationArray[destinationIndex + 3] = (byte)(value);
            return 4;
        }
        public static int CopyBytes(byte[] sourceArray, byte[] destinationArray, int destinationIndex)
        {
            if (sourceArray == null || sourceArray.Length == 0) return 0;
            for (int i = 0; i < sourceArray.Length; i++)
                destinationArray[destinationIndex + i] = sourceArray[i];
            return sourceArray.Length;
        }
        //public static unsafe float ToSingle(byte[] buffer, int startIndex)
        //{
        //    float A = BitConverter.ToSingle(buffer, startIndex);
        //    int int32 = ToInt32(buffer, startIndex);
        //    float B = *(float*)&int32;
        //    return B;
        //}

    }
}
