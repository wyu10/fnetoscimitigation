﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNETOsciMitigation
{
    
    public class OsciDetection
    {
        //Parameters that should be specified
        private int _FramePerSecond = 10;
        private double _NominalFreq = 60;
        private double _WindowLength_Step1_Sec = 3;
        private double _WindowLength_Step2_Sec = 3;
        private int _DataWindowAfterOsci_Sec = 45;
        private int _DataWindowBeforeOsci_Sec = 15;

        //whether enable detecting oscillation based on frequency variation (freq-ave freq)
        private bool _EnableOsciDetect_Freq = true;
        //window length to calculate average frequency
        private double _FreqAveWindow_Sec = 4;
        private double _Freq_Step1TH;
        private double _Freq_Step2TH;

        //whether enable detecting oscillation based on relative phase angle (use system angle as reference)
        private bool _EnableOsciDetect_Ang = true;
        private double _Ang_Step1TH;
        private double _Ang_Step2TH;

        //
        private int _FreqAveBase = 10;

        private SystemMedianCalculator _SysCalculator = null;
        private double _AveFreq = 60;
        private Dictionary<int, PMUMeasurement> _PreviousMea = new Dictionary<int, PMUMeasurement>();
        private Dictionary<string, OsciDetector> _OsciDetectorDic = new Dictionary<string, OsciDetector>();
        public int OsciFlagCount = 0;
        private bool _OsciDetected = false;
        /// <summary>
        /// Timestamp of the frame when oscillation was detected.
        /// </summary>
        private DateTime _OsciDetectedTime = DateTime.MinValue; 
        private DateTime _OsciFlagExpireTime = DateTime.MinValue;

        /// <summary>
        /// When oscillation was detected, the timestamp of latest PMU frame
        /// </summary>
        private DateTime _OsciLatestPMUTime = DateTime.MinValue;
        private DateTime _OsciLatestLocalTime = DateTime.MinValue;
        private Dictionary<int, FNETTimeSeriesBuffer<PMUMeasurement>> _Buffers = new Dictionary<int, FNETTimeSeriesBuffer<PMUMeasurement>>();
        private FNETTimeSeriesBuffer<TimeStampedValue> _BufferToAve;
        private Queue<PMUFrame> _FramesBuffer = new Queue<PMUFrame>();
        private int _FrameBufferLength = -1;
        private DateTime _LatestPMUTime = DateTime.MinValue;
        public static string OsciDetectionFolder="OsciDetect\\";
        private bool _PlotOsci = true;
        private OsciMitigator OsciMitigatorObj = null;
        public OsciDetection(OsciMitigator osciMitiObj, int fps, int nominalFreq,
            double step1_window_s, double step2_window_s,
            bool enableOsciDetect_Freq, double freq_step1TH,double freq_step2TH,double freq_aveWindow_s,
            bool enableOsciDetect_Ang, double ang_step1TH,double ang_step2TH,
            bool PlotOsci)
        {
            OsciMitigatorObj = osciMitiObj;
            _FramePerSecond = fps;
            _NominalFreq = nominalFreq;
            _AveFreq = _NominalFreq;
            _WindowLength_Step1_Sec = step1_window_s;
            _WindowLength_Step2_Sec = step2_window_s;

            _EnableOsciDetect_Freq = enableOsciDetect_Freq;
            _FreqAveWindow_Sec = freq_aveWindow_s;
            _FreqAveBase = (int)Math.Round(_FreqAveWindow_Sec * _FramePerSecond / 2, 0);
            _Freq_Step1TH = freq_step1TH;
            _Freq_Step2TH = freq_step2TH;
            _BufferToAve = new FNETTimeSeriesBuffer<TimeStampedValue>(fps, freq_aveWindow_s, (x) => x.Value);
            _FrameBufferLength = (int)(freq_aveWindow_s * _FramePerSecond / 2);

            _EnableOsciDetect_Ang = enableOsciDetect_Ang;
            _Ang_Step1TH = ang_step1TH;
            _Ang_Step2TH = ang_step2TH;
            if (!System.IO.Directory.Exists(OsciDetectionFolder))
                System.IO.Directory.CreateDirectory(OsciDetectionFolder);
            _PlotOsci = PlotOsci;

        }
        public void Detect(PMUFrame OneFrame)
        {
            if (OneFrame == null)
                return;
            _LatestPMUTime = OneFrame.TimeStamp;
            //check & update oscillation flags
            if (_OsciDetected &&OneFrame.TimeStamp.CompareTo(_OsciFlagExpireTime)>=0)
            {
                if(_PlotOsci)
                    PlotDataInBuffer(_OsciDetectedTime);
                _OsciDetected = false;
                _OsciFlagExpireTime = DateTime.MinValue;
                _OsciDetectedTime = DateTime.MinValue;
                
            }
            //calculate relative
            if(_SysCalculator==null)
            {
                _SysCalculator = new SystemMedianCalculator();
            }
            PMUMeasurement sys = _SysCalculator.CalculateSystemMedian(OneFrame);
            if (sys == null) return;
            OneFrame.SysMedian = sys;
            OneFrame.Measurements.Add(sys);
            //calculate average system median frequency
            _BufferToAve.Push(new TimeStampedValue(sys.TimeStamp, sys.Frequency));
            _AveFreq = _BufferToAve.AverageValue;
            _FramesBuffer.Enqueue(OneFrame);
            if (_FramesBuffer.Count>0)
                _FramesBuffer.Peek().SysMedian.Frequency = _AveFreq;
            
            if (_FramesBuffer.Count < _FrameBufferLength) return;
            OneFrame = _FramesBuffer.Dequeue();

            CalculateRelative(OneFrame.SysMedian, OneFrame);
            //buffer data
            foreach (var v in OneFrame.Measurements)
            {
                if (!_Buffers.ContainsKey(v.UnitID))
                {
                    _Buffers.Add(v.UnitID, new FNETTimeSeriesBuffer<PMUMeasurement>(_FramePerSecond, _DataWindowBeforeOsci_Sec + _DataWindowAfterOsci_Sec));
                    _Buffers[v.UnitID].BufferID = v.UnitID;
                    _Buffers[v.UnitID].BufferName = v.UnitName;
                }
                _Buffers[v.UnitID].Push(v);
            }
            //detection
            string key;
            int osciFlagCount = 0;
            foreach(var v in OneFrame.Measurements)
            {
                bool f = false;
                if (_EnableOsciDetect_Freq && !double.IsNaN(v.RelativeFreq))
                {
                    key = v.UnitID.ToString() + "_RelFreq";
                    if (!_OsciDetectorDic.ContainsKey(key))
                        _OsciDetectorDic.Add(key, new OsciDetector(_FramePerSecond, _WindowLength_Step1_Sec, _WindowLength_Step2_Sec, _Freq_Step1TH, _Freq_Step2TH, v.UnitID, key));
                    f = _OsciDetectorDic[key].Detect(new TimeStampedValue(v.TimeStamp, v.RelativeFreq));
                    if (f)
                        osciFlagCount += 1;
                }
                if (_EnableOsciDetect_Ang && (!double.IsNaN(v.RelativeAngle)))
                {
                    key = v.UnitID.ToString() + "_RelAng";
                    if (!_OsciDetectorDic.ContainsKey(key))
                        _OsciDetectorDic.Add(key, new OsciDetector(_FramePerSecond, _WindowLength_Step1_Sec, _WindowLength_Step2_Sec, _Ang_Step1TH, _Ang_Step2TH, v.UnitID, key));
                    f = _OsciDetectorDic[key].Detect(new TimeStampedValue(v.TimeStamp, v.RelativeAngle));
                    if (f)
                        osciFlagCount += 1;
                }                
            }
            //set oscillation flag
            if (osciFlagCount > 0 && !_OsciDetected)
            {
                OsciMitigatorObj.OsciFlag = true;
                _OsciDetected = true;
                _OsciDetectedTime = OneFrame.TimeStamp;
                _OsciFlagExpireTime = _OsciDetectedTime.AddSeconds(_DataWindowAfterOsci_Sec);
                _OsciLatestPMUTime = _LatestPMUTime;
                _OsciLatestLocalTime = DateTime.UtcNow;
            }
        }
        private void CalculateRelative(PMUMeasurement sys, PMUFrame OneFrame)
        {
            //_BufferToAve.Push(new TimeStampedValue(sys.TimeStamp, sys.Frequency));
            //_AveFreq = _BufferToAve.AverageValue;
            //_AveFreq = (_AveFreq * (_FreqAveBase - 1) + sys.Frequency) / _FreqAveBase;
            //sys.Frequency = _AveFreq;
            foreach (var v in OneFrame.Measurements)
            {
                v.RelativeFreq = v.Frequency - sys.Frequency;//  _AveFreq; //modified on 12.13.2022
                v.RelativeAngle = v.Angle - sys.Angle;
                if (!_PreviousMea.ContainsKey(v.UnitID)) _PreviousMea.Add(v.UnitID, v);
                v.RelativeAngle = v.RelativeAngle - Math.Round((v.RelativeAngle - _PreviousMea[v.UnitID].RelativeAngle) / 2 / Math.PI, 0) * 2 * Math.PI;
                _PreviousMea[v.UnitID] = v;
            }
        }
        private void PlotDataInBuffer(DateTime OsciTime)
        {
            YWPPlotCFG FreqPlot = new YWPPlotCFG("Frequency");
            YWPPlotCFG FreqPlot_Rel = new YWPPlotCFG("Freq-Variation");
            YWPPlotCFG AngPlot_Rel = new YWPPlotCFG("Ang-Rel");
            foreach(var v in _Buffers)
            {
                double iniAng = v.Value.First().RelativeAngle;
                foreach (var m in v.Value)
                    m.RelativeAng_Nom = (m.RelativeAngle - iniAng) / Math.PI * 180;
            }
            foreach(var v in _Buffers)
            {
                //FreqPlot.MultipleDataSeries.Add(new YWPTimeSeriesForPlot(v.Key.ToString() + "_F", v.Value, (x) => ((PMUMeasurement)x).Frequency));
                //FreqPlot_Rel.MultipleDataSeries.Add(new YWPTimeSeriesForPlot(v.Key.ToString() + "_FR", v.Value, (x) => ((PMUMeasurement)x).RelativeFreq));
                //AngPlot_Rel.MultipleDataSeries.Add(new YWPTimeSeriesForPlot(v.Key.ToString() + "_AR", v.Value, (x) => ((PMUMeasurement)x).RelativeAng_Nom));
                FreqPlot.MultipleDataSeries.Add(new YWPTimeSeriesForPlot(v.Value.BufferName + "_F", v.Value, (x) => ((PMUMeasurement)x).Frequency));
                FreqPlot_Rel.MultipleDataSeries.Add(new YWPTimeSeriesForPlot(v.Value.BufferName + "_FR", v.Value, (x) => ((PMUMeasurement)x).RelativeFreq));
                AngPlot_Rel.MultipleDataSeries.Add(new YWPTimeSeriesForPlot(v.Value.BufferName + "_AR", v.Value, (x) => ((PMUMeasurement)x).RelativeAng_Nom));
            }
            List<YWPPlotCFG> TimeSeriesGroup = new List<YWPPlotCFG>();
            TimeSeriesGroup.Add(FreqPlot);
            TimeSeriesGroup.Add(FreqPlot_Rel);
            TimeSeriesGroup.Add(AngPlot_Rel);
            string title = string.Format("Oscillation timestamp is {0}, detected at {1}, local time {2}", OsciTime.ToString("HH:mm:ss.fff"), _OsciLatestPMUTime.ToString("HH:mm:ss.fff"), _OsciLatestLocalTime.ToString("HH:mm:ss.fff"));
            if (!System.IO.Directory.Exists(OsciDetectionFolder))
                System.IO.Directory.CreateDirectory(OsciDetectionFolder);
            string folder = OsciDetectionFolder + OsciTime.ToString("yyyy-MM-dd") + "\\";
            if (!System.IO.Directory.Exists(folder))
                System.IO.Directory.CreateDirectory(folder);
            FNETTimeSeriesPlotter.CreateMultiPlotImg(folder + "Osci_" + OsciTime.ToString("yyyy-MM-ddTHH.mm.ss") + ".png", title, TimeSeriesGroup, 1200, 600, OsciTime, OsciTime.AddSeconds(-1 * _DataWindowBeforeOsci_Sec), null);

        }
 
    }
    public class SystemMedianCalculator
    {
        private Dictionary<int, PMUMeasurement> previousMea = new Dictionary<int, PMUMeasurement>();
        private Dictionary<int, PMUMeasurement> thisMea = new Dictionary<int, PMUMeasurement>();
        private DateTime preTimeStamp = DateTime.MinValue;
        private double systemAng = 0;
        private bool _EnableFreqSmooth = true;
        public PMUMeasurement CalculateSystemMedian(PMUFrame OneFrame)
        {
            if (preTimeStamp == DateTime.MinValue)
                preTimeStamp = OneFrame.TimeStamp;
            PMUMeasurement sys = new PMUMeasurement(1, OneFrame.TimeStamp, "SysMedian", false);
            List<double> dAngList = new List<double>();
            List<double> freqList = new List<double>();
            thisMea.Clear();
            foreach (var v in OneFrame.Measurements)
            {
                thisMea.Add(v.UnitID, v);
                freqList.Add(v.Frequency);
                if (!previousMea.ContainsKey(v.UnitID))
                    previousMea.Add(v.UnitID, v);
                if (previousMea[v.UnitID].TimeStamp != preTimeStamp || double.IsNaN(v.Angle) || double.IsNaN(v.Frequency))
                    continue;
                double dAng = v.Angle - previousMea[v.UnitID].Angle;
                dAng = dAng - Math.Round(dAng / 2 / Math.PI, 0) * Math.PI * 2;
                if (!double.IsNaN(dAng) && Math.Abs(dAng) < 1)
                    dAngList.Add(dAng);
            }
            if (freqList.Count > 0)
            {
                freqList.Sort();
                if (freqList.Count > 4)
                    sys.Frequency = (float)(freqList[freqList.Count / 2]);
                else
                    sys.Frequency = freqList.Average();
            }
            double dSysAng = 0;
            if (dAngList.Count > 0)
            {
                dAngList.Sort();
                if (dAngList.Count > 8)
                    dAngList = dAngList.GetRange(dAngList.Count / 2 - 3, 6);
                dSysAng = dAngList.Average();
            }
            systemAng += dSysAng;
            if (systemAng < 0)
                systemAng += Math.PI * 2;
            if (systemAng >= Math.PI * 2)
                systemAng -= Math.PI * 2;
            sys.Angle = (float)systemAng;
            //calculate relative angle
            foreach (var v in OneFrame.Measurements)
            {
                if (_EnableFreqSmooth)
                    v.Frequency = previousMea[v.UnitID].Frequency * 2 / 3 + v.Frequency / 3;
                //double dAng = v.Angle - previousMea[v.FDRID].Angle;
                //if (dAng > 3.14) dAng -= Math.PI * 2;
                //if (dAng < -3.14) dAng += Math.PI * 2;
                //if (Math.Abs(dAng) < 1.2)
                //v.RelativeAngle = (float)(v.Angle - systemAng);
                //else
                //    v.RelativeAngle = previousMea[v.FDRID].RelativeAngle;
                previousMea[v.UnitID] = v;
            }
            preTimeStamp = OneFrame.TimeStamp;
            return sys;
        }
    }

}
