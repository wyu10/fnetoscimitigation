﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace FNETOsciMitigation
{
    class HistoricalDataStreamer
    {
        
        string HistoricalDataFile = "FieldData.txt";
        public int LatencyToMimic_inMS = 100;
        Thread ExeThread;
        List<List<PMUMeasurement>> Frames = new List<List<PMUMeasurement>>();
        OsciMitigator parentAdaptor;
        public HistoricalDataStreamer(OsciMitigator adaptor,int latency_ms = 100)
        {
            parentAdaptor = adaptor;
            LatencyToMimic_inMS = latency_ms;
            parentAdaptor.SendGUIMsg("Field data retriever is initialized.");
        }
        public void StartStreaming()
        {
            ExeThread = new Thread(new ThreadStart(ExeStream));
            ExeThread.Start();
        }
        public void ExeStream()
        {
            LoadData();
            while(true)
                StreamData();
        }
        private void StreamData()
        {
            if (Frames == null || Frames.Count == 0|| Frames[0].Count ==0) return;
            DateTime FirstFrameTime = Frames[0][0].TimeStamp;
            DateTime UTCNow = DateTime.UtcNow;
            DateTime MimicStartTimestamp = DateTime.ParseExact(UTCNow.AddSeconds(5).ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", null);  // remove fractional seconds.
            double TimeOffset = MimicStartTimestamp.Subtract(FirstFrameTime).TotalSeconds;  //offset to map the time to current time.
            parentAdaptor.SendGUIMsg("Start to stream loaded data to controller...");
            for (int i=0;i< Frames.Count;i++)
            {
                List<PMUMeasurement> OneFrame = Frames[i];
                if (OneFrame.Count == 0) continue;
                foreach (var m in OneFrame)
                    m.TimeStamp = m.TimeStamp.AddSeconds(TimeOffset);
                //DateTime timeToSend = OneFrame[0].TimeStamp.AddMilliseconds(LatencyToMimic_inMS);
                DateTime timeToSend = OneFrame[0].TimeStamp.AddMilliseconds(OneFrame[0].ArtificialLatency_MS);
                while (DateTime.UtcNow.CompareTo(timeToSend) < 0) Thread.Sleep(5);
                parentAdaptor.DataReceived(OneFrame);
            }
            parentAdaptor.SendGUIMsg("Data streaming is done. May start next round...");
        }
        private void LoadData()
        {
            Frames.Clear();
            parentAdaptor.SendGUIMsg("Start to read field data file.");
            DateTime TimeOfCurrentFrame = DateTime.MinValue;
            StreamReader sr = new StreamReader(HistoricalDataFile);
            List<PMUMeasurement> MeaList = new List<PMUMeasurement>();
            int PreviousLatency = 0;
            int records = 0;
            while (!sr.EndOfStream)
            {   //                  0                   1             2   3        4     5         6         7                   8 Specified latency
                //[04/29/2022 10:35:17.605]\t2022-04-29 08:35:17.560,13,BRIN#348,True,50.004002,0.000000,2022-04-29 08:35:17.605,300
                string oneline = "";
                try
                {
                    oneline = sr.ReadLine().Trim();
                    if (oneline == null || oneline.Length < 10) continue;
                    oneline = oneline.Replace("\"", "");
                    string[] arr = oneline.Split(new char[] { '\t', ',' });
                    if (arr.Length < 7) continue;
                    int unitID = Convert.ToInt32(arr[2]);
                    DateTime ts = DateTime.ParseExact(arr[1], "yyyy-MM-dd HH:mm:ss.fff", null);
                    string unitName = arr[3];
                    bool isBackup = Convert.ToBoolean(arr[4]);
                    double Freq = Convert.ToDouble(arr[5]);
                    PMUMeasurement mea = new PMUMeasurement(unitID, ts, unitName, isBackup);
                    mea.Frequency = Freq;
                    if (arr.Length >= 9 && arr[8].Length > 0)
                    {
                        try { PreviousLatency = Convert.ToInt32(arr[8]); } catch { }
                    }
                    mea.ArtificialLatency_MS = PreviousLatency;
                    if (ts.CompareTo(TimeOfCurrentFrame) != 0)
                    {
                        MeaList = new List<PMUMeasurement>();
                        Frames.Add(MeaList);
                        TimeOfCurrentFrame = ts;
                    }
                    MeaList.Add(mea);
                    records++;
                }
                catch (Exception e) { parentAdaptor.SendGUIMsg(string.Format("Exception happened when read data. Msg:{0}", e.Message)); }
            }
            sr.Close();
            parentAdaptor.SendGUIMsg(string.Format("Field data is loaded, {0} frames with {1} values are loaded.", Frames.Count, records));
        }

    }


    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            OsciMitigator wadc = new OsciMitigator();
            HistoricalDataStreamer HisDataStreamer = new HistoricalDataStreamer(wadc);
            HisDataStreamer.StartStreaming();
        }
    }
}
