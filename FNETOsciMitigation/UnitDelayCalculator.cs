﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;


// This code is for WADC command calculation
// Note: PMU_In_Use 0 - primary, 1 - backup, 2 - both fail, 3 - start receive PMU measurement but not enough for calculation, 4 - No PMU measurements received yet, -1 - WADC re-initilize after chunk data loss > 20s

namespace FNETOsciMitigation
{
    public class DelayController //: IController
    {
        // Initilization
        private Dictionary<PMU, List<double>> FreqDiffsDict = new Dictionary<PMU, List<double>>();
        private Dictionary<PMU, List<double>> FreqDiffsWashedDict = new Dictionary<PMU, List<double>>();

        private List<int> DecisionLogs = new List<int>();
        private Dictionary<PMU, SortedList<DateTime, PMUMeasurement>> PMUMesDict = new Dictionary<PMU, SortedList<DateTime, PMUMeasurement>>();
        private bool backupEnabled = false;
        private List<int> PrimaryResponseLog = new List<int>();
        private Dictionary<PMU, List<double>> TFWashoutStatesDict = new Dictionary<PMU, List<double>>();

        private Dictionary<PMU, List<double>> TF1StatesDict = new Dictionary<PMU, List<double>>();
        private Dictionary<PMU, List<double>> TF2StatesDict = new Dictionary<PMU, List<double>>();
        private Dictionary<PMU, List<double>> TF6StatesDict = new Dictionary<PMU, List<double>>();
        private Dictionary<PMU, List<double>> TF7StatesDict = new Dictionary<PMU, List<double>>();
        private Dictionary<PMU, List<double>> TFDelay1StatesDict = new Dictionary<PMU, List<double>>();
        private Dictionary<PMU, List<double>> TFDelay2StatesDict = new Dictionary<PMU, List<double>>();
        private Dictionary<PMU, List<double>> TFDelay3StatesDict = new Dictionary<PMU, List<double>>();

        private Dictionary<PMU, List<double>> SmoothStatesDict = new Dictionary<PMU, List<double>>();
        private Dictionary<PMU, List<double>> OscStatesDict = new Dictionary<PMU, List<double>>();
        private List<double> CMDList = new List<double>(new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 });
        private Dictionary<PMU, List<double>> CMDSmoothStatesDict = new Dictionary<PMU, List<double>>();

        // Paramter default values, parameters will be updated by reading PARAMETER.txt in openPDC root folder
        private double bufferTime = 0.2;
        private double OscDectThreshold = 0.0012;
        private double supervisoryControlPosThresh = 0.5;
        private double supervisoryControlNegThresh = -0.3;
        private double limiter = 0.1;
        private List<double> FileParams = new List<double>();
        private DateTime currentRcvTime = DateTime.UtcNow;

        /// <summary>
        /// All measurement will be saved into this queue firstly.
        /// </summary>
        private Queue<PMUMeasurement> MeaToBeProcessed = new Queue<PMUMeasurement>();
        private Thread ThreadForController;
        private DateTime NextExecuteTime = DateTime.MinValue;
        /// <summary>
        /// This is for excute WADC command calculation every 0.1s
        /// </summary>
        private int ExeIntervalInMS = 100;  //unit ms.

        public bool disconnectErrorFlag = false;

        //===========================Logging==========================
        public void Log(string logMessage)
        {
            ControllerLoger.WriteLog(logMessage);
        }
        //============================================================
        //Added by Wayne
        public event SendControlCMD OnControlCMDGenerated;  //handler for sending command
        public event SendGUIMsg OnSendGUIMsg;
        public static FNETLog ControllerLoger = new FNETLog("Log\\", new FNETLogTypeCFG("ControllerLog", "", true, true, FNETLogTypeCFG.ExpireType.Daily));
        public List<byte> DACChannelIDs;    //ID of controlled DAC channel
        public double NominalFreq = 50;    //nominal frequency
        public OsciMitigator OsciMitigatorObj = null;
        private bool Stopping = false;
        // This function is for sending out WADC command
        private void SendCMD(double y)
        {
            List<DACSet> settings = new List<DACSet>();
            foreach (byte dacid in DACChannelIDs)
                settings.Add(new DACSet(dacid, (int)(1000 * y), 0));      //the set value should be 'int'. Please check DAC channel ID and Delay. 
            DateTime timestamp = DateTime.UtcNow;                       //Shall use UTCNow as timestamp?
            OnControlCMDGenerated?.Invoke(timestamp, settings);
        }
        // Use this function to send values to display on GUI
        private void SendGUIMsg(string MsgName, string MsgValue)
        {
            OnSendGUIMsg?.Invoke(MsgName, MsgValue);
        }
        public void DisposeController()
        {
            Log($"DisposeController");
            Stopping = true;
            Thread.Sleep(300);
            OnControlCMDGenerated = null;
            try
            {
                ThreadForController.Abort();
            }
            catch { }
        }
        //============================================================
        
        // This function is for clear out saved old values by newest values in lists
        private List<T> ListRecycleAdd<T>(List<T> ori, T newitem, int length_limit)
        {
            List<T> newList = new List<T>();
            if (ori.Count >= length_limit)
            {
                for (int i = 1; i < ori.Count(); i++)
                {
                    newList.Add(ori[i]);
                }
                newList.Add(newitem);
            }
            else
            {
                for (int i = 0; i < ori.Count(); i++)
                {
                    newList.Add(ori[i]);
                }
                newList.Add(newitem);
            }
            return newList;
        }

        private SortedList<DateTime, PMUMeasurement> SortedListRecycleAdd(SortedList<DateTime, PMUMeasurement> ori, DateTime newkey, PMUMeasurement newitem)
        {
            SortedList<DateTime, PMUMeasurement> newList = new SortedList<DateTime, PMUMeasurement>();
            int i = 0;
            foreach (var kvpair in ori)
            {
                if (i < 1)
                {
                    i += 1;
                    continue;
                }
                else
                {
                    newList.Add(kvpair.Key, kvpair.Value);
                    i += 1;
                }
            }
            newList.Add(newkey, newitem);
            return newList;
        }

        // This function is for reading the parameters from PARAMETER.txt file
        public void ReadParamFromText(string FilePath)
        {
            string[] lines = System.IO.File.ReadAllLines(@FilePath);
            while (FileParams.Count < lines.Length) FileParams.Add(0);
            int skipLines = 4;
            for (int i = skipLines; i < lines.Length; i++)
                try { FileParams[i - skipLines] = Convert.ToDouble(lines[i].Trim()); } catch { }

            // TERNA Feild Test Version -- 20 parameters
            OscDectThreshold = FileParams[16];
            bufferTime = FileParams[17];
            supervisoryControlPosThresh = FileParams[18];
            supervisoryControlNegThresh = FileParams[19];
            limiter = FileParams[20];
            // Log($"Read params from file {FilePath}, added {FileParams.Count()} : {String.Join(",", FileParams)}");
        }

        /// <summary>
        /// Initial the controller.
        /// Initial the thread to execute the algorithm.
        /// </summary>
        public DelayController(OsciMitigator osciMitigator)
        {
            //if (ControllerLoger == null)
            //    ControllerLoger = new FNETLog("Log\\", new FNETLogTypeCFG("ControllerLog", "", true, true, FNETLogTypeCFG.ExpireType.Daily));
            OsciMitigatorObj = osciMitigator;
            ThreadForController = new Thread(new ThreadStart(RunControllerAlgorithm));
            ThreadForController.Start();
        }
        /// <summary>
        /// Execute the controller algorithm in a thread.
        /// The reason of using 'Thread' other than a 'Timer' is to avoid the impact of the algorithm running time, which could be accumulated.
        /// Using Timer, the execute time could be: 0.00-> 0.110->0.218->0.335->0.450->0.570 ....
        /// </summary>
        private void RunControllerAlgorithm()
        {
            while (true)
            {
                if (Stopping == true)
                    break;
                if (DateTime.UtcNow.CompareTo(NextExecuteTime) < 0)
                {
                    Thread.Sleep(5); continue;
                }
                try
                {
                    NextExecuteTime = GetNextExeTime();
                    List<PMUMeasurement> meaList = new List<PMUMeasurement>();
                    lock (MeaToBeProcessed)
                    {
                        while (MeaToBeProcessed.Count > 0)
                            meaList.Add(MeaToBeProcessed.Dequeue());
                    }
                    //Exe the algorithm
                    PublishMeasurementsInternal(meaList);
                }
                catch (Exception e)
                {
                    Log(string.Format("Exception was caught in controller algorithm. Msg:{0}\r\nStack:{1}", e.Message, e.StackTrace));
                }
            }
        }
        /// <summary>
        /// Obtain the expected time of next execution.
        /// Supposing interval is 100ms,
        /// 0.110->0.200, 0.190->0.200, 0.920->1.000.
        /// Purpose is to avoid the accumulation of running time of the algorithm, so that the start time will be always xx.x00 second.
        /// </summary>
        /// <returns></returns>
        private DateTime GetNextExeTime()
        {
            DateTime nextTime = DateTime.UtcNow.AddMilliseconds(ExeIntervalInMS);
            int msToSubtract = nextTime.Millisecond % ExeIntervalInMS;
            nextTime = nextTime.AddMilliseconds(-1 * msToSubtract);
            return nextTime;
        }
        //============================================================
        /// <summary>
        /// This function will be called by openPDC->adaptor->..
        /// </summary>
        /// <param name="ReceivedMea"></param>
        public void PublishMeasurements(List<PMUMeasurement> ReceivedMea)
        {
            if (ReceivedMea == null) return;
            //------------------------------------------------------------------
            //do some processing which should be executed IMMEDIATELY, if any.

            //------------------------------------------------------------------
            //buffer the data
            lock (MeaToBeProcessed)
            {
                foreach (var v in ReceivedMea)
                {
                    // filter out random bad data
                    if (v.Frequency >= (NominalFreq + 0.5) && v.Frequency <= (NominalFreq - 0.5))
                    {
                        continue;
                    }
                    MeaToBeProcessed.Enqueue(v);
                }
            }
        }
        
        // This function is to find which PMU channel is the current using channel based on the decision 
        private PMU decision2PMU(int decision_)
        {
            bool usingBackup = false;
            int decision = decision_;

            Log($"decision {decision} before reinit if {disconnectErrorFlag}, {DecisionLogs.Count()}");

            if (decision == 2)
            {
                for (int i = DecisionLogs.Count() - 1; i >= 0; i--)
                {
                    int d = DecisionLogs[i];
                    if (d != 2 && d != 3 && d != 4)
                    {
                        decision = d;
                        break;
                    }
                }
            }

            // if decision is always 2 backwards 200 steps: raise error -- should re-initialize
            if (decision == 2 && DecisionLogs.Count() >= 200)
            {
                Log($"decision 2 reinit {disconnectErrorFlag}");
                disconnectErrorFlag = true;
                Log($"decision 2 reinit {disconnectErrorFlag}");

                return null;
            }

            if (decision == 1)
            {
                usingBackup = true;
            }
            foreach (var entry in PMUMesDict)
            {
                PMU unit = entry.Key;
                if (unit.isBackup == usingBackup)
                {
                    return unit;
                }
            }
            return null;
        }


        private void PublishMeasurementsInternal(List<PMUMeasurement> ReceivedMea)
        {

            Log($"Received {ReceivedMea.Count()} measurements");
            currentRcvTime = DateTime.UtcNow;

            // store PMU and their measurements
            foreach (var m in ReceivedMea)
            {
                // Log($"Loading PMU {m.pmu.UnitName} {m.pmu.UnitID}");
                if (!PMUMesDict.ContainsKey(m.pmu))
                {
                    PMUMesDict[m.pmu] = new SortedList<DateTime, PMUMeasurement>();
                }
                if (PMUMesDict[m.pmu].Count() >= 500)
                {
                    PMUMesDict[m.pmu] = SortedListRecycleAdd(PMUMesDict[m.pmu], m.TimeStamp, m);
                }
                else
                {
                    PMUMesDict[m.pmu].Add(m.TimeStamp, m);
                }

                // initialize containers
                if (!FreqDiffsDict.ContainsKey(m.pmu))
                {
                    FreqDiffsDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0, 0.0, 0.0 });
                }
                if (!FreqDiffsWashedDict.ContainsKey(m.pmu))
                {
                    FreqDiffsWashedDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0, 0.0, 0.0 });
                }
                if (!TFWashoutStatesDict.ContainsKey(m.pmu))
                {
                    TFWashoutStatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0 });
                }
                if (!TF1StatesDict.ContainsKey(m.pmu))
                {
                    TF1StatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0 });
                }
                if (!TF2StatesDict.ContainsKey(m.pmu))
                {
                    TF2StatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0 });
                }
                if (!TF6StatesDict.ContainsKey(m.pmu))
                {
                    TF6StatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0 });
                }
                if (!TF7StatesDict.ContainsKey(m.pmu))
                {
                    TF7StatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0 });
                }
                if (!TFDelay1StatesDict.ContainsKey(m.pmu))
                {
                    TFDelay1StatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0 });
                }
                if (!TFDelay2StatesDict.ContainsKey(m.pmu))
                {
                    TFDelay2StatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0 });
                }
                if (!TFDelay3StatesDict.ContainsKey(m.pmu))
                {
                    TFDelay3StatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0 });
                }
                if (!SmoothStatesDict.ContainsKey(m.pmu))
                {
                    SmoothStatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0 });
                }
                if (!OscStatesDict.ContainsKey(m.pmu))
                {
                    OscStatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0 });
                }
                if (!CMDSmoothStatesDict.ContainsKey(m.pmu))
                {
                    CMDSmoothStatesDict[m.pmu] = new List<double>(new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 });
                }
            }

            int decision = MakeDecision();
            // Log($"Decision made : {decision}, calculating frequency difference in CalcFreqDiff");
            DecisionLogs = ListRecycleAdd(DecisionLogs, decision, 500);
            // Log($"DecisionLogs length - {DecisionLogs.Count()}");
            if (decision == 3 || decision == 4)
            {
                // not enough data
                Log($"command y - {12}");
                SendGUIMsg("PMU_In_Use", decision.ToString());
                SendCMD(12);
                Log("============================================================");
                return;
            }

            Dictionary<PMU, double> cmdPMUDict = new Dictionary<PMU, double>();
            Dictionary<PMU, double> CurrentSmoothStatePMUDict = new Dictionary<PMU, double>();

            foreach (var entry in PMUMesDict)
            {
                PMU currPMU = entry.Key;

                double freqDiffs_Original = CalcFreqDiff(currPMU, decision);
                FreqDiffsDict[currPMU] = ListRecycleAdd(FreqDiffsDict[currPMU], freqDiffs_Original, 500);

                double freqDiffWashed = TFWashout(freqDiffs_Original, currPMU);
                FreqDiffsWashedDict[currPMU] = ListRecycleAdd(FreqDiffsWashedDict[currPMU], freqDiffWashed, 500);


                double Osc = 0.0;
                if (OsciMitigatorObj.OsciDetectEnable)
                {
                    bool oscmonitorFlag = OsciMitigatorObj.OsciFlag;
                    if (oscmonitorFlag)
                    {
                        Osc = 1.0;
                    }
                    else
                    {
                        Osc = 0.0;
                    }
                }
                else
                {
                    Osc = CheckOscillation(FreqDiffsWashedDict[currPMU]);
                }
                OscStatesDict[currPMU] = ListRecycleAdd(OscStatesDict[currPMU], Osc, OscStatesDict[currPMU].Count());

                double CurrentSmoothState = TFSmoothing(Osc, currPMU);
                CurrentSmoothStatePMUDict.Add(currPMU, CurrentSmoothState);

                double TF1y = TF1(currPMU);
                double TF2y = TF2(TF1y, currPMU);
                double TF6y = TF6(TF2y, currPMU);
                double TF7y = TF7(TF6y, currPMU);
                double TFDelay1y = TFDelay1(TF7y, currPMU);
                double TFDelay2y = TFDelay2(TFDelay1y, currPMU);
                double TFDelay3y = TFDelay3(TFDelay2y, currPMU);

                double cmd = TFcmdSmoothing(TFDelay3y, currPMU);

                if (cmd >= limiter)
                {
                    cmd = limiter;
                }
                else if (cmd <= -limiter)
                {
                    cmd = -limiter;
                }

                cmdPMUDict.Add(currPMU, cmd);
            }

            PMU chosenPMU = decision2PMU(decision);

            if (chosenPMU == null || disconnectErrorFlag == true)
            {
                Log($"Command y - disconnect error or null return, triggering default cmd and null decision");
                SendGUIMsg("PMU_In_Use", "-1");
                SendGUIMsg("Osc_Detection", "null");
                SendCMD(12);
                Log("============================================================");
                return;
            }

            double chosenCMD = cmdPMUDict[chosenPMU];
            double chosenSmoothState = CurrentSmoothStatePMUDict[chosenPMU];
            double chosenOsc = OscStatesDict[chosenPMU][OscStatesDict[chosenPMU].Count() - 1];
            CMDList = ListRecycleAdd(CMDList, chosenCMD, CMDList.Count());

            double CMD = (chosenCMD * chosenSmoothState) * 80 + 12;

            Log($"Command y - {CMD} {chosenOsc} {chosenSmoothState}");
            SendGUIMsg("PMU_In_Use", decision.ToString());
            SendGUIMsg("Osc_Detection", chosenOsc.ToString());
            SendCMD(CMD);
            Log("============================================================");

        }

        // This transfer function is to smoothing the output of Osc Detector, so no sharp jump from 0 to 1 or 1 to 0
        private double TFSmoothing(double ipt, PMU currPMU)
        {
            double y = ipt * 0.09091 + OscStatesDict[currPMU][OscStatesDict[currPMU].Count() - 2] * 0.09091 + SmoothStatesDict[currPMU][SmoothStatesDict[currPMU].Count() - 1] * 0.8182;
            SmoothStatesDict[currPMU] = ListRecycleAdd(SmoothStatesDict[currPMU], y, SmoothStatesDict[currPMU].Count());
            return y;
        }

        // Washout transfer function
        private double TFWashout(double ipt, PMU currPMU)
        {
            double y = ipt * 0.995 - FreqDiffsDict[currPMU][FreqDiffsDict[currPMU].Count() - 2] * 0.995 + TFWashoutStatesDict[currPMU][TFWashoutStatesDict[currPMU].Count() - 1] * 0.99;
            TFWashoutStatesDict[currPMU] = ListRecycleAdd(TFWashoutStatesDict[currPMU], y, TFWashoutStatesDict[currPMU].Count());
            return y;
        }

        private double TF1Helper(double freqdiff)
        {
            return (freqdiff / NominalFreq) * FileParams[0];
        }
        private double TF1(PMU currPMU)
        {
            // Filter
            double y = TF1Helper(FreqDiffsDict[currPMU][FreqDiffsDict[currPMU].Count() - 1]) * FileParams[1] +
                TF1Helper(FreqDiffsDict[currPMU][FreqDiffsDict[currPMU].Count() - 2]) * FileParams[2] +
                TF1Helper(FreqDiffsDict[currPMU][FreqDiffsDict[currPMU].Count() - 3]) * FileParams[3] +
                TF1StatesDict[currPMU][TF1StatesDict[currPMU].Count() - 1] * FileParams[4] +
                TF1StatesDict[currPMU][TF1StatesDict[currPMU].Count() - 2] * FileParams[5];

            TF1StatesDict[currPMU] = ListRecycleAdd(TF1StatesDict[currPMU], y, TF1StatesDict[currPMU].Count());
            return y;
        }

        private double TF2(double ipt, PMU currPMU)
        {
            // Washout
            double y = ipt * FileParams[6] + TF1StatesDict[currPMU][TF1StatesDict[currPMU].Count() - 2] * FileParams[7] + TF2StatesDict[currPMU][TF2StatesDict[currPMU].Count() - 1] * FileParams[8];
            TF2StatesDict[currPMU] = ListRecycleAdd(TF2StatesDict[currPMU], y, TF2StatesDict[currPMU].Count());
            return y;
        }


        private double TF6(double ipt, PMU currPMU)
        {
            // lead lag 1
            double y = ipt * FileParams[9] + TF2StatesDict[currPMU][TF2StatesDict[currPMU].Count() - 2] * FileParams[10] + TF6StatesDict[currPMU][TF6StatesDict[currPMU].Count() - 1] * FileParams[11];
            TF6StatesDict[currPMU] = ListRecycleAdd(TF6StatesDict[currPMU], y, TF6StatesDict[currPMU].Count());
            return y;
        }

        private double TF7(double ipt, PMU currPMU)
        {
            // lead lag 2
            double y = ipt * FileParams[9] + TF6StatesDict[currPMU][TF6StatesDict[currPMU].Count() - 2] * FileParams[10] + TF7StatesDict[currPMU][TF7StatesDict[currPMU].Count() - 1] * FileParams[11];
            TF7StatesDict[currPMU] = ListRecycleAdd(TF7StatesDict[currPMU], y, TF7StatesDict[currPMU].Count());
            return y;
        }

        private double TFDelay1(double ipt, PMU currPMU)
        {
            // Delay Gain + delay 1
            double y = FileParams[12] * (ipt * FileParams[13] + TF7StatesDict[currPMU][TF7StatesDict[currPMU].Count() - 2] * FileParams[14]) + TFDelay1StatesDict[currPMU][TFDelay1StatesDict[currPMU].Count() - 1] * FileParams[15];
            TFDelay1StatesDict[currPMU] = ListRecycleAdd(TFDelay1StatesDict[currPMU], y, TFDelay1StatesDict[currPMU].Count());
            return y;
        }

        private double TFDelay2(double ipt, PMU currPMU)
        {
            // delay 2
            double y = ipt * FileParams[13] + TFDelay1StatesDict[currPMU][TFDelay1StatesDict[currPMU].Count() - 2] * FileParams[14] + TFDelay2StatesDict[currPMU][TFDelay2StatesDict[currPMU].Count() - 1] * FileParams[15];
            TFDelay2StatesDict[currPMU] = ListRecycleAdd(TFDelay2StatesDict[currPMU], y, TFDelay2StatesDict[currPMU].Count());
            return y;
        }

        private double TFDelay3(double ipt, PMU currPMU)
        {
            // delay 3
            double y = ipt * FileParams[13] + TFDelay2StatesDict[currPMU][TFDelay2StatesDict[currPMU].Count() - 2] * FileParams[14] + TFDelay3StatesDict[currPMU][TFDelay3StatesDict[currPMU].Count() - 1] * FileParams[15];
            TFDelay3StatesDict[currPMU] = ListRecycleAdd(TFDelay3StatesDict[currPMU], y, TFDelay3StatesDict[currPMU].Count());
            return y;
        }

        // CMD Smoothing Transfer Function
        private double TFcmdSmoothing(double ipt, PMU currPMU)
        {
            double y = ipt * 0.8046 + CMDSmoothStatesDict[currPMU][CMDSmoothStatesDict[currPMU].Count() - 1] * 0.1353;
            CMDSmoothStatesDict[currPMU] = ListRecycleAdd(CMDSmoothStatesDict[currPMU], y, CMDSmoothStatesDict[currPMU].Count());
            return y;
        }

        // helper function, check if primary is stable when using backup PMU channel. If primary is stable for 5s, switch back to primary
        private bool CheckPrimaryStable()
        {
            if (PrimaryResponseLog.Count() >= 50)
            {
                Log($"PrimaryResponseLog >= 50 with length {PrimaryResponseLog.Count()}");
                List<int> PrimaryResponseLogLast50 = PrimaryResponseLog.Skip(Math.Max(0, PrimaryResponseLog.Count() - 50)).ToList();
                if (PrimaryResponseLogLast50.Sum() == 50)
                {
                    return true; // primary stable, use primary
                }
                else
                {
                    return false; // primary unstable, use backup
                }
            }
            else
            {
                return false; // not enough primary log, use backup
            }
        }
        
        // This function is for making the decion to use which PMU hannel. The decion is shown on GUI PMU_In_Use
        private int MakeDecision()
        {
            Dictionary<PMU, int> DelayCalcResponses = new Dictionary<PMU, int>();
            Log($"PMUMesDict length {PMUMesDict.Count()}");

            bool hasPrimary = false;
            bool hasBackup = false;
            foreach (var entry in PMUMesDict)
            {
                PMU unit = entry.Key;
                if (unit.isBackup)
                {
                    hasBackup = true;
                }
                if (!unit.isBackup)
                {
                    hasPrimary = true;
                }
            }
            if (hasPrimary == false && hasBackup == false)
            {
                Log($"Decision 4 - not enough PMUs, missing both primary and backup");
                return 4;
            }

            // flag check at least one PMU has enough data
            bool enoughData = false;

            foreach (var entry in PMUMesDict)
            {
                PMU unit = entry.Key;
                SortedList<DateTime, PMUMeasurement> UnitMes = entry.Value;
                Log($"PMU {unit} unitMes length {UnitMes.Count()}");

                if (UnitMes.Count() >= 150)
                {
                    enoughData = true;
                    // get last 150 time step measurements
                    List<PMUMeasurement> UnitMesLast150 = UnitMes.Values.ToList().Skip(Math.Max(0, UnitMes.Count() - 150)).ToList();
                    // get delay decisions
                    UnitDelayCalculator UnitDelayCalculator = new UnitDelayCalculator(unit.UnitID, UnitMesLast150, currentRcvTime, bufferTime);
                    int response = UnitDelayCalculator.DecideDelay(supervisoryControlPosThresh, supervisoryControlNegThresh);
                    Log($"Unit {unit.UnitID} delay response {response}");
                    DelayCalcResponses[unit] = response;
                }
            }

            if (enoughData == false)
            {
                // decision 3 - not enough data
                return 3;
            }

            bool allBackupOK = true;
            bool allPrimaryOK = true;
            int backupUnitCounter = 0;
            int primaryUnitCounter = 0;

            // check if all primary PMUs are ok
            // check if all backup PMUs are ok
            foreach (var entry in DelayCalcResponses)
            {
                PMU unit = entry.Key;
                int response = entry.Value;

                if (unit.isBackup == true && response == 1)
                {
                    allBackupOK = false;
                }

                if (unit.isBackup == false && response == 1)
                {
                    allPrimaryOK = false;
                }

                if (unit.isBackup == true)
                {
                    backupUnitCounter += 1;
                }
                if (unit.isBackup == false)
                {
                    primaryUnitCounter += 1;
                }
            }

            if (primaryUnitCounter == 0)
            {
                allPrimaryOK = false;
            }
            if (backupUnitCounter == 0)
            {
                allBackupOK = false;
            }

            // make decisions based on primary PMU status, and backup PMU status
            if (allPrimaryOK && allBackupOK)
            {
                Log("Primary and Backup all ok");
                if (backupEnabled)
                {
                    Log("Backup was enabled");
                    PrimaryResponseLog = ListRecycleAdd(PrimaryResponseLog, 1, 100); // log primary response
                    Log("PrimaryResponseLog added 1, checking primary stable");
                    if (CheckPrimaryStable())
                    {
                        Log("Primary stable");
                        backupEnabled = false;
                        PrimaryResponseLog.Clear();
                        Log($"PrimaryResponseLog cleared with length {PrimaryResponseLog.Count()}");
                        return 0; // primary ok, use primary
                    }
                    else
                    {
                        Log("Primary unstable, enabled backup");
                        return 1;
                    }
                }
                else
                {
                    Log("Backup wasnot enabled");
                    backupEnabled = false;
                    return 0; // not using backup, use primary
                }

            }
            else if (!allPrimaryOK && allBackupOK)
            {
                Log("Primary !ok and Backup ok");
                // switch to backup
                backupEnabled = true;
                Log("Switched to backup");
                return 1;
            }
            else if (allPrimaryOK && !allBackupOK)
            {
                Log("Primary ok and Backup !ok");
                if (backupEnabled)
                {
                    // Log("Backup was enabled");
                    PrimaryResponseLog = ListRecycleAdd(PrimaryResponseLog, 1, 100); // log primary response
                    // Log("PrimaryResponseLog added 1, checking primary stable");
                    if (CheckPrimaryStable())
                    {
                        // Log("Primary stable");
                        backupEnabled = false;
                        PrimaryResponseLog.Clear();
                        // Log($"PrimaryResponseLog cleared with length {PrimaryResponseLog.Count()}");
                        return 0; // primary ok, use primary
                    }
                    else
                    {
                        // Log("Primary unstable, still use backup");
                        return 1; // primary abit stable but not stable enough for 5s, still use backup
                    }
                }
                else
                {
                    // Log("Backup was not enabled");
                    backupEnabled = false;
                    return 0; // not using backup, use primary
                }
            }
            else
            {
                // Log("Primary and Backup both failed");
                backupEnabled = false;
                return 2; // both primary and backup failed
            }
        }
        private double Dt2Seconds(DateTime inputTime)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            DateTime targetTime = inputTime;
            TimeSpan result = targetTime.Subtract(dt);
            double seconds = Convert.ToDouble(result.TotalSeconds);
            return seconds;
        }
        
        /// <summary>
        ///  In this function, we first find the specific frequency measurement that has the delay most close to the buffer. 
        ///  Then we use that frequency minus nominal frequency to get the frequency difference. 
        ///  These frequency differences have 2 uses: 
        ///  1. Calculate the final commands
        ///  2. Going the the TFWashout transfer function. Then the washed frequency difference is going through the Osc Detector
        ///  In the situation that both PMU channels disconnected, we will keep using the last frequency measurement entry to calculate the frequency difference. Therefore the command will slowly back to 0.
        /// </summary>
        /// <param name="currPMU"></param>
        /// <param name="decision"></param>
        /// <returns></returns>
        private double CalcFreqDiff(PMU currPMU, int decision)
        {
            if (decision != 2)
            {
                List<double> selectedFreqs = new List<double>();

                foreach (var entry in PMUMesDict)
                {
                    PMU unit = entry.Key;
                    if (unit == currPMU)
                    {
                        SortedList<DateTime, PMUMeasurement> UnitMes = entry.Value;
                        List<PMUMeasurement> UnitMesLast150 = UnitMes.Values.ToList().Skip(Math.Max(0, UnitMes.Count() - 150)).ToList();
                        // Log($"{unit.UnitID} UnitMes last 150 elements generated");
                        double minDelay = double.PositiveInfinity;
                        double Frequency = 0.0;
                        foreach (var v in UnitMesLast150)
                        {
                            double delay = Dt2Seconds(currentRcvTime) - Dt2Seconds(v.TimeStamp);
                            if (Math.Abs(delay - bufferTime) < minDelay)
                            {
                                minDelay = Math.Abs(delay - bufferTime);
                                Frequency = v.Frequency;
                            }
                        }

                        selectedFreqs = ListRecycleAdd(selectedFreqs, Frequency, 500);
                    }

                }
                // Log($"CalcFreqDiff decison != 2 {selectedFreqs[selectedFreqs.Count() - 1] - NominalFreq}, decision = {decision}");
                return selectedFreqs[selectedFreqs.Count() - 1] - NominalFreq;
            }
            else
            {
                // Log($"CalcFreqDiff decison = 2 {FreqDiffsDict[currPMU][FreqDiffsDict[currPMU].Count() - 1]}, decision = {decision}");
                return FreqDiffsDict[currPMU][FreqDiffsDict[currPMU].Count() - 1];
            }
        }
        
        // this function is for checking if the frequency > Osc detector's threshold
        private double CheckOscillation(List<double> FreqDiffs)
        {
            // Log($"FreqDiffs with lenght {FreqDiffs.Count()}");
            if (FreqDiffs.Count() >= 50)
            {
                List<double> FreqDiffsLast50 = FreqDiffs.Skip(Math.Max(0, FreqDiffs.Count() - 50)).ToList();
                // Log($"Osc check {FreqDiffsLast50.Max()} - {FreqDiffsLast50.Min()}");
                if (FreqDiffsLast50.Max() > OscDectThreshold || FreqDiffsLast50.Min() < -OscDectThreshold)
                {
                    return 1.0;
                }
            }
            return 0.0;
        }
    }


    public class UnitDelayCalculator
    {
        public int UnitID;
        public List<PMUMeasurement> PMUMeasurements;
        private DateTime currentRcvTime;
        private double bufferTime;
        public void Log(string logMessage)
        {
            DelayController.ControllerLoger.WriteLog(logMessage);
        }
        public UnitDelayCalculator(int UnitID_, List<PMUMeasurement> PMUMeasurements_, DateTime currentRcvTime_, double bufferTime_)
        {
            UnitID = UnitID_;
            PMUMeasurements = PMUMeasurements_;
            currentRcvTime = currentRcvTime_;
            bufferTime = bufferTime_;
        }
        private double Dt2Seconds(DateTime inputTime)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            DateTime targetTime = inputTime;
            TimeSpan result = targetTime.Subtract(dt);
            double seconds = Convert.ToDouble(result.TotalSeconds);
            return seconds;
        }

        /// <summary>
        /// This function is the superviory control.
        /// If the PMU Channel has positive delay larger than positive delay threshold and negative delay less than negative delay threshold, the PMU channel cannot be used.
        /// </summary>
        public int DecideDelay(Double posThreshold, Double negThreshold)
        {
            // 1 - above threshold, not ok
            // 2 - below threshold, ok
            // 3 - Error input

            // check if all unit ids match
            foreach (var v in PMUMeasurements)
            {
                if (v.pmu.UnitID != UnitID)
                {
                    return 3;
                }
            }


            double posMinDelay = double.PositiveInfinity;
            double negMinDelay = 0.0;

            double latestTimeStamp = -double.PositiveInfinity;
            PMUMeasurement latestPMUMeas = null;
            foreach (var v in PMUMeasurements)
            {
                if (Dt2Seconds(v.TimeStamp) > latestTimeStamp)
                {
                    latestTimeStamp = Dt2Seconds(v.TimeStamp);
                    latestPMUMeas = v;
                }
                double delay = Dt2Seconds(currentRcvTime) - Dt2Seconds(v.TimeStamp);
                double delayminusbuffer = delay - bufferTime;
                if (delayminusbuffer >= 0)
                {
                    if (delayminusbuffer < posMinDelay)
                    {
                        posMinDelay = delayminusbuffer;
                    }
                }
                else
                {
                    if (delayminusbuffer < negMinDelay)
                    {
                        negMinDelay = delayminusbuffer;
                    }
                }
            }

            double latestdelay = Dt2Seconds(currentRcvTime) - Dt2Seconds(latestPMUMeas.TimeStamp);

            if (posMinDelay > posThreshold || negMinDelay < negThreshold)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }
    }
}