﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNETOsciMitigation
{
    public class OsciDetectionTERNA
    {
        public int FramePerSecond = 50;
        public int SubWindowLengthInSec = 5;
        public int SubWindowCount = 4;
        public double FreqP2PThreshold = 0.06;
        public int SubWindowTrueThreshold = 3;

        private DateTime latestTimestamp = DateTime.MinValue;
        //push to the last and pop the first
        private LinkedList<FreqWithTime> dataBuffer = new LinkedList<FreqWithTime>();
        private DateTime nextOsciDetectTime = DateTime.MinValue;
        private List<int> SubWindowFlags = new List<int>();
        private bool LastestOsciFlag = false;   //true: oscillation, false: no osci.
        public OsciDetectionTERNA()
        {
            //nextOsciDetectTime = DateTime.UtcNow.AddSeconds(SubWindowLengthInSec);  //what if local computer time is wrong?
        }
        public bool Detect(DateTime ts, double freq)
        {
            //---------------------------------------------------------------
            //push data to buffer
            FreqWithTime f = new FreqWithTime(ts, freq);
            latestTimestamp = ts;
            dataBuffer.AddLast(f);
            //---------------------------------------------------------------
            //remove old data
            DateTime expire = ts.AddSeconds(-1 * SubWindowLengthInSec);
            while(dataBuffer.Count>0)
            {
                if (dataBuffer.First.Value.Timestamp.CompareTo(expire) < 0 || dataBuffer.Count > FramePerSecond * SubWindowLengthInSec * 1.2)
                    dataBuffer.RemoveFirst();
                else
                    break;
            }
            //---------------------------------------------------------------
            //detect if oscillation occurs
            if(latestTimestamp.CompareTo(nextOsciDetectTime)>=0)
            {
                nextOsciDetectTime = latestTimestamp.AddSeconds(SubWindowLengthInSec);  //what if the timestamp is wrong?
                double minFreq = 100, maxFreq = 0;
                foreach(var v in dataBuffer)
                {
                    if (v.Frequency > maxFreq) maxFreq = v.Frequency;
                    if (v.Frequency < minFreq) minFreq = v.Frequency;
                }
                if (maxFreq - minFreq >= FreqP2PThreshold) 
                    SubWindowFlags.Add(1);
                else 
                    SubWindowFlags.Add(0);
                while (SubWindowFlags.Count > 4) 
                    SubWindowFlags.RemoveAt(0);
                LastestOsciFlag = (SubWindowFlags.Sum() >= SubWindowTrueThreshold);
                //---log---
                /*StringBuilder sb = new StringBuilder();                
                sb.Append(latestTimestamp.ToString("yyyy-MM-dd HH:mm:ss.fff" + ","));
                foreach (var v in SubWindowFlags) sb.Append(v.ToString());
                sb.Append("," + LastestOsciFlag.ToString()); */
            }
            return LastestOsciFlag;
        }
    }
    public class FreqWithTime
    {
        public double Frequency;
        public DateTime Timestamp;
        public FreqWithTime(DateTime ts, double freq)
        {
            Timestamp = ts;
            Frequency = freq;
        }
    }
}
