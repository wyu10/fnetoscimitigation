﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNETOsciMitigation
{
    public class OsciDetector
    {
        private int _UnitID;
        private string _Name;
        //parameters
        private int _FramePerSecond = 10;
        private double _WindowLength_Step1_Sec = 3;
        private double _WindowLength_Step2_Sec = 3;
        private double _Threshold_Step1;
        private double _Threshold_Step2;
        /// <summary>
        /// Period of validity of the oscillation flag, in second.
        /// </summary>
        private double _OsciFlagValidTime_Sec = 2;

        /// <summary>
        /// Data buffer for data in both step1 and step2;
        /// </summary>
        private FNETTimeSeriesBuffer<TimeStampedValue> _Buffer;
        /// <summary>
        /// Data buffer for getting max in step2;
        /// </summary>
        private FNETSlideWindowForMax<TimeStampedValue> _Buffer_ForMax;
        /// <summary>
        /// Data buffer for getting min in step2;
        /// </summary>
        private FNETSlideWindowForMax<TimeStampedValue> _Buffer_ForMin;
        //private bool _Step1Flag = false;    //true: exceed step1 threshold.
        //private bool _Step2Flag = false;    //true: exceed step2 threshold.
        private bool _OsciFlag = false;
        private DateTime _OsciFlagExpireTime = DateTime.MinValue;
        private DateTime _LatestTimeStamp;
        private static FNETLog OsciDetectorLoger = new FNETLog(OsciDetection.OsciDetectionFolder, new FNETLogTypeCFG("OsciDetectorLog", "", true, true, FNETLogTypeCFG.ExpireType.Monthly));
        private FNETDistribution _FirstStepDis;
        public OsciDetector(int fps, double step1Window, double step2Window, double threshold_Step1,double threshold_Step2, int unitID, string name)
        {
            //if(OsciDetectorLoger == null)
            //    OsciDetectorLoger = new FNETLog(OsciDetection.OsciDetectionFolder, new FNETLogTypeCFG("OsciDetectorLog", "", true, true, FNETLogTypeCFG.ExpireType.Monthly));
            _FramePerSecond = fps;
            _WindowLength_Step1_Sec = step1Window;
            _WindowLength_Step2_Sec = step2Window;
            _Threshold_Step1 = threshold_Step1;
            _Threshold_Step2 = threshold_Step2;
            _UnitID = unitID;
            _Name = name;
            _Buffer = new FNETTimeSeriesBuffer<TimeStampedValue>(_FramePerSecond, _WindowLength_Step1_Sec * 1.1 + _WindowLength_Step2_Sec);
            _Buffer_ForMax = new FNETSlideWindowForMax<TimeStampedValue>(_FramePerSecond, _WindowLength_Step2_Sec, (A, B) => A > B, TimeStampedValue.GetValueHandler);
            _Buffer_ForMin = new FNETSlideWindowForMax<TimeStampedValue>(_FramePerSecond, _WindowLength_Step2_Sec, (A, B) => A < B, TimeStampedValue.GetValueHandler);
            _FirstStepDis = new FNETDistribution(-3 * threshold_Step1, 3 * threshold_Step1, 1000, name);
        }
        public bool Detect(TimeStampedValue oneValue)
        {
            if (oneValue == null)
                return _OsciFlag;
            _LatestTimeStamp = oneValue.TimeStamp;
            if(_OsciFlag && oneValue.TimeStamp.CompareTo(_OsciFlagExpireTime)>=0)
                _OsciFlag = false;
            _Buffer.Push(oneValue);
            _Buffer_ForMax.Push(oneValue);
            _Buffer_ForMin.Push(oneValue);
            DetectOsci();
            return _OsciFlag;
        }
        private void DetectOsci()
        {
            //check if there are enought frames in the buffer.
            if (_Buffer.IsInside(_LatestTimeStamp.AddSeconds(-1 * (_WindowLength_Step1_Sec + _WindowLength_Step2_Sec)))<0)
                return;
            TimeStampedValue V2 = _Buffer.GetExactItemByTime(_LatestTimeStamp.AddSeconds(-1 * _WindowLength_Step2_Sec));
            TimeStampedValue V1 = _Buffer.GetExactItemByTime(_LatestTimeStamp.AddSeconds(-1 * (_WindowLength_Step2_Sec+ _WindowLength_Step1_Sec)));
            //missing frame?
            if (V1 == null || V2 == null) return;
            //distribution statistics
            if(V1.TimeStamp.Millisecond==0)
            {
                _FirstStepDis.AddOneValue(V2.Value - V1.Value);
                if (V1.TimeStamp.Second == 0 && V1.TimeStamp.Minute % 5 == 0)
                    _FirstStepDis.SyncToFile("Log\\");
            }
            //log
            if (Math.Abs(V2.Value - V1.Value) > _Threshold_Step1 * 0.7)
            {
                Log(string.Format("Step 1: {0}\t{1}({2}@{4} -> {3}@{5}) (TH: {6})", (Math.Abs(V2.Value - V1.Value) >= _Threshold_Step1 ? "*" : " "), (V2.Value - V1.Value).ToString("F6"), V1.Value.ToString("F6"), V2.Value.ToString("F6"), V1.TimeStamp.ToString("HH:mm:ss.fff"), V2.TimeStamp.ToString("HH:mm:ss.fff"), _Threshold_Step1.ToString("F6")));
            }
            //check the variation of step1
            if (Math.Abs(V2.Value - V1.Value) < _Threshold_Step1) return;
            //check the max & min of step2.
            TimeStampedValue max = _Buffer_ForMax.Max;
            TimeStampedValue min = _Buffer_ForMin.Max;
            if (max == null || min == null) return;
            //log step2
            Log(string.Format("Step 2: {0}\t{1}(Max {2}@{3}, Min {4}@{5}) (TH: {6})", (Math.Abs(max.Value - min.Value) >= _Threshold_Step2 ? "*" : " "), (max.Value - min.Value).ToString("F6"), max.Value.ToString("F6"), max.TimeStamp.ToString("HH:mm:ss.fff"), min.Value.ToString("F6"), min.TimeStamp.ToString("HH:mm:ss.fff"), _Threshold_Step2.ToString("F6")));

            if (Math.Abs(max.Value - min.Value) < _Threshold_Step2) return;
            _OsciFlag = true; 
            _OsciFlagExpireTime = _LatestTimeStamp.AddSeconds(_OsciFlagValidTime_Sec);
        }
        private void Log(string msg)
        {
            OsciDetectorLoger.WriteLog(_Name + "|" + msg);
        }
    }
}
