﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Timers;
using System.Threading;
using System.Diagnostics;

namespace FNETOsciMitigation
{
    
    public class SystemIdentification
    {
        public string ProbingCMDFile { get; set; }
        /// <summary>
        /// period of each probing command, default is 100ms.
        /// </summary>
        public int ProbingCMDPeriod_MS { get; set; }
        /// <summary>
        /// period of probing, in seconds, default is 3600.
        /// </summary>
        public int ProbingPeriod_S { get; set; }


        //for Probing and System Identification.
        /// <summary>
        /// flag indicates if probing in progress
        /// </summary>
        public bool IsProbing = false;
        /// <summary>
        /// flag indicates if writing frequency into files.
        /// </summary>
        private bool _IsSavingFreq = false;
        private Dictionary<int, StreamWriter> _ProbResponseWriters = new Dictionary<int, StreamWriter>();
        private Dictionary<int, StreamWriter> _ProbResponseWriters_His = new Dictionary<int, StreamWriter>();
        private string _ProbingFolder = "WADC_SysIdent\\";
        private string _SysIdentExe = "SysIdent.exe";
        private string _ProbingFolder_His = "WADC_SysIdent\\ProbRecord\\";
        private System.Timers.Timer _TimerForProbing;
        private OsciMitigator _OsciMitigator = null;
        private FNETLog SysIdentLoger;// = new FNETLog("Log\\", new FNETLogTypeCFG("SysIdentLog", "", true, true, FNETLogTypeCFG.ExpireType.Monthly));
        private Queue<double> _CommandFromFile = null;
        //private List<byte> ChannelIDs;
        private bool StoppingProb = false;
        private double _ReProbTimeSec = 60; //If probing is terminated due to osci, retry probing _ReProbTimeSec seconds later.

        public SystemIdentification(OsciMitigator osciMitigator, string rootFolder, string cmdFile, int cmdPeriod_MS, int probingPeriod_s, bool enableProbing)
        {            
            _ProbingFolder = rootFolder;
            _ProbingFolder_His = rootFolder + "ProbRecord\\";
            if (!Directory.Exists(_ProbingFolder_His))  Directory.CreateDirectory(_ProbingFolder_His);

            _OsciMitigator = osciMitigator;
            ProbingCMDFile = rootFolder + cmdFile;
            ProbingCMDPeriod_MS = cmdPeriod_MS;
            ProbingPeriod_S = probingPeriod_s;
            SysIdentLoger = new FNETLog(_ProbingFolder, new FNETLogTypeCFG("SysIdentLog", "", true, true, FNETLogTypeCFG.ExpireType.Monthly));
            if(enableProbing)
                InitialProbing();
        }

        //====================================================================
        
        private void InitialProbing()
        {
            _TimerForProbing = new System.Timers.Timer();
            _TimerForProbing.Elapsed += ExeProbing;
            _TimerForProbing.AutoReset = true;
            //_TimerForProbing.Interval = ProbingPeriod_S * 1000;
            _TimerForProbing.Interval = 60 * 1000;  //First probing will start 1 minute after initialled.
            _TimerForProbing.Start();
        }
        /// <summary>
        /// Execute probing/system identification process.
        /// </summary>
        private void ExeProbing(object sender, ElapsedEventArgs e)
        {
            try
            {
                _TimerForProbing.Interval = ProbingPeriod_S * 1000;

                if(_OsciMitigator.OsciFlag)
                {
                    SetReProbTimer();
                    _OsciMitigator.SendGUIMsg(string.Format("****** Probing is not started due to oscillation, will try again in {0} seconds ******", _ReProbTimeSec));
                    return;
                }
                //------------------------------------
                //Request Probing
                if(!ProbCoordinator.RequestProbing(_OsciMitigator.ControllerName))
                {   //Probing request denied
                    SetReProbTimer();
                    _OsciMitigator.SendGUIMsg(string.Format("****** [WADC:{2}]Probing request is denied because [{0}], will try again in {1} seconds ******", ProbCoordinator.GetCurrentProbName(), _ReProbTimeSec, _OsciMitigator.ControllerName));
                    return;
                }
                //probing request is accepted.
                //---------------------------------
                LoadCMDFromFile(ProbingCMDFile);
                IsProbing = true;
                _OsciMitigator.SendGUIMsg($"[WADC:{_OsciMitigator.ControllerName}]Probing signal is loaded from the command file. Probing injection started.");
                SysIdentLoger.WriteLog("****************\r\nProbing signal is loaded from the command file. Probing injection started.");
                SendProbingCMD();
                ProbCoordinator.ReleaseProbing(_OsciMitigator.ControllerName);  //release probing lock. 
                if (StoppingProb)   //function SendProbingCMD() is returned premature due to oscillation.
                {
                    _OsciMitigator.SendGUIMsg("****** Probing procedure is terminated since an oscillation is detected. ******");
                    SysIdentLoger.WriteLog("Probing procedure is terminated since an oscillation is detected.");
                    IsProbing = false;
                    StoppingProb = false;
                    return;
                }
                _OsciMitigator.SendGUIMsg($"[WADC:{_OsciMitigator.ControllerName}]Probing signal injection finished.");
                SysIdentLoger.WriteLog("Probing signal injection finished.");
                IsProbing = false;
                while (_IsSavingFreq) Thread.Sleep(5);
                ExecuteSystemIdent();
                StoppingProb = false;
            }
            catch (Exception exc)
            {
                SendGUIMsg("Exception happened during probing process. Exception message: " + exc.Message);
                SysIdentLoger.WriteLog("Exception happened during probing process. Exception message: " + exc.Message);
            }
        }
        //Stop the probing if a oscillation is detected.
        public void AbortProbing()
        {
            if (IsProbing)
            {
                _OsciMitigator.SendGUIMsg("****** Probing procedure will be terminated, will try again 1 minute later. ******");
                StoppingProb = true;
                _TimerForProbing.Stop();
                _TimerForProbing.Interval = _ReProbTimeSec * 1000;
                _TimerForProbing.Start();
            }            
        }
        public void SetReProbTimer()
        {
            _TimerForProbing.Stop();
            _TimerForProbing.Interval = _ReProbTimeSec * 1000;
            _TimerForProbing.Start();
        }
        private void SendGUIMsg(string Msg)
        {
            _OsciMitigator.SendGUIMsg(Msg);
        }
        private void LoadCMDFromFile(string ProbCMDFile = "CMD.txt")
        {
            if (_CommandFromFile == null)
                _CommandFromFile = new Queue<double>();//
            else
                _CommandFromFile.Clear();
            if (!File.Exists(ProbCMDFile)) return;
            string str;
            using (StreamReader sr = new StreamReader(ProbCMDFile))
            {
                while (!sr.EndOfStream)
                {
                    try
                    {
                        str = sr.ReadLine().Trim();
                        if (str == null || str.Length < 1 || str.Length > 30) continue;
                        _CommandFromFile.Enqueue(Convert.ToDouble(str));
                    }
                    catch (Exception e) { SysIdentLoger.WriteLog("Exception:" + e.Message); }
                }
            }
        }
        private void SendProbingCMD()
        {
            DateTime nextProbCMDTime = DateTime.UtcNow;
            StreamWriter sw_His = new StreamWriter(string.Format("{0}{1}_SysIdent_probing.csv", _ProbingFolder_His, DateTime.UtcNow.ToString("yyyy_MM_dd_HH")));
            using (StreamWriter sw = new StreamWriter(_ProbingFolder + "SysIdent_probing.csv", false))
            {
                while (_CommandFromFile.Count > 0)
                {
                    if (StoppingProb) break;
                    if (DateTime.UtcNow.CompareTo(nextProbCMDTime) < 0)
                    {
                        Thread.Sleep(2); continue;
                    }
                    try
                    {
                        nextProbCMDTime = GetNextExeTime(ProbingCMDPeriod_MS);
                        double v = _CommandFromFile.Dequeue();
                        _OsciMitigator.SendControlCommand(DateTime.UtcNow, Convert.ToInt32(v), true);
                        string record = string.Format("{0},{1}", DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss.fff"), Convert.ToInt32(v).ToString());
                        sw.WriteLine(record);
                        sw_His.WriteLine(record);
                        sw.Flush();
                        sw_His.Flush();
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
            sw_His.Close();
        }
        public void SaveProbResponse(List<PMUMeasurement> ReceivedMea)
        {
            if (!IsProbing)
            {
                if (_ProbResponseWriters != null && _ProbResponseWriters.Count > 0)
                {
                    lock (_ProbResponseWriters)
                    {
                        foreach (var v in _ProbResponseWriters)
                            v.Value.Close();
                        _ProbResponseWriters.Clear();
                        foreach (var v in _ProbResponseWriters_His)
                            v.Value.Close();
                        _ProbResponseWriters_His.Clear();
                        SendGUIMsg("Probing frequency response were saved.");
                        SysIdentLoger.WriteLog("Probing frequency response were saved.");
                    }
                }
                _IsSavingFreq = false;
                return;
            }
            _IsSavingFreq = true;
            foreach (var m in ReceivedMea)
            {
                if (!_ProbResponseWriters.ContainsKey(m.UnitID))
                    _ProbResponseWriters.Add(m.UnitID, new StreamWriter(string.Format("{0}SysIdent_{1}_freq.csv", _ProbingFolder, m.UnitName), false));
                if (!_ProbResponseWriters_His.ContainsKey(m.UnitID))
                    _ProbResponseWriters_His.Add(m.UnitID, new StreamWriter(string.Format("{0}{1}_SysIdent_{2}_freq.csv", _ProbingFolder_His, DateTime.UtcNow.ToString("yyyy_MM_dd_HH"), m.UnitName), false));

                if (m.Frequency != 0)
                {
                    string oneRec = string.Format("{0},{1},{2}", m.TimeStamp.ToString("MM/dd/yyyy HH:mm:ss.fff"), m.Frequency.ToString(), DateTime.UtcNow.Subtract(m.TimeStamp).TotalSeconds.ToString("F3"));
                    _ProbResponseWriters[m.UnitID].WriteLine(oneRec);
                    _ProbResponseWriters_His[m.UnitID].WriteLine(oneRec);
                }
            }

        }
        private DateTime GetNextExeTime(int ExeIntervalInMS)
        {
            DateTime nextTime = DateTime.UtcNow.AddMilliseconds(ExeIntervalInMS);
            int msToSubtract = nextTime.Millisecond % ExeIntervalInMS;
            nextTime = nextTime.AddMilliseconds(-1 * msToSubtract);
            return nextTime;
        }
        private void ExecuteSystemIdent()
        {
            if (!File.Exists(_ProbingFolder + _SysIdentExe))
            {
                SendGUIMsg("File " + _ProbingFolder + _SysIdentExe + " doesn't exist.");
                SysIdentLoger.WriteLog("executable file " + _ProbingFolder + _SysIdentExe + " can not be found.");
                return;
            }
            else
                SysIdentLoger.WriteLog("executable file "+ _ProbingFolder + _SysIdentExe +" is found.");
            try
            {
                FileInfo fi = new FileInfo(_ProbingFolder + _SysIdentExe);
                Process SysIdentProc = new Process();
                SysIdentProc.StartInfo.FileName = fi.FullName;
                SysIdentProc.StartInfo.WorkingDirectory = _ProbingFolder;                
                SendGUIMsg("System identification process started.");
                SysIdentLoger.WriteLog("System identification process started.");
                SysIdentProc.Start();
            }
            catch (Exception e)
            {
                SendGUIMsg("Exception happened during system identification process. Exception message: " + e.Message);
                SysIdentLoger.WriteLog("Exception happened during system identification process. Exception message: " + e.Message);
            }
        }

    }

    public class ProbCoordinator
    {
        public static int IntervalSec = 30;
        public static DateTime NextProbTime = DateTime.Now;
        public static Dictionary<string, DateTime> RunningProbDic = new Dictionary<string, DateTime>();
        
        /// <summary>
        /// True: The probing request is accepted, False: denied.
        /// </summary>
        /// <param name="WADCName"></param>
        /// <returns></returns>
        public static bool RequestProbing(string WADCName)
        {
            lock(RunningProbDic)
            {
                foreach (var v in RunningProbDic)
                    if (DateTime.Now.CompareTo(v.Value) < 0)    //Some WADC is probling
                        return false;
                RunningProbDic.Clear();
                if (DateTime.Now.CompareTo(NextProbTime) < 0)   //Within WADCs' probing interval.
                    return false;
                RunningProbDic.Add(WADCName, DateTime.Now.AddSeconds(3600)); //record the name and expire time of the probing WADC. It will expire in 60 minutes, no matter if it "release"
                return true;
            }
        }
        public static string GetCurrentProbName()
        {
            foreach (var v in RunningProbDic)
                if (DateTime.Now.CompareTo(v.Value) < 0)    //Some WADC is probling
                    return v.Key;
            if (DateTime.Now.CompareTo(NextProbTime) < 0)
                return "it will wait a few seconds after previous probing done";
            return " ";
        }
        public static void ReleaseProbing(string WADCName)
        {
            if (RunningProbDic.ContainsKey(WADCName))
                RunningProbDic.Remove(WADCName);
            NextProbTime = DateTime.Now.AddSeconds(IntervalSec);
        }

    }
}
