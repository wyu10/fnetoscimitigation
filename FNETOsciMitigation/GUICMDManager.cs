﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace FNETOsciMitigation
{
    //class to receive and handle command from GUI
    public class GUICMDManager
    {
        private OsciMitigator _OsciMitigator;
        private CMDReceiver _CMDReceiver;
        private int _PortToReceiveCMD;
        public GUICMDManager(int portToReceive, OsciMitigator osciMitigator)
        {
            _PortToReceiveCMD = portToReceive;
            _OsciMitigator = osciMitigator;
            _CMDReceiver = new CMDReceiver(_PortToReceiveCMD, osciMitigator);
            _CMDReceiver.OnCommandReceived += ExecuteCommand;
        }

        private void ExecuteCommand(string cmdName, string cmdValue, string WADCName)
        {
            
            switch (cmdName)
            {
                case "Relay":
                    SetRelayStatus(cmdValue, WADCName);
                    break;
                case "EnableWADC":
                    EnableWADC(cmdValue, WADCName);
                    break;
            }

        }
        private void EnableWADC(string cmdValue, string WADCName)
        {
            if(OsciMitigator.WADCAdaptors.ContainsKey(WADCName))
            {
                try
                {
                    //OsciMitigator.WADCAdaptors[WADCName].SendGUIMsg("WADC received cmd from GUI: EnableWADC=" + cmdValue + " Node:" + WADCName);
                    OsciMitigator.WADCAdaptors[WADCName].WADCEnabled = Convert.ToDouble(cmdValue) > 0 ? true : false;
                }
                catch { }
            }
        }
        private void SetRelayStatus(string cmdValue, string WADCName)
        {
            byte[] password = null;
            int GUIRelayCode = 0;   //wadc+=2 or 0,  pss+=1 or 0.
            switch(cmdValue)
            {
                case "AA55":
                    password = new byte[] { 0xAA, 0x55 };
                    GUIRelayCode = 3;   //11
                    break;
                case "AA00":
                    password = new byte[] { 0xAA, 0x00 };
                    GUIRelayCode = 1;   //01
                    break;
                case "0055":
                    password = new byte[] { 0x00, 0x55 };
                    GUIRelayCode = 2;   //10
                    break;
                case "0000":
                    password = new byte[] { 0x00, 0x00 };
                    GUIRelayCode = 0;   //00
                    break;
            }
            if (password != null && OsciMitigator.WADCAdaptors.ContainsKey(WADCName))
            {
                OsciMitigator.WADCAdaptors[WADCName].SetRelayStatus(password, GUIRelayCode);
            }
                //_OsciMitigator.SetRelayStatus(password);
        }
    }
    public class CMDReceiver
    { 
        private int PortToReceive;
        private UdpClient ThisUDPClient;
        private IPEndPoint ThisIPEndPoint;
        public delegate void CommandHandler(string commandName, string commandValue, string WADCName);
        public event CommandHandler OnCommandReceived;
        private OsciMitigator _OsciMitigator;
        private char[] spliter = new char[] { '=', '@' };
        public CMDReceiver(int portToReceive, OsciMitigator osciMitiObj)
        {
            PortToReceive = portToReceive;
            _OsciMitigator = osciMitiObj;
            StartToListen();
        }

        private void StartToListen()
        {
            try
            {
                if (PortToReceive > 65535) return;
                ThisUDPClient = new UdpClient(PortToReceive);
                ThisIPEndPoint = new IPEndPoint(IPAddress.Any, PortToReceive);
                try
                {
                    ThisUDPClient.BeginReceive(new AsyncCallback(ReceiveCallback), null);
                }
                catch(Exception ee)
                {
                    _OsciMitigator.SendGUIMsg("Exception caught from controller when try to receive UDP package from port " + PortToReceive.ToString()+".\r\n Exception msg:\r\n"+ee.Message);
                }
            }
            catch (Exception e)
            {
                _OsciMitigator.SendGUIMsg("Exception caught from controller when try to listen port " + PortToReceive.ToString() + ".\r\n Exception msg:\r\n" + e.Message);
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                byte[] ReceivedByte;
                try { ReceivedByte = ThisUDPClient.EndReceive(ar, ref ThisIPEndPoint); }
                catch { return; }
                if (ReceivedByte != null && ReceivedByte.Length > 0)
                {
                    string content = Encoding.ASCII.GetString(ReceivedByte, 0, ReceivedByte.Length).Trim();
                    ProcessCommand(content);                    
                }
            }
            catch { }
            ThisUDPClient.BeginReceive(new AsyncCallback(ReceiveCallback), null);
        }
        private void ProcessCommand(string contentReceived)
        {
            try
            {
                if (contentReceived.Length < 5) return;
                if (!contentReceived.StartsWith("CMD:")) return;
                string[] arr = contentReceived.Substring(4).Split(spliter);
                if (arr.Length != 3) return;
                OnCommandReceived?.Invoke(arr[0], arr[1], arr[2]);
            }
            catch { }
        }
    }
}
