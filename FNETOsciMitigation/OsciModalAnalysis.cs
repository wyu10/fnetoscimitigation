﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNETOsciMitigation
{
    public class OsciModalAnalysis
    {
        private int bufferPMUID = 0;
        private FNETTimeSeriesBuffer<PMUMeasurement> dataBuffer = null;
        private int intervalOfAnalysis = 300;
        private DateTime nextAnalysisTime=DateTime.MinValue;
        private string modalAnalysisFolder = "ModalAnalysis\\";
        private string rawDataFileSeed = "ModalAnalysisRawData";
        private string modalAnalyzerName = "ModalAnalyzer.exe";
        private string modalResultFile = "Dominant_Mode.txt.txt";
        private string modalResultHist = "Dominant_Mode_Record.txt";
        private DateTime latestTimestamp = DateTime.MinValue;
        private OsciMitigator _OsciMitigator = null;
        private Process SysIdentProc = null;
        //------------------------------------------------
        //result
        public DateTime ModalAnalysisTime;
        public double DominantFrequency;
        public double DampingRatio;
        public bool WADCFlag = false;

        public OsciModalAnalysis(int PMUID, int fps, int bufferTimeLength,int analysisInterval, OsciMitigator osciMitigator)
        {
            bufferPMUID = PMUID;
            dataBuffer = new FNETTimeSeriesBuffer<PMUMeasurement>(fps, bufferTimeLength);
            intervalOfAnalysis = analysisInterval;
            _OsciMitigator = osciMitigator;
        }
        public void OnDataReceived(List<PMUMeasurement> MeaList)
        {
            if (MeaList == null || MeaList.Count <= 0) return;
            if (nextAnalysisTime == DateTime.MinValue)
                nextAnalysisTime = DateTime.Now;
            lock (dataBuffer)
            {
                //The same data could be sent to this function multiple times when multiple WADC are setup.
                foreach (var v in MeaList)
                    if (v.UnitID == bufferPMUID && v.TimeStamp.CompareTo(latestTimestamp) > 0)
                    {
                        dataBuffer.Push(v);
                        latestTimestamp = v.TimeStamp;
                    }
            }
            if (DateTime.Now.CompareTo(nextAnalysisTime) >= 0)
                ExecuteAnalysis();            
        }
        private void ExecuteAnalysis()
        {
            nextAnalysisTime = DateTime.Now.AddSeconds(intervalOfAnalysis);
            SaveRawFile();
            ExecuteModalAnalysis();
        }

        private void ExecuteModalAnalysis()
        {
            if (!File.Exists(modalAnalysisFolder + modalAnalyzerName))
            {
                SendGUIMsg("Modal analyzer executable file " + modalAnalysisFolder + modalAnalyzerName + " doesn't exist.");
                // log SysIdentLoger.WriteLog("executable file " + _ProbingFolder + _SysIdentExe + " can not be found.");
                return;
            }
            //else SysIdentLoger.WriteLog("executable file " + _ProbingFolder + _SysIdentExe + " is found.");
            try
            {
                FileInfo fi = new FileInfo(modalAnalysisFolder + modalAnalyzerName);
                SysIdentProc = new Process();
                SysIdentProc.StartInfo.FileName = fi.FullName;
                SysIdentProc.StartInfo.WorkingDirectory = modalAnalysisFolder;
                SysIdentProc.Exited += OnModalAnalysisExit;
                SendGUIMsg("Modal analysis process started.");
                //SysIdentLoger.WriteLog("System identification process started.");
                SysIdentProc.Start();                
            }
            catch (Exception e)
            {
                SendGUIMsg("Exception happened during system identification process. Exception message: " + e.Message);
                //SysIdentLoger.WriteLog("Exception happened during system identification process. Exception message: " + e.Message);
            }
        }
        public void OnModalAnalysisExit(object sender, EventArgs e)
        {
            if(!File.Exists(modalAnalysisFolder + modalResultFile))
            {
                SendGUIMsg("Modal analysis result file " + modalResultFile + " can't be found.");
                return;
            }
            StreamReader sr = new StreamReader(modalAnalysisFolder + modalResultFile);
            string oneline;
            while(!sr.EndOfStream)
            {
                oneline = sr.ReadLine();
                if (oneline == null || oneline.Length < 3) continue;
                string[] arr = oneline.Split(',');
                if (arr.Length < 4) continue;
                //time, dominant mode freq, damping ratio, WADC flag
                ModalAnalysisTime = DateTime.ParseExact(arr[0], "MM/dd/yyyy HH:mm:ss.fff", null);
                DominantFrequency = Convert.ToDouble(arr[1]);
                DampingRatio = Convert.ToDouble(arr[2]);
                WADCFlag = Convert.ToInt32(arr[3]) > 0 ? true : false;
                StreamWriter sw = new StreamWriter(modalAnalysisFolder + modalResultHist, true);
                sw.WriteLine(oneline);
                sw.Close();
                break;
            }
        }

        private void SaveRawFile()
        {
            StreamWriter sw = new StreamWriter(modalAnalysisFolder + rawDataFileSeed + ".csv");
            StreamWriter sw_His = new StreamWriter(string.Format("{0}{1}_{2}.csv", modalAnalysisFolder, rawDataFileSeed, DateTime.Now.ToString("yyyy.MM.ddTHH.mm.ss")));
            lock (dataBuffer)
            {
                foreach (var v in dataBuffer)
                {
                    string oneRec = string.Format("{0},{1}", v.TimeStamp.ToString("MM/dd/yyyy HH:mm:ss.fff"), v.Frequency.ToString());
                    sw.WriteLine(oneRec);
                    sw_His.WriteLine(oneRec);
                }
            }
            sw.Close();
            sw_His.Close();
        }
        private void SendGUIMsg(string Msg)
        {
            _OsciMitigator.SendGUIMsg(Msg);
        }
    }
}
