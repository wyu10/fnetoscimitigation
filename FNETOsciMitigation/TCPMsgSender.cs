﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Collections.Concurrent;

namespace FNETOsciMitigation
{
    public class TCPMsgSender
    {
        public Socket WorkSocket = null;
        public IPEndPoint RemoteEndpoint;
        public string Key = "";
        private bool IsTryingToConnect = false;
        private bool IsConnected = false;
        private DateTime TimeToConnect = DateTime.Now;
        private double SecondsToReconnect = 30;
        private Thread FwdThread = null;
        private ConcurrentQueue<byte[]> FwdBuffer;// = new ConcurrentQueue<FDRRecord>();
        private int FwdBufferCap = 600;
        
        //private bool IsDisposing = false;
        //private int CountToStartSendMultipleRec = 20;
        //private int CountMultipleRec = 10;
        private byte[] BufferFromFwdDest = null;
        private AsyncCallback AsyncCallbackHandler;
        //private bool FNETProtocolOnly = false;
        public event SendGUIMsg OnTCPMsgSent;
        public OsciMitigator WADCObj;

        //public SocketAndEndPoint(Socket s, IPEndPoint ep, DataHandler fdrReceiver, bool fnetProtocolOnly)
        public TCPMsgSender(IPEndPoint ep, OsciMitigator wadcObj)
        {
            RemoteEndpoint = ep;
            WADCObj = wadcObj;
            Key = ep.Address.ToString() + ":" + ep.Port.ToString();            
            AsyncCallbackHandler = new AsyncCallback(ReceiveCallback);
        }
        //public void Dispose()
        //{
        //    IsDisposing = true;
        //    try
        //    {
        //        if (FwdThread != null)
        //            FwdThread.Abort();
        //    }
        //    catch { }
        //}

        private void FwdConCallback(IAsyncResult ar)
        {
            Socket s = (Socket)ar.AsyncState;
            try
            {
                this.IsTryingToConnect = false;                
                if (s != null && s.Connected)
                {
                    IsConnected = true;
                    BufferFromFwdDest = new byte[8192];
                    ((Socket)ar.AsyncState).BeginReceive(BufferFromFwdDest, 0, 8192, SocketFlags.None, AsyncCallbackHandler, ar.AsyncState);
                    //OnTCPMsgSent?.Invoke("TCP_Sender", "Connected");
                    if (WADCObj != null) WADCObj.SendGUIMsg(string.Format("[{0}] TCP connection to {1} is established", WADCObj.ControllerName, RemoteEndpoint.ToString()));
                }
            }
            catch (Exception e)
            {
                //OnTCPMsgSent?.Invoke("TCP_Sender_Exception", "ConnCallback: "+e.Message );
                //throw e;
                //FNetServerManager.LogException(YWPLogCategory.Type_DataFwd_Exception, 0, "Exception caught when Fwd data.", e);
            }
        }
        private void ReceiveCallback(IAsyncResult ar)
        {
            Socket thisSocket = (Socket)ar.AsyncState;
            
            try
            {
                //OnTCPMsgSent?.Invoke("TCP_Sender", "ReceiveCallback: before end receive.");
                int count = thisSocket.EndReceive(ar);
                //OnTCPMsgSent?.Invoke("TCP_Sender", "ReceiveCallback: " + count.ToString()+" bytes received.");
                //if received anything from the destination, send cfg frame to it.
                //YWPLog.WriteDefaultLog(YWPLogCategory.Type_DataFwd_Exception, BitConverter.ToString(BufferFromFwdDest), DataHandler.FwdLogCFG);
                if (count > 8)
                {
                    //thisSocket.Send(FDRDataReceiverAndHandler.TCPServer.CFGFrame.FrameHeader.Source);
                    //do something?
                }
                else
                {
                    //do something?
                    //YWPLog.WriteDefaultLog(YWPLogCategory.Type_DataFwd_Exception, FDRDataReceiverAndHandler.FDRID.ToString() + " doesn't reply cfg frame due to some reason.", DataHandler.FwdLogCFG);
                }
            }
            catch (Exception e)
            {
                //OnTCPMsgSent?.Invoke("TCP_Sender_Exception", "ReceiveCallback: " + e.Message);
                //throw e;
                //YWPLog.WriteDefaultLog(YWPLogCategory.Type_DataFwd_Exception, FDRDataReceiverAndHandler.FDRID.ToString() + " got exception when received something from destination.", e, DataHandler.FwdLogCFG);
            }
            Thread.Sleep(2000);
            //OnTCPMsgSent?.Invoke("TCP_Sender", "ReceiveCallback: before begin receive.");
            try { thisSocket.BeginReceive(BufferFromFwdDest, 0, 8192, SocketFlags.None, AsyncCallbackHandler, thisSocket); }
            catch (Exception ee)
            {
                //OnTCPMsgSent?.Invoke("TCP_Sender_Exception", "ReceiveCallback2: " + ee.Message);
                //throw ee;
                //YWPLog.WriteDefaultLog(YWPLogCategory.Type_DataFwd_Exception, FDRDataReceiverAndHandler.FDRID.ToString() + " got exception when try to BeginReceive from destination.", ee, DataHandler.FwdLogCFG);
            }
        }

        public void SendTCPData(byte[] OneRec, bool AsyncMode = true)
        {
            if (AsyncMode)
                FwdDataAsync(OneRec);
            else
                SendDataSync(OneRec);
        }
        private void SendDataSync(byte[] OneRec)
        {
            if (OneRec == null) return;
            try
            {
                if ((WorkSocket == null || !WorkSocket.Connected) && (!IsTryingToConnect || TimeToConnect.AddSeconds(SecondsToReconnect).CompareTo(DateTime.Now) < 0))
                {   //need to re-connect tcp connection
                    if (WorkSocket != null)
                        WorkSocket.Dispose();
                    WorkSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, System.Net.Sockets.ProtocolType.IP);
                    IsTryingToConnect = true;
                    IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
                    WorkSocket.Bind(endPoint);
                    TimeToConnect = DateTime.Now;
                    WorkSocket.BeginConnect(RemoteEndpoint, new AsyncCallback(FwdConCallback), this);
                }
                if (WorkSocket != null && WorkSocket.Connected)
                {
                    try
                    {
                        WorkSocket.Send(OneRec);
                        //WorkSocket.BeginSend(data, 0, data.Length, 0, null, null);
                    }
                    catch (Exception ee)
                    {
                        //OnTCPMsgSent?.Invoke("TCP_Sender_Exception", "SendTCPSynch: " + ee.Message);
                        //throw ee;
                    }
                }
            }
            catch { }
        }

        private void FwdDataAsync(byte[] OneRec)
        {
            if (FwdBuffer == null)
                FwdBuffer = new ConcurrentQueue<byte[]>();
            byte[] headData;
            while (FwdBuffer.Count >= FwdBufferCap)
                FwdBuffer.TryDequeue(out headData);
            FwdBuffer.Enqueue(OneRec);
            if (FwdThread == null)
            {
                FwdThread = new Thread(new ThreadStart(FwdDataFromBuffer));
                FwdThread.Start();
            }
        }
        private void FwdDataFromBuffer()
        {
            int SleepTime = 20;
            while (true)
            {
                try
                {
                    //------Try to connect--------
                    if ((WorkSocket == null || !WorkSocket.Connected) && (!IsTryingToConnect || TimeToConnect.AddMinutes(1).CompareTo(DateTime.Now) < 0))
                    {
                        if (WorkSocket != null) WorkSocket.Dispose();
                        WorkSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, System.Net.Sockets.ProtocolType.IP);
                        IsTryingToConnect = true;    
                        IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
                        WorkSocket.Bind(endPoint);
                        TimeToConnect = DateTime.Now;
                        WorkSocket.BeginConnect(RemoteEndpoint, new AsyncCallback(FwdConCallback), WorkSocket);
                        if (WADCObj != null) WADCObj.SendGUIMsg(string.Format("[{0}] Try establish TCP connection to {1}", WADCObj.ControllerName, RemoteEndpoint.ToString()));
                        //OnTCPMsgSent?.Invoke("TCP_Sender", "Begin Connect");
                    }
                    //-------send data----------
                    //if (FwdBuffer.Count > 0)
                    //{
                    //    string str = "|||-> " + FwdBuffer.Count.ToString() + " records in buffer.";
                    //    OnTCPMsgSent?.Invoke("TCP_Sender", str);
                    //}
                    while (WorkSocket != null && WorkSocket.Connected && FwdBuffer.Count > 0)
                    {                        
                        try
                        {                            
                            byte[] byteToFwd = null;
                            FwdBuffer.TryDequeue(out byteToFwd);
                            if (byteToFwd != null)
                            {
                                int sentByte=WorkSocket.Send(byteToFwd);
                                if(sentByte>0)
                                    OnTCPMsgSent?.Invoke("Heartbeat_Sent", "1");
                                else
                                    OnTCPMsgSent?.Invoke("Heartbeat_Sent", "2");
                            }
                            //OnTCPMsgSent?.Invoke("TCP_Sender", "One HB sent.");
                        }
                        catch (Exception ee)
                        {
                            //OnTCPMsgSent?.Invoke("TCP_Sender_Exception", ee.Message);
                        }
                    }                    
                    
                }
                catch (Exception e)
                {
                    //OnTCPMsgSent?.Invoke("TCP_Sender_Exception_2", e.Message);
                    //FNetServerManager.LogException(YWPLogCategory.Type_DataFwd_Exception, FDRID, "Get exception when FWD data.", e);
                }
                Thread.Sleep(SleepTime);
                //SleepTime = Math.Min(SleepTime + 50, 5000);
            }
        }
    }
}
