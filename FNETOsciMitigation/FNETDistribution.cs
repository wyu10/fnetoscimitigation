﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FNETOsciMitigation
{
    public class FNETDistribution
    {
        private double LowerLimit;
        private double UpperLimit;
        /// <summary>
        /// count of values smaller than low limit.
        /// </summary>
        private int OverLower = 0;
        private int OverUpper = 0;
        private int BinCount;
        private List<int> Counts;
        private double BinWidth;
        private int _totalCount = 0;
        public int TotalCount { get { return _totalCount; } }
        private double Sum = 0;
        public string Name;
        public string ID
        {
            get
            {
                if (Name.Length > 0 && Name.Split('-').Length == 3) return Name.Split('-')[2];
                else return Name;
            }
        }
        public FNETDistribution(double lowLimit, double upLimit, int binCount, string name)
        {
            Name = name;
            LowerLimit = lowLimit;
            UpperLimit = upLimit;
            BinCount = binCount;
            BinWidth = (UpperLimit - LowerLimit) / (double)BinCount;
            Counts = new List<int>(BinCount);
            for (int i = 0; i < BinCount; i++) Counts.Add(0);
        }
        /// <summary>
        /// Add one sample value to the statistics
        /// </summary>
        public void AddOneValue(double value)
        {
            if (value >= UpperLimit) OverUpper++;
            else if (value < LowerLimit) OverLower++;
            else
            {
                int Index = (int)((value - LowerLimit) / BinWidth);
                if (Index >= 0 && Index < Counts.Count) Counts[Index]++;
            }
            _totalCount++;
            Sum += value;
        }
        public double Expectation { get { return _totalCount == 0 ? 0 : Sum / _totalCount; } }
        public double GetMost()
        {
            if (Counts == null || Counts.Count == 0) return 0;
            int index = 0;
            int c = 0;
            for (int i = 0; i < Counts.Count; i++)
            {
                if (Counts[i] > c)
                {
                    c = Counts[i];
                    index = i;
                }
            }
            return GetX(index);
        }
        /// <summary>
        /// Calculate the lowest boundary such that the count of values over this boundary is not more than N.
        /// </summary>
        public double GetNthUpper(int N)
        {
            if (N >= _totalCount)
                return LowerLimit;
            int count = OverUpper;
            int i = Counts.Count - 1;
            while (i > 0 && count + Counts[i] < N)
                count += Counts[i--];
            double result = GetX(i);
            if (i >= 0 && Counts[i] > 0)
                result = result + BinWidth * (1 - Convert.ToDouble(N - count) / Counts[i]);
            return result;
            //return GetX(i) + BinWidth *(1-Convert.ToDouble(N-count)/Counts[i]);
        }

        /// <summary>
        /// Calculate the highest boundary such that the count of values below this boundary is not more than N.
        /// </summary>
        public double GetNthLower(int N)
        {
            if (N >= _totalCount)
                return UpperLimit;
            int count = OverLower;
            int i = 0;
            while (i < BinCount - 1 && count + Counts[i] < N)
                count += Counts[i++];
            double result = GetX(i);
            if (i <= BinCount - 1 && Counts[i] > 0)
                result = result + Convert.ToDouble(N - count) / Counts[i] * BinWidth;
            return result;
        }
        /// <summary>
        /// Get the highest boundary such that the percentage of values below this boundary is not more than N, which belongs to [0,1)
        /// </summary>
        public double GetNPercentLower(double N) { return GetNthLower((int)(N * _totalCount)); }
        /// <summary>
        /// Get the lowest boundary such that the percentage of values over this boundary is not more than N. N belongs to [0,1)
        /// </summary>
        public double GetNPercentUpper(double N) { return GetNthUpper((int)(N * _totalCount)); }

        public List<int> GetAbsCounts()
        {
            List<int> r = Counts.ToList<int>();
            r.Insert(0, OverLower);
            r.Add(OverUpper);
            return r;
        }
        public List<double> GetCountPercent()
        {
            List<double> result = new List<double>();
            result.Add(((double)OverLower / (double)_totalCount));
            foreach (int c in Counts)
                result.Add(((double)c) / ((double)_totalCount));
            result.Add(((double)OverUpper / (double)_totalCount));
            return result;
        }
        public string GetLabel()
        {
            string r = "<" + LowerLimit.ToString();
            for (int i = 0; i < BinCount; i++)
                r += "\t" + (LowerLimit + BinWidth * i).ToString();
            r += "\t>" + UpperLimit.ToString();
            return r;
        }
        public string GetAbsCountsString()
        {
            var counts = GetAbsCounts();
            string r = Name;
            foreach (var c in counts) r += "\t" + c.ToString();
            return r;
        }
        public string GetCountPercentString()
        {
            var counts = GetCountPercent();
            string r = Name;
            foreach (var c in counts) r += "\t" + c.ToString("F4");
            return r;
        }
        /// <summary>
        /// Get the left boundary of BinIndex th bin.
        /// </summary>
        /// <param name="BinIndex"></param>
        /// <returns></returns>
        public double GetX(int BinIndex)
        {
            return LowerLimit + BinWidth * BinIndex;
        }
        /// <summary>
        /// Get the probability of variable being smaller than X
        /// </summary>
        /// <param name="X"></param>
        /// <returns></returns>
        public double GetProbality(double X)
        {
            double datacount = 0;
            if (LowerLimit <= X) datacount += OverLower;
            for (int i = 0; i < BinCount; ++i)
            {
                if (GetX(i) < X) datacount += Counts[i];
                else break;
            }
            if (UpperLimit <= X) datacount += OverUpper;
            if (TotalCount > 0) return datacount / TotalCount;
            else return -1;
        }
        public string SerializeToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.Append(string.Format("{0}|{1}|{2}|{3}|{4}|{5}", Name, LowerLimit, UpperLimit, BinCount, _totalCount, Sum));
            var counts = GetAbsCounts();
            foreach (var c in counts) SB.Append("," + c.ToString());
            return SB.ToString();
        }
        public string SerializePercentToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.Append(string.Format("{0}|{1}|{2}|{3}|{4}|{5}", Name, LowerLimit, UpperLimit, BinCount, _totalCount, Sum));
            var counts = GetCountPercent();
            foreach (var c in counts) SB.Append("," + c.ToString());
            return SB.ToString();
        }
        public string SerializeToTitleString()
        {
            StringBuilder SB = new StringBuilder();
            SB.Append("Info");
            SB.Append("," + (LowerLimit - BinWidth).ToString());
            for (int i = 0; i < BinCount; i++)
                SB.Append("," + GetX(i).ToString());
            SB.Append("," + UpperLimit.ToString());
            return SB.ToString();
        }
        public static FNETDistribution DeserializeFromString(string OneDistributionStr)
        {
            try
            {
                if (OneDistributionStr.Length < 100) return null;
                string[] arr = OneDistributionStr.Split(',');
                if (arr.Length < 2) return null;
                string[] header = arr[0].Split('|');
                if (header.Length < 6) return null;
                FNETDistribution OneDist = new FNETDistribution(Convert.ToDouble(header[1]), Convert.ToDouble(header[2]), Convert.ToInt32(header[3]), header[0]);
                OneDist._totalCount = Convert.ToInt32(header[4]);
                OneDist.Sum = Convert.ToDouble(header[5]);

                if (arr.Length != OneDist.BinCount + 3) return null;
                OneDist.OverLower = Convert.ToInt32(arr[1]);
                for (int i = 0; i < OneDist.BinCount; ++i)
                    OneDist.Counts[i] = Convert.ToInt32(arr[i + 2]);
                OneDist.OverUpper = Convert.ToInt32(arr[arr.Length - 1]);
                return OneDist;
            }
            catch (Exception e)
            { return null; }
        }
        public void SyncToFile(string folder)
        {
            StreamWriter SW = new StreamWriter(folder + Name + "_CountDistribution.txt", false);
            StreamWriter SW2 = new StreamWriter(folder + Name + "_PercentageDistribution.csv", false);
            SW.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            SW2.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            SW.WriteLine(SerializeToTitleString());
            SW.WriteLine(SerializeToString());
            SW2.WriteLine(SerializeToTitleString());
            SW2.WriteLine(SerializePercentToString());
            SW.Close();
            SW2.Close();
        }

    }

    /*
    public class YWPDistributionGroup
    {
        private bool _Initialized = false;
        private Dictionary<string, FNETDistribution> _Distributions = new Dictionary<string, FNETDistribution>();
        private Dictionary<string, FNETDistribution> _Distributions_ID = new Dictionary<string, FNETDistribution>();
        private string _GroupName;
        private string _RootFolder;
        private double _LowLimit;
        private double _UpLimit;
        private int _BinCount;
        public YWPDistributionGroup(string groupName, string rootFolder, double lowLimit, double upLimit, int binCount)
        {
            _GroupName = groupName;
            _RootFolder = rootFolder;
            _LowLimit = lowLimit;
            _UpLimit = upLimit;
            _BinCount = binCount;
            Initial(_GroupName, _RootFolder);
        }
        public void SyncToFile()
        {
            SyncToFile(_GroupName, _RootFolder);
        }
        private string GetCachFileName(string GroupName, string RootFolder)
        {
            return string.Format("{0}{1}_DistCache.txt", RootFolder, GroupName);
        }
        private string GetCachFileName_Percentage(string GroupName, string RootFolder)
        {
            return string.Format("{0}{1}_DistPercentage.csv", RootFolder, GroupName);
        }
        private void Initial(string GroupName, string RootFolder)
        {
            lock (_Distributions)
            {
                if (_Initialized) return;
                _Initialized = true;
                string cachFileName = GetCachFileName(GroupName, RootFolder);
                if (!File.Exists(cachFileName)) return;
                StreamReader SR = new StreamReader(new FileStream(cachFileName, FileMode.Open, FileAccess.Read));
                while (!SR.EndOfStream)
                {
                    string OneLine = SR.ReadLine();
                    FNETDistribution OneDis = FNETDistribution.DeserializeFromString(OneLine);
                    if (OneDis == null) continue;
                    if (_Distributions.ContainsKey(OneDis.Name)) _Distributions[OneDis.Name] = OneDis;
                    else _Distributions.Add(OneDis.Name, OneDis);
                    if (_Distributions_ID.ContainsKey(OneDis.ID)) _Distributions_ID[OneDis.ID] = OneDis;
                    else _Distributions_ID.Add(OneDis.ID, OneDis);
                }
                SR.Close();
            }
        }
        private void SyncToFile(string GroupName, string RootFolder)
        {
            //if (!_Initialized) Initial(RootFolder);
            lock (_Distributions)
            {
                StreamWriter SW = new StreamWriter(GetCachFileName(GroupName, RootFolder), false);
                StreamWriter SW2 = new StreamWriter(GetCachFileName_Percentage(GroupName, RootFolder), false);
                SW.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                SW2.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                foreach (var v in _Distributions)
                {
                    SW.WriteLine(v.Value.SerializeToString());
                    SW2.WriteLine(v.Value.SerializePercentToString());
                }
                SW.Close();
                SW2.Close();
            }
        }
        public FNETDistribution GetDistribution(string Name)
        {
            if (_Distributions.ContainsKey(Name)) return _Distributions[Name];
            if (_Distributions_ID.ContainsKey(Name)) return _Distributions_ID[Name];
            return null;
        }
        public FNETDistribution GetOrCreateDistribution(string Name)
        {
            if (_Distributions.ContainsKey(Name)) return _Distributions[Name];
            if (_Distributions_ID.ContainsKey(Name)) return _Distributions_ID[Name];
            var v = new FNETDistribution(_LowLimit, _UpLimit, _BinCount, Name);
            _Distributions.Add(v.Name, v);
            _Distributions_ID.Add(v.ID, v);
            return v;
        }
        public void AddDistribution(FNETDistribution Dist)
        {
            if (!_Distributions.ContainsKey(Dist.Name))
                _Distributions.Add(Dist.Name, Dist);
            if (!_Distributions_ID.ContainsKey(Dist.ID))
                _Distributions_ID.Add(Dist.ID, Dist);
        }
    }
    */
}
