﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FNETOsciMitigation
{
    public delegate void SendControlCMD(DateTime timestamp, List<DACSet> DACSettings, bool CMDFromSysIdent = false);
    public delegate void SendGUIMsg(string MsgName, string MsgValue);
    public delegate void DataHandler(List<PMUMeasurement> ReceivedDataList);
    //public abstract class IController
    //{
    //    /// <summary>
    //    /// openPDC publish received measurements with time alignment.
    //    /// This function will be invoked by openPDC/openECA when PMU data is received. 
    //    /// Measurements of the subscribed PMUs, primary PMU and backup PMU in this project, can be found from  OneFrame.Measurements
    //    /// If fps=30, theoretically PublishFrame(...) will be called 30 times every second.
    //    /// </summary>
    //    /// <param name="OneFrame"></param>
    //    public abstract void PublishFrame(PMUFrame OneFrame);

    //    /// <summary>
    //    /// openPDC publish received measurements without time alignment.
    //    /// This function will be invoked by openPDC/openECA when PMU data is received. 
    //    /// Measurements from one or multiple PMUs can be provided. The timestamps of the measurement may not be same
    //    /// since no time alignment is performed. Given fps = 30, this function may be called more than 30 times per second
    //    /// </summary>
    //    /// <param name="ReceivedMea"></param>
    //    public abstract void PublishMeasurements(List<PMUMeasurement> ReceivedMea);

    //    /// <summary>
    //    /// Once the controlling common is generated, OnControlCMDGenerated(...) can be invoked to send cmd to receiver
    //    /// </summary>
    //    public abstract event SendControlCMD OnControlCMDGenerated;

    //}
    public class PMUFrame
    {
        /// <summary>
        /// Timestamp of the frame, which should be same with the timstamp in each PMUMeasurement
        /// </summary>
        public DateTime TimeStamp;
        /// <summary>
        /// Data list
        /// </summary>
        public List<PMUMeasurement> Measurements = new List<PMUMeasurement>();
        public PMUMeasurement SysMedian = null;
        public PMUFrame(DateTime timestamp)
        {
            TimeStamp = timestamp;
        }
        public PMUFrame GetCopy()
        {
            PMUFrame copy = new PMUFrame(TimeStamp);
            foreach (var v in Measurements)
                copy.Measurements.Add(v);
            return copy;
        }
    }
    public class PMUMeasurement : TimeStamped
    {
        public int UnitID;
        public string UnitName;
        public PMU pmu;
        //public DateTime TimeStamp;
        public double Frequency = 0;
        /// <summary>
        /// frequency deviation from the average frequency of a time window.
        /// </summary>
        public double RelativeFreq = 0;
        /// <summary>
        /// phase angle, unit: radian
        /// </summary>
        public double Angle = 0;
        /// <summary>
        /// relative phase angle
        /// </summary>
        public double RelativeAngle = 0;
        public double RelativeAng_Nom = 0;  //nominalized relative angle (substract first relative angle)
        public double Voltage = 0;
        public int ArtificialLatency_MS = 0;
        public double DelayInSec
        {
            get { return DateTime.UtcNow.Subtract(TimeStamp).TotalSeconds; }
        }
        public PMUMeasurement(int unitID, DateTime ts,string unitName,bool isBackup)
        {
            UnitID = unitID;
            UnitName = unitName;
            pmu = new PMU(unitID, unitName, isBackup);
            TimeStamp = ts;
            //DelayInSec = DateTime.UtcNow.Subtract(ts).TotalSeconds;
        }
    }
    public class PMU
    {
        public int UnitID;
        public string UnitName;
        public bool isBackup;
        public PMU(int id, string name, bool backup)
        {
            UnitID = id;UnitName = name;isBackup = backup;
        }
        public override int GetHashCode()
        {
            string combined = $"{UnitID}{UnitName}{Convert.ToString(isBackup)}";
            return combined.GetHashCode();
        }
        public bool Equals(PMU otherpmu)
        {
            return otherpmu != null && otherpmu.UnitID == UnitID && otherpmu.UnitName == UnitName && otherpmu.isBackup == isBackup;
        }
        public override bool Equals(object obj)
        {
            return this.Equals(obj as PMU);
        }
    }
    public class DACSet
    {        
        public static int ByteLength = 9;
        public byte DACChannelID;
        /// <summary>
        /// DAC output current (uA, -20000 ~ 20000).
        /// </summary>
        public int DACSetValue = 0;
        public int DACDelay_ms = 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelID"></param>
        /// <param name="settingValue">DAC output current. uA,(4000, 20000) </param>
        /// <param name="delayMS"></param>
        public DACSet(byte channelID, int settingValue, int delayMS)
        {
            DACChannelID = channelID;
            DACSetValue = settingValue;
            DACDelay_ms = delayMS;
        }
        public byte[] GetByte()
        {   //0:    DAC ID
            //1-4:  DAC set value;
            //5-8:  Delay
            byte[] buff = new byte[ByteLength];
            buff[0] = DACChannelID;
            FNetBigEndian.CopyBytes(DACSetValue, buff, 1);
            FNetBigEndian.CopyBytes(DACDelay_ms, buff, 5);    //Delay
            return buff;
        }
    }
}
