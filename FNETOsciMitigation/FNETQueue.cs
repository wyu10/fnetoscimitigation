﻿/*******************************************************************************************
 * Base class of all the buffer.
 * The buffer is implemented based on cycled List becaused of the following requirements:
 * 1) Time complexity O(1) to get head or tail.
 * 2) Time complexity O(1) to get any element with its index.
 * 3) Average time complexity O(1) to push element from head and remove element from tail.
 * 
 * Author: Wenpeng Yu
 * Email: yuwenpeng0820@gmail.com, wyu900@gmail.com
 * 2017.8
********************************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;

namespace FNETOsciMitigation
{
    public class FNETQueue<T>:IEnumerable<T>,IEnumerable
    {
        private List<T> _data;
        private int _data_cap;
        private int _tailIndex;
        private int _version;
        private int _count;
        public FNETQueue()
        {
            _tailIndex = 0;
            _data_cap = 7;
            _data = new List<T>(_data_cap);
            _count = 0;
            _version = 0;
            for (int i = 0; i < _data_cap; ++i)
                _data.Add(default(T));
        }
        public FNETQueue(int capacity)
        {
            _tailIndex = 0;
            _data_cap = capacity;
            _count = 0;
            _version = 0;
            _data = new List<T>(_data_cap);
            for (int i = 0; i < _data_cap; ++i)
                _data.Add(default(T));
        }
        public int Count
        {
            get { return _count; }
            private set { _count = value; _version++; if (_version > int.MaxValue - 2) _version = 0; }
        }
        /// <summary>
        /// Add one item to head
        /// </summary>
        /// <param name="OneItem"></param>
        public void Push(T OneItem)
        {
            lock (_data)
            {
                T h_old, H1, H2;
                h_old = Head;
                Resize();
                H1 = Head;
                _data[(_tailIndex + Count) % _data_cap] = OneItem;
                Count++;
                H2 = Head;
                if(H2==null)
                {
                    Count += 0;
                }
            }
        }
        /// <summary>
        /// Remove one item from tail.
        /// </summary>
        public void Pop()
        {
            if(Count > 0)
            {
                _data[_tailIndex] = default(T);
                _tailIndex = (_tailIndex + 1) % _data_cap;
                Count--;
            }
        }
        public void PopHead()
        {
            if (Count > 0)
            {
                _data[(_tailIndex + Count - 1) % _data_cap] = default(T);
                Count--;
            }
        }
        public T Tail
        {
            get
            {
                if (Count > 0) return _data[_tailIndex];
                else return default(T);
            }
        }
        public T Head
        {
            get
            {
                if (Count > 0) return _data[(_tailIndex + Count - 1) % _data_cap];
                else return default(T);
            }
        }
        public void Clear()
        {
            for (int i = 0; i < Count; ++i)
                this[i] = default(T);
            _tailIndex = 0;
            Count = 0;
        }
        private void Resize()
        {   //downsize is not implemented yet...
            if (_data_cap - Count > 1) return;
            List<T> newData = new List<T>(_data_cap * 2+1);
            foreach (var v in this) newData.Add(v);
            for (int i = _data_cap * 2 - newData.Count; i >= 0; --i)
                newData.Add(default(T));
            _tailIndex = 0;
            _data.Clear();
            _data = newData;
            _data_cap *= 2;
        }
        public T this[int Index]
        {
            get
            {
                if (Index < 0 || Index >= Count) return default(T);
                return _data[(_tailIndex + Index) % _data_cap];
            }
            private set
            {
                if (Index < 0 || Index >= Count) throw new Exception("The index is out of range.");
                _data[(_tailIndex + Index) % _data_cap] = value;
            }
        }
        public IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(this);
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator(this);
        }
        public struct Enumerator: IEnumerator<T>, IEnumerator
        {
            private FNETQueue<T> _ywpQueue;
            private int _localVersion;
            private int _localIndex;
            private T _current;
            internal Enumerator(FNETQueue<T> Q)
            {
                _ywpQueue = Q;
                _localVersion = Q._version;
                _localIndex = -1;
                _current = default(T);
            }
            public bool MoveNext()
            {
                CheckVersion();
                _localIndex++;
                if (_localIndex >= _ywpQueue.Count)
                {
                    _current = default(T);
                    return false;
                }
                else
                {
                    _current = _ywpQueue[_localIndex];
                    return true;
                }
            }
            public T Current
            {
                get
                {
                    CheckVersion();
                    if (_localIndex >= _ywpQueue.Count) throw new Exception("Out of range");
                    return _current;
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    CheckVersion();
                    if (_localIndex >= _ywpQueue.Count) throw new Exception("Out of range");
                    return _current;
                }
            }

            public void Dispose()
            {
            }           
            private void CheckVersion()
            {
                if (_localVersion != _ywpQueue._version)
                    throw new Exception("The queue changed during the enumeration.");
            }
            public void Reset()
            {
                throw new NotImplementedException();
            }
        }
    }
}
