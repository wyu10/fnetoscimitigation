﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace FNETOsciMitigation
{
    /// <summary>
    /// Class for logging purpose.
    /// </summary>
    public class FNETLog
    {

        private readonly string RootFolder = "";
        private static Dictionary<string, MyStreamWriter> AllWriters = new Dictionary<string, MyStreamWriter>();
        private static FNETLog DefaultLoger = new FNETLog("Log\\", new FNETLogTypeCFG("LogDefault", "", true, true, FNETLogTypeCFG.ExpireType.Monthly));
        private readonly FNETLogTypeCFG DefaultLogCfg;
        private string GetKey(FNETLogTypeCFG logCFG)
        {
            if (logCFG == null) return "";
            return RootFolder.ToUpper() + logCFG.GetKey();
        }
        /// <summary>
        /// Specify the root folder of all log files. 
        /// If 'defaultLogCfg' is null, default log file will be %RootFolder%\Log\Log[yyyy-MM].txt
        /// </summary>
        public FNETLog(string rootFolder, FNETLogTypeCFG defaultLogCfg = null)
        {
            RootFolder = rootFolder;
            if (RootFolder != null && RootFolder.Length > 0 && !Directory.Exists(RootFolder))
                Directory.CreateDirectory(RootFolder);
            if (defaultLogCfg != null)
                DefaultLogCfg = defaultLogCfg;
            else
                DefaultLogCfg = new FNETLogTypeCFG("Log", "Log\\", true, true, FNETLogTypeCFG.ExpireType.Monthly);
        }
        public void WriteLog(string LogMsg, FNETLogTypeCFG logCFG = null)
        {
            if (logCFG == null) logCFG = DefaultLogCfg;
            string key = GetKey(logCFG);
            lock (AllWriters)
                if (!AllWriters.ContainsKey(key))
                    AllWriters.Add(key, new MyStreamWriter(RootFolder, logCFG));
            lock (AllWriters[key])
                AllWriters[key].Log(LogMsg);
        }
        public static void WriteDefaultLog(string LogMsg)
        {
            DefaultLoger.WriteLog(LogMsg);
        }
        public void CloseWritter(FNETLogTypeCFG logCFG)
        {
            string key = GetKey(logCFG);
            if (!AllWriters.ContainsKey(key)) return;
            lock (AllWriters[key]) AllWriters[key].CloseWriter();
        }
        public void CloseAllWritter(FNETLogTypeCFG logCFG)
        {
            lock (AllWriters)
                foreach (var v in AllWriters)
                    v.Value.CloseWriter();
        }
        private class MyStreamWriter
        {
            private StreamWriter SW = null;
            private DateTime ExpiredTime;
            private string RootFolder;
            private FNETLogTypeCFG ThisLogType;
            public MyStreamWriter(string rootFolder, FNETLogTypeCFG logType)
            {
                ThisLogType = logType;
                RootFolder = rootFolder;
            }
            public void Log(string msg)
            {
                if (SW == null) SW = GetNewSW();
                else if (ThisLogType.LogExpireType != FNETLogTypeCFG.ExpireType.NeverExpire && DateTime.Now.CompareTo(ExpiredTime) >= 0)
                {
                    SW.Close();
                    SW = GetNewSW();
                }
                if (!ThisLogType.LogLocalTime) SW.WriteLine(msg);
                else SW.WriteLine(DateTime.Now.ToString("[MM/dd/yyyy HH:mm:ss.fff]\t") + msg);
                SW.Flush();
            }
            private StreamWriter GetNewSW()
            {
                StreamWriter sw = null;
                string FileNameAndPath = GetFileNameAndSetExpireTime();
                try
                { sw = new StreamWriter(FileNameAndPath + ".txt", ThisLogType.Append); }
                catch (Exception e)
                {
                    Random ra = new Random(DateTime.Now.Millisecond);
                    int tryCount = 0;
                    while (sw == null && tryCount++ < 10000)
                    {
                        try
                        { sw = new StreamWriter(FileNameAndPath + "_" + ra.Next(1000).ToString() + ".txt", ThisLogType.Append); }
                        catch (Exception ee) { sw = null; }
                    }
                    if (sw != null)
                    {
                        sw.WriteLine("**********************************************************");
                        sw.WriteLine("* File Access Confliction: " + FileNameAndPath + ".txt");
                        sw.WriteLine("**********************************************************");
                        sw.Flush();
                    }
                }
                return sw;
            }
            public void CloseWriter()
            {
                if (SW != null)
                {
                    try { SW.Close(); SW = null; }
                    catch (Exception e) { }
                }
            }
            /// <summary>
            /// No extension 
            /// </summary>
            private string GetFileNameAndSetExpireTime()
            {
                string Path = RootFolder + ThisLogType.SubFolder;
                if (Path != null && Path.Length > 0 && !Directory.Exists(Path)) Directory.CreateDirectory(Path);
                string FileName = Path + ThisLogType.TypeName;
                DateTime Now = DateTime.Now;
                switch (ThisLogType.LogExpireType)
                {
                    case FNETLogTypeCFG.ExpireType.NeverExpire:
                        ExpiredTime = DateTime.MaxValue;
                        break;
                    case FNETLogTypeCFG.ExpireType.Yearly:
                        ExpiredTime = new DateTime(Now.Year + 1, 1, 1);
                        FileName += string.Format(" [{0}]", Now.Year);
                        break;
                    case FNETLogTypeCFG.ExpireType.Monthly:
                        ExpiredTime = new DateTime(Now.AddMonths(1).Year, Now.AddMonths(1).Month, 1);
                        FileName += string.Format(" [{0}]", Now.ToString("yyyy-MM"));
                        break;
                    case FNETLogTypeCFG.ExpireType.Daily:
                        ExpiredTime = Now.AddDays(1).Date;
                        FileName += string.Format(" [{0}]", Now.ToString("yyyy-MM-dd"));
                        break;
                    case FNETLogTypeCFG.ExpireType.Hourly:
                        ExpiredTime = Now.AddHours(1).Date.AddHours(Now.AddHours(1).Hour);
                        FileName += string.Format(" [{0}]", Now.ToString("yyyy-MM-dd HH"));
                        break;
                    case FNETLogTypeCFG.ExpireType.Minutely:
                        ExpiredTime = Now.AddMinutes(1).Date.AddHours(Now.AddMinutes(1).Hour).AddMinutes(Now.AddMinutes(1).Minute);
                        FileName += string.Format(" [{0}]", Now.ToString("yyyy-MM-dd HH.mm"));
                        break;
                }
                return FileName;
            }
        }
    }

    public class FNETLogTypeCFG
    {
        public enum ExpireType { NeverExpire, Yearly, Monthly, Daily, Hourly, Minutely }

        public readonly string TypeName;
        public readonly string SubFolder;
        public readonly bool Append;
        public readonly ExpireType LogExpireType;
        public readonly bool LogLocalTime;
        public FNETLogTypeCFG(string typeName, string subFolder, bool append, bool logLocalTime, ExpireType expireType)
        {
            if (typeName != null && typeName.Length > 0)
                TypeName = typeName;
            else TypeName = "Log_NoTypeName";
            SubFolder = subFolder;
            Append = append;
            LogLocalTime = logLocalTime;
            LogExpireType = expireType;
        }
        public override string ToString() { return SubFolder.ToUpper() + TypeName.ToUpper(); }
        public string GetKey() { return SubFolder.ToUpper() + TypeName.ToUpper(); }
    }
}
